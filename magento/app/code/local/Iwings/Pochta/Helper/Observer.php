<?php
class Iwings_Pochta_Helper_Observer
{
    public function beforeSave(Varien_Event_Observer $observer)
  	{

        /** @var $event Varien_Event */
        $event = $observer->getEvent();

        $object = $event->getDataObject();

        if ($object instanceof Mage_Catalog_Model_Product && $object->getTypeId() == "bundle" && $object->getAttributeSetId() == Mage::registry('attr_set_id_parcel')) {

            $this->checkStatusAndSendEmail($object);
        }

        return $this;
  	}

    private function checkStatusAndSendEmail($product) {
        /** @var $product Mage_Catalog_Model_Product */
        if (!$product->isObjectNew()) {
            if ($product->getData('client_visibility') && !$product->getOrigData('client_visibility')) {
                $this->sendEmail('Parcel New Available', $product->getBoxSuiteNum());
            }
            if ($product->getData('state_custom') == Mage::registry('state_custom_client_info_required') && $product->getOrigData('state_custom') == Mage::registry('state_custom_na')) {
                $this->sendEmail('Parcel Info Required', $product->getBoxSuiteNum());
            }
            if ($product->getData('state_payment') == Mage::registry('state_payment_ready_for_payment') && $product->getOrigData('state_payment') == Mage::registry('state_payment_not_ready')) {
                $this->sendEmail('Parcel Ready For Payment', $product->getBoxSuiteNum());
            }
            if ($product->getData('state_payment') == Mage::registry('state_payment_paid') && $product->getOrigData('state_payment') == Mage::registry('state_payment_ready_for_payment')) {
                $this->sendEmail('Parcel Paid', $product->getBoxSuiteNum());
            }
            if ($product->getData('state_payment') == Mage::registry('state_payment_deposit_returned') && $product->getOrigData('state_payment') == Mage::registry('state_payment_paid')) {
                $this->sendEmail('Parcel Deposit Returned', $product->getBoxSuiteNum());
            }
        }
    }

    private function sendEmail($templateName, $suiteNum) {

        /** @var $emailTemplate Aschroder_SMTPPro_Model_Email_Template */
        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($templateName);

        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

        $emailTemplate
            ->setSenderEmail($senderEmail)
            ->setSenderName($senderName);

        $customer = Mage::getModel('customer/customer')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter('suite', $suiteNum)->getFirstItem();
        $customer->load($customer->getId());

        $customerName = $customer->getName();
        if (!Mage::registry('is_production_environment')) {
            $customerEmail = Mage::getModel('core/variable')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByCode('non_production_email')
                ->getValue('text');
            if (!$customerEmail) {
                $customerEmail = 'admin@pochta.fi';
            }
        } else {
            $customerEmail = $customer->getEmail();
        }

        $emailTemplateVariables = array();      // useless - overwritten in $emailTemplate->send
        $emailTemplateVariables['name'] = $customerName;
        $emailTemplateVariables['email'] = $customerEmail;

        try {
            $emailTemplate->send($customerEmail, $customerName, $emailTemplateVariables);
        }
        catch(Exception $error)
        {
            Mage::getSingleton('core/session')->addError($error->getMessage());
            return false;
        }

    }
	
	
}

?>