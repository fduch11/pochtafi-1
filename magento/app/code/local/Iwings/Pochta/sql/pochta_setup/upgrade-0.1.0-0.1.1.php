<?php

return;
$original_installer = $this;
$installer = Mage::getModel('customer/entity_setup', 'core_setup');
$installer->startSetup();

$customerAttributes = array(
    array('AttrName' => 'default_place', 'AttrLabel' => 'Default Place', 'AttrType' => 'int', 'AttrInput' => 'select', 'AttrRequired' => true, 'AttrSource' => 'eav/entity_attribute_source_table',
        'AttrOptions' => array('values' => array('Kotka', 'Lapa'))),
    array('AttrName' => 'notify_me', 'AttrLabel' => 'Notify', 'AttrType' => 'varchar', 'AttrInput' => 'multiselect', 'AttrRequired' => false, 'AttrBackend' => 'eav/entity_attribute_backend_array', 'AttrSource' => 'eav/entity_attribute_source_table',
        'AttrOptions' => array('values' => array('Email', 'Phone'))),
    array('AttrName' => 'phone', 'AttrLabel' => 'Phone', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => false),
    array('AttrName' => 'suite', 'AttrLabel' => 'Suite', 'AttrType' => 'varchar', 'AttrInput' => 'text', 'AttrRequired' => true)
);

foreach ($customerAttributes as $attribute) {
    $installer->addAttribute('customer', $attribute['AttrName'], array(
        'type' => $attribute['AttrType'],
        'label' => $attribute['AttrLabel'],
        'input' => $attribute['AttrInput'],
        'required' => $attribute['AttrRequired'],
        'source' => $attribute['AttrSource'] == null ? '' : $attribute['AttrSource'],
        'backend' => $attribute['AttrBackend'] == null ? '' : $attribute['AttrBackend'],
        'user_defined' => true,
        'is_configurable' => false,
        'is_html_allowed_on_front' => true,
        'option' => $attribute['AttrOptions'] == null ? '' : $attribute['AttrOptions']
    ));

    $showAttr = Mage::getSingleton('eav/config')->getAttribute('customer', $attribute['AttrName']);
    $showAttr->setData('used_in_forms', array('adminhtml_customer', 'customer_account_edit'));
    $showAttr->save();
}

$installer->endSetup();
?>