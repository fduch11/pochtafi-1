<?php

require_once("BaseController.php");

require_once('lib/tulli/Custom.php');

class Iwings_Pochta_IssuanceController extends Iwings_Pochta_BaseController
{

    public function buildListAction() {
        /** @var $movements Mage_Catalog_Model_Resource_Product_Collection */
        $movements = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'virtual')
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_movement'))
            ->addAttributeToFilter('reception_type', Mage::registry('reception_type_na'))
            ->addAttributeToFilter('mvt_src_status', Mage::registry('mvt_src_status_planned'))
            ->addAttributeToFilter('mvt_src_whs', Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::getSingleton('core/session')->getSelectedStoreId()))
        ;

        foreach ($movements as $movement) {
            /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
            $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movement->getId());

            $fullyPaid = true;
            $declaration_required = false;
            foreach ($productsLinkedAsCrossSell as $link) {
                $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
                if ($parcel->getStatePayment() != Mage::registry('state_payment_paid')) {
                    $fullyPaid = false;
                }
                if ($parcel->getBoxOrigin() != Mage::registry('box_origin_european')) {
                    $declaration_required = true;
                }
            }

            $movement->setParcelsCount(count($productsLinkedAsCrossSell));
            $movement->setDeclarationRequired($declaration_required);
            $movement->setFullyPaid($fullyPaid);
        }

        Mage::register('issuanceList', $movements);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function buildViewAction() {

        $movementId = $this->getRequest()->getParam('id');
        Mage::register('movementId', $movementId);

        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movementId);
        Mage::register('parcelIds', $productsLinkedAsCrossSell);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function buildViewPostAction() {

        $movementId = $this->getRequest()->getParam('movementId');
        $parcels = $this->getRequest()->getParam('parcelIds');

        $movementComment = '';
        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movementId);
        foreach ($productsLinkedAsCrossSell as $link) {
            if (array_search($link->getProductId(), $parcels) === false) {
                // missed parcel
                $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
                $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
                $parcel->setBoxStore(Mage::registry('box_store_na'));
                $message = $this->__("Parcel not found. MovementId: " . $movementId . ", DateTime: " . date("Y-m-d hh:mm"));
                if ($parcel->getComment()) {
                    $message = PHP_EOL . $message;
                }
                $parcel->setComment($message);
                $parcel->save();

                if ($movementComment) {
                    $movementComment .= PHP_EOL;
                }
                $movementComment .= $this->__("Parcel " . $link->getProductId() ." not found. DateTime: " . date("Y-m-d hh:mm"));
            }
        }

        $movement = Mage::getModel('catalog/product')->load($movementId);
        $movement->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $movement->setMvtSrcStatus(Mage::registry('mvt_src_status_assembled'));
        if ($movementComment) {
            if ($movement->getComment()) {
                $movementComment = PHP_EOL . $movementComment;
            }
            $movement->setComment($movementComment);
        }
        $movement->save();

        $this->_redirect('pochta/issuance/buildList');
    }


    public function listAction() {
        /** @var $movements Mage_Catalog_Model_Resource_Product_Collection */
        $movements = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'virtual')
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_movement'))
            ->addAttributeToFilter('reception_type', Mage::registry('reception_type_na'))
            ->addAttributeToFilter('mvt_src_status', Mage::registry('mvt_src_status_assembled'))
            ->addAttributeToFilter('mvt_src_whs', Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::getSingleton('core/session')->getSelectedStoreId()))
        ;

        foreach ($movements as $movement) {
            /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
            $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movement->getId());

            $fullyPaid = true;
            $declaration_required = false;
            foreach ($productsLinkedAsCrossSell as $link) {
                $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
                if ($parcel->getStatePayment() != Mage::registry('state_payment_paid')) {
                    $fullyPaid = false;
                }
                if ($parcel->getBoxOrigin() != Mage::registry('box_origin_european')) {
                    $declaration_required = true;
                }
            }

            $movement->setParcelsCount(count($productsLinkedAsCrossSell));
            $movement->setDeclarationRequired($declaration_required);
            $movement->setFullyPaid($fullyPaid);
        }

        Mage::register('issuanceList', $movements);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function viewAction() {

        Mage::getSingleton('core/session')->unsIssuance();
        Mage::getSingleton('core/session')->setIssuance(array());

        $issuance = Mage::getSingleton('core/session')->getIssuance();
        $issuance['movementId'] = $this->getRequest()->getParam('id');
        Mage::getSingleton('core/session')->setIssuance($issuance);

        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($issuance['movementId']);
        Mage::register('parcelIds', $productsLinkedAsCrossSell);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function viewPostAction() {

        $issuance = Mage::getSingleton('core/session')->getIssuance();
        //$issuance['movementId'] = $this->getRequest()->getParam('movementId');
        $issuance['parcels'] = $this->getRequest()->getParam('parcelIds');


        $movementId = $issuance['movementId'];
        $parcels = $issuance['parcels'];

        $movementComment = '';
        $declaration_required = false;
        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movementId);
        foreach ($productsLinkedAsCrossSell as $link) {
            if (array_search($link->getProductId(), $parcels) === false) {
                // missed parcel
                $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
                if ($parcel->getBoxStore() != Mage::registry('box_store_na')) {
                    $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $message = $this->__("Parcel cannot be issued. MovementId: " . $movementId . ", DateTime: " . date("Y-m-d hh:mm"));
                    if ($parcel->getComment()) {
                        $message = PHP_EOL . $message;
                    }
                    $parcel->setComment($message);
                    $parcel->save();

                    if ($movementComment) {
                        $movementComment .= PHP_EOL;
                    }
                    $movementComment .= $this->__("Parcel " . $link->getProductId() ." cannot be issued. DateTime: " . date("Y-m-d hh:mm"));
                }
            } else {
                $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
                if ($parcel->getBoxOrigin() != Mage::registry('box_origin_european')) {
                    $declaration_required = true;
                }
            }
        }

        $issuance['declaration_required'] = $declaration_required;
        Mage::getSingleton('core/session')->setIssuance($issuance);

        if ($movementComment) {
            $movement = Mage::getModel('catalog/product')->load($movementId);
            $movement->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
            if ($movement->getComment()) {
                $movementComment = PHP_EOL . $movementComment;
            }
            $movement->setComment($movementComment);
            $movement->save();
        }

        if ($declaration_required) {
            $this->_redirect('pochta/issuance/movement');
        } else {
            $this->_redirect('pochta/issuance/complete');
        }
    }

    public function issuedListAction()
    {
        $collection_movements = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'virtual')
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_movement'))
            ->addAttributeToFilter('reception_type', Mage::registry('reception_type_na'))
            ->addAttributeToFilter('mvt_src_status', Mage::registry('mvt_src_status_left'));

        Mage::register('issuance_list', $collection_movements);
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function issuedViewAction()
    {
        $issuance = Mage::getSingleton('core/session')->getIssuance();
        $issuance['movement_id'] = $this->getRequest()->getParam('id');
        $product_links = Mage::helper('pochta')->getProductsByMovement($issuance['movement_id']);

        Mage::getSingleton('core/session')->setIssuance($issuance);
        Mage::register('parcel_ids', $product_links);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function movementAction() {

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function movementPostAction() {

        $issuance = Mage::getSingleton('core/session')->getIssuance();

        $movementId = $issuance['movementId'];
        $movement = Mage::getModel('catalog/product')->load($movementId);

        $movement
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope

            ->setCarrierNameFirst($_POST['carrier_name_first'])
            ->setCarrierNameLast($_POST['carrier_name_last'])
            ->setConsigneeAddressLine($_POST['consignee_address_line'])
            ->setConsigneeAddressCity($_POST['consignee_address_city'])
            ->setConsigneeAddressCountry($_POST['consignee_address_country'])
            ->setConsigneeAddressPostcode($_POST['consignee_address_postcode'])

            ->setTransportModeCode($_POST['transport_mode_code'])
            ->setTransportNationalityCode($_POST['transport_nationality_code'])
            ->setTransportMeansId($_POST['transport_means_id'])
//            ->setConveyanceReferenceId(null)

            ->setMvtDstWhs($_POST['borderline'])
            ->setMvtDstPlannedDate($_POST['carrier_date_in'])

            ->setComment($_POST['comment'])
        ;

        // attach documents
        $this->attachDocument('passport_scan', $movementId);
        $this->attachDocument('signature_scan', $movementId);
        $this->attachDocument('pts_scan', $movementId);

        $movement->save();


        $this->_redirect('pochta/issuance/showFITDEC');
    }

    public function showFITDECAction() {

        $issuance = Mage::getSingleton('core/session')->getIssuance();

        $movementId = $issuance['movementId'];
        $movement = Mage::getModel('catalog/product')->load($movementId);

        $fitdec_body = Custom::buildFITDEC($movement);
        Mage::register('fitdec_body', $fitdec_body);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function showFITDECPostAction() {

        $fitdec_body = $this->getRequest()->getParam('fitdec_body');

        $declaration = Custom::sendDeclaration($fitdec_body);

        $issuance = Mage::getSingleton('core/session')->getIssuance();
        $issuance['declarationId'] = $declaration->getId();
        $issuance['ControlReferenceID'] = $declaration->getCsControlReferenceId();
        Mage::getSingleton('core/session')->setIssuance($issuance);

        /** @var $movement Mage_Catalog_Model_Product */
        $movement = Mage::getModel('catalog/product')->load($issuance['movementId']);
        $movement->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        $relation_data = Mage::helper('pochta')->getCustomData($movement);
        $relation_data[$declaration->getId()] = array('position' => 0);
        $movement->setCustomLinkData($relation_data);

        $movement->save();

        if ($declaration->getCsResponseCode() === '000') {
            $this->_redirect('pochta/issuance/complete');
        } else {
            $this->_redirect('pochta/issuance/showFITDECResult');
        }
    }

    public function showFITDECResultAction() {

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function showFITDECResultPostAction() {
        $this->_redirect('pochta/issuance/complete');
    }

    public function completeAction() {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function completePostAction() {

        $takeoutTime = strtotime('now');

        $issuance = Mage::getSingleton('core/session')->getIssuance();
        $movementId = $issuance['movementId'];
        $parcels = $issuance['parcels'];

        $movement = Mage::getModel('catalog/product')->load($movementId);

        $movement
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope

            ->setMvtSrcStatus(Mage::registry('mvt_src_status_left'))
            ->setMvtSrcActualDate($takeoutTime)
        ;

        // create out document
        {
            /** @var $declarations Mage_Catalog_Model_Resource_Product_Link_Product_Collection */
            $declarations = $movement->getCustomProductCollection()
                ->addAttributeToSelect('cs_mrn')
                ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_declaration'))
                ->addAttributeToFilter('name', array('like' => '%FITREL%'))
                ->setOrder('date', 'asc');
            $fitdec = $declarations->getLastItem();

            if ($fitdec) {
                $prev_document_id = $fitdec->getCsMrn();
                $prev_document_typecode = 'VV';
            }

            /** @var $document Mage_Catalog_Model_Product */
            $document = Mage::getModel('catalog/product');
            $document
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                ->setTypeId('virtual')                              //product type
                ->setSku(uniqid('sku'))
                ->setName('Document, Type: ' . $prev_document_typecode . ' Number: ' . $prev_document_id)
                ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//                ->setVisibility(?)
//                ->setCreatedAt($takeoutTime)                    //product creation time

                ->setWebsiteIds(array(Mage::registry('website_backoffice')))        //website ID the product is assigned to, as an array
                ->setAttributeSetId(Mage::registry('attr_set_id_document'))       //ID of a attribute set named 'PD'

                ->setPrevDocumentId($prev_document_id)
                ->setPrevDocumentTypecode($prev_document_typecode)
                ->setDate($takeoutTime)
            ;
            $document->save();

            $relation_data = array();
            $relation_data[$document->getId()] = array('position' => 0);
            $movement->setRelatedLinkData($relation_data);
        }   // create out document

        $movement->save();


        // remove non-issued parcels and update issued status

        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movementId);
        foreach ($productsLinkedAsCrossSell as $link) {
            $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
            $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

            if (array_search($link->getProductId(), $parcels) === false) {
                // non-issued parcel
                $relation_data = Mage::helper('pochta')->getMovementData($parcel);
                unset($relation_data[$movement->getId()]);
                $parcel->setCrossSellLinkData($relation_data);
            } else {
                $parcel->setBoxStore(Mage::registry('box_store_na'));
            }

            $parcel->save();
        }

        $this->_redirect('pochta/issuance/list');
    }

    public function cancelAction() {
        $movementId = $this->getRequest()->getParam('id');

        $movement = Mage::getModel('catalog/product')->load($movementId);
        $movement->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        Mage::register('isSecureArea', true);
        $movement->delete();
        Mage::unregister('isSecureArea');

        $this->_redirect('pochta/issuance/list');
    }

    private function attachDocument($inputName, $movementId) {
        if (!$_FILES[$inputName]['name']) {
            return;
        }

        $folderName = Mage::getBaseDir('media').DS.'product_custom_files';
        $extension = pathinfo($_FILES[$inputName]['name'], PATHINFO_EXTENSION);
        $fileName = $inputName . '_' . $movementId . '.' .$extension;
        $fullPath = $folderName . DS . $fileName;
        move_uploaded_file($_FILES[$inputName]['tmp_name'], $fullPath);

        $filesModel = Mage::getModel('files/files');
        $filesModel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $filesModel->setProdId($movementId)
            ->setFileTitle($inputName)
            ->setProdFile(DS . $fileName)
            ->setFileType($extension)
            ->setFileSize(filesize($fullPath))
            ->setFileSortorder(0)
            ->setFileCreation(date("Y-m-d H:i:s"))
            ->setFileLastmodification(date("Y-m-d H:i:s"))
            ->setFileTimestamp(time())
        ;
        $filesModel->save();

    }

}