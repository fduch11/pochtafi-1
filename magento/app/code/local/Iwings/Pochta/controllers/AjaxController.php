<?php

require_once("BaseController.php");

class Iwings_Pochta_AjaxController  extends Iwings_Pochta_BaseController
{
    public function setStoreAction()
    {
        $storeId = $_POST['storeId'];

        Mage::getSingleton('core/session')->setSelectedStoreId($storeId);
    }

    public function updateCustomerAction()
    {
        $customerId = $_POST['customerId'];
        $field = $_POST['field'];
        $value = $_POST['value'];

        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customer->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        switch ($field) {
            case "default_place" :
                $customer->setDefaultPlace($value);
                break;
            case "notify_me" :
                $oldValue = $customer->getNotifyMe();
                $notify_me = $oldValue != '' ? explode(',', $oldValue) : array();
                $key = array_search($value, $notify_me);
                if ($key === false) {
                    $notify_me[] = $value;
                } else {
                    unset($notify_me[$key]);
                }
                $customer->setNotifyMe(implode(',', $notify_me));
                break;
            case 'email':
                $customer->setEmail($value);
                break;
            case 'phone':
                $customer->setPhone($value);

                if ($value == '') {
                    $oldValue = $customer->getNotifyMe();
                    $notify_me = $oldValue != '' ? explode(',', $oldValue) : array();
                    $key = array_search(Mage::registry('notify_me_phone'), $notify_me);
                    if ($key !== false) {
                        unset($notify_me[$key]);
                    }
                    $customer->setNotifyMe(implode(',', $notify_me));
                }
                break;
        }

        try {
            $customer->validate();
            $customer->save();
        } catch (Mage_Core_Exception $e) {
            $this->setResponse($e->getMessage());
        }
    }

    public function updateCustomerFioAction()
    {
        $customerId = $_POST['customerId'];
        $lastname = $_POST['lastname'];
        $firstname = $_POST['firstname'];
        $doc_number = $_POST['doc_number'];

        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customer->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $customer->setLastname($lastname);
        $customer->setFirstname($firstname);
        $customer->setDocNumber($doc_number);

        try {
            $customer->validate();
            $customer->save();
        } catch (Mage_Core_Exception $e) {
            $this->setResponse($e->getMessage());
        }
    }
	
	
    public function updateCustomerPasswordAction()
    {
        $customerId = $_POST['customerId'];
        $customer = Mage::getModel('customer/customer')->load($customerId);
        $customer->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        // the following code pasted from AccountController/editPostAction

        $errors = array();

        $currPass   = $this->getRequest()->getPost('current_password');
        $newPass    = $this->getRequest()->getPost('password');
        $confPass   = $this->getRequest()->getPost('confirmation');

        $oldPass = $customer->getPasswordHash();
        if ( Mage::helper('core/string')->strpos($oldPass, ':')) {
            list($_salt, $salt) = explode(':', $oldPass);
        } else {
            $salt = false;
        }

        if ($customer->hashPassword($currPass, $salt) == $oldPass) {
            if (strlen($newPass)) {
                /**
                 * Set entered password and its confirmation - they
                 * will be validated later to match each other and be of right length
                 */
                $customer->setPassword($newPass);
                $customer->setConfirmation($confPass);
            } else {
                $errors[] = $this->__('New password field cannot be empty.');
            }
        } else {
            $errors[] = $this->__('Invalid current password');
        }

        try {
            $customerErrors = $customer->validate();
            if (is_array($customerErrors)) {
                $errors = array_merge($errors, $customerErrors);
            }

            if (!empty($errors)) {
                $this->setResponse($errors);
            } else {
                $customer->setConfirmation(null);
                $customer->save();
            }

        } catch (Mage_Core_Exception $e) {
            $this->setResponse($e->getMessage());
        }
    }
	
    private function setResponse($data) {
        $jsonData = json_encode($data);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);
    }

    public function convertCurrencyAction() {
        $currencyCode = $_POST['currencyCode'];
        $price = $_POST['price'];

        try {
            $rate = Mage::helper('directory')->currencyConvert(1, 'EUR', $currencyCode);
            $newPrice = $price / $rate;
            //$newPrice = Mage::helper('directory')->currencyConvert($price, $currencyCode, 'EUR');
        }
        catch (Exception $e) {
            $newPrice = 'N/A';
        }

        $this->setResponse($newPrice);
    }

    public function searchParcelsAction()
    {
        $this->initAttributeSets();

        $search_type = $_POST['search_type'];
        $search_criteria = $_POST['search_criteria'];

        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection
            ->addAttributeToFilter('attribute_set_id', $this->parcelSetId)
            ->addAttributeToSelect('box_suite_num')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('box_tracking_num')
            ->addAttributeToSelect('box_original_tracking')
            ->addAttributeToSelect('consignee_name')
        ;


        switch ($search_type) {
            case "Tracking" :
                $collection
                    ->addAttributeToFilter('box_tracking_num', $search_criteria)
                ;
                break;
            case "Original Tracking" :
                $collection
                    ->addAttributeToFilter('box_original_tracking', $search_criteria)
                ;
                break;
            case "SKU" :
                $collection
                    ->addAttributeToFilter('sku', $search_criteria)
                ;
                break;
            case 'Suite':
                $collection
                    ->addAttributeToFilter('box_suite_num', $search_criteria)
                ;
                break;
            case 'PassportNumber':
                /** @var $customers Mage_Customer_Model_Resource_Customer_Collection */
                $customers = Mage::getModel('customer/customer')->getCollection();
                $customers
                    ->addAttributeToSelect('suite')
                    ->addAttributeToFilter('doc_number', $search_criteria);

                $collection
                    ->addAttributeToFilter('box_suite_num', array('in' => $customers->getColumnValues('suite')));
                break;
            case 'PassportFullName':
                $name_parts = explode(' ', $search_criteria);
                /** @var $customers Mage_Customer_Model_Resource_Customer_Collection */
                $customers = Mage::getModel('customer/customer')->getCollection();
                $customers
                    ->addAttributeToSelect('suite')
                    ->addAttributeToFilter('firstname', $name_parts[0]);
                if (count($name_parts) > 1) {
                    $customers
                        ->addAttributeToFilter('lastname', $name_parts[1]);
                }

                $collection
                    ->addAttributeToFilter('box_suite_num', array('in' => $customers->getColumnValues('suite')));
                break;
        }

        try {
            $data = array();
            foreach ($collection as $row) {
                $item = array();
                $item['sku'] = $row->getSku();
                $item['suite'] = $row->getBoxSuiteNum();
                $item['tracking'] = $row->getBoxTrackingNum();
                $item['original_tracking'] = $row->getBoxOriginalTracking();

                /** @var $customers Mage_Customer_Model_Resource_Customer_Collection */
                $customers = Mage::getModel('customer/customer')->getCollection();
                $customers
                    ->addAttributeToSelect('suite')
                    ->addFieldToFilter('suite', $row->getBoxSuiteNum());
                $customer = $customers->getFirstItem();
                $customer->load($customer->getId());

                $item['suitefio'] = $customer->getFirstname() . ' ' . $customer->getLastname();
                $item['parcelfio'] = $row->getConsigneeName();

                $data[] = $item;
            }

            $this->setResponse($data);
        } catch (Mage_Core_Exception $e) {
            $this->setResponse($e->getMessage());
        }
    }

    public function addParcelAction() {

        $sku = $_POST['sku'];

        if (Mage::getSingleton('core/session')->getCurrentParcels() == null) {
            $parcels = array();
        } else {
            $parcels = Mage::getSingleton('core/session')->getCurrentParcels();
        }

        $parcels[] = $sku;
        Mage::getSingleton('core/session')->setCurrentParcels($parcels);
    }

    public function removeParcelAction() {

        $sku = $_POST['sku'];

        if (Mage::getSingleton('core/session')->getCurrentParcels() != null) {
            $parcels = Mage::getSingleton('core/session')->getCurrentParcels();
            $key = array_search($sku, $parcels);
            if ($key) {
                unset($key);
                Mage::getSingleton('core/session')->setCurrentParcels($parcels);
            }
        }
    }

    public function updateParcelAction()
    {
        $parcelId = $_POST['parcelId'];
        $field = $_POST['field'];
        $value = $_POST['value'];

        if (!$parcelId || !$field) {
            return;
        }

        $parcel = Mage::getModel('catalog/product')->load($parcelId);
        $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
        switch ($field) {
            case "name" :
                $parcel->setName($value);
                break;
        }

        try {
//            $parcel->validate();
            $parcel->save();
        } catch (Mage_Core_Exception $e) {
            $this->setResponse($e->getMessage());
        }
    }

    public function saveParcelContentAction() {

        $this->initAttributeSets();

        $parcelId = $_POST['parcelId'];
        $product = Mage::getModel('catalog/product')->load($parcelId);
        $product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        for ($i = 0; $i < count($_POST['sproduct_name']); $i++) {
            // little hack for general code
            $_POST['sproduct_user_currency'][$i] = 'EUR';
        }

        $suggestedFee = $this->prepareBundleContent($product, false);

        $relation_data = Mage::helper('pochta')->getServiceFeeData($product);
        foreach ($relation_data as $fee_id => $properties) {
            $serviceFee = Mage::getModel('catalog/product')->load($fee_id);

            if ($serviceFee->getFeeType() == Mage::registry('fee_type_deposit')) {
                $serviceFee
                    ->setPrice($suggestedFee);

                $serviceFee->save();
                break;
            }
        }

        $product->setStateCustom(Mage::registry('state_custom_operator_info_required'));

        $product->save();
    }

    public function saveAttorneyAction()
    {
        $this->initAttributeSets();

        $parcelId = Mage::getSingleton('core/session')->getAttorneyParcelId();
        if ($parcelId != '') {
            $parcel = Mage::getModel('catalog/product')->load($parcelId);

            $names = explode(',', $parcel->getAttorneyFullName());
            $docs = explode(',', $parcel->getAttorneyDocNumber());
            $powers = explode(',', $_POST['powers']);

            $names = array_map('trim', $names);
            $docs = array_map('trim', $docs);
            $powers = array_map('trim', $powers);

            $newNames = array();
            $newDocs = array();
            foreach ($powers as $power) {
                if ($power != '') {
                    $index = array_search($power, $names);
                        if ($index !== false) {
                        $newNames[] = $names[$index];
                        $newDocs[] = $docs[$index];
                    }
                }
            }

            if ($_POST['name'] != "") {
                $newNames[] = $_POST['name'];
                $newDocs[] = $_POST['passport'];
            }


            $parcel
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
                ->setAttorneyFullName(implode(',', $newNames))
                ->setAttorneyDocNumber(implode(',', $newDocs));

            $parcel->save();
        }
    }

    public function saveAttorneyParcelAction()
    {
        $this->initAttributeSets();

        $parcelId = $_POST['parcelId'];
        if ($parcelId != '') {
            Mage::getSingleton('core/session')->setAttorneyParcelId($parcelId);
        }
    }

    public function checkDeclarationAction() {

        $movementId = $this->getRequest()->getParam('movementId');

        $product = Mage::getModel('catalog/product')->load($movementId);

        if ($product && $product->getId()) {

            $folderName = Mage::getBaseDir('media').DS.'product_custom_files';

            $data = array();
            $relation_data =  Mage::helper('pochta')->getCustomData($product);
            foreach ($relation_data as $declarationId => $attrs) {

                $declaration = Mage::getModel('catalog/product')->load($declarationId);
                $link = null;
                if (file_exists($folderName . DS . $declaration->getSku() . '.zip')) {
                    $link = '/media/product_custom_files/' . $declaration->getSku() . '.zip';
                }
                $data[] = array(
                    'id' => $declaration->getId(),
                    'date' => $declaration->getDate(),
                    'name' => $declaration->getName(),
                    'sku' => $declaration->getSku(),
                    'mrn' => $declaration->getCsMrn(),
                    'control' => $declaration->getCsControlReferenceId(),
                    'storage' => $declaration->getCsMessageStorageId(),
                    'link' => $link
                );
            }

            $this->setResponse($data);
        } else {
            $this->setResponse(null);
        }
    }

    public function getParcelsCountAction() {
        $suite = $_POST['suite'];

        try {
            $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('box_suite_num', $suite);
            $count = $collection->getSize();
        }
        catch (Exception $e) {
            $count = 'N/A';
        }

        $this->setResponse($count);
    }
}