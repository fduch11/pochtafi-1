<?php

require_once("BaseController.php");

require_once('lib/tulli/Custom.php');

class Iwings_Pochta_CustomController extends Iwings_Pochta_BaseController
{

    public function indexAction()
    {
		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function prepareFITDECAction()
    {
        $parcel_id = $this->getRequest()->getParams()['id'];
        Mage::register('parcel_id', $parcel_id);

        /** @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')->load($parcel_id);

        /** @var $declarations Mage_Catalog_Model_Resource_Product_Link_Product_Collection */
        $declarations = $product->getCustomProductCollection()
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_declaration'))
            ->addAttributeToFilter('name', array('like' => '%FITDEC%'))
            ->setOrder('date', 'asc');
        $fitdec = $declarations->getLastItem();

        Mage::register('custom_id', $fitdec->getId());

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function postPrepareFITDECAction() {

        $parcel_id = $_POST['parcel_id'];

        /** @var $product Mage_Catalog_Model_Product */
        $product = Mage::getModel('catalog/product')->load($parcel_id);

        // fictive movement
        $movement = Mage::getModel('catalog/product');
        $movement
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
            ->setSku($product->getSku());


        $place = Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::getSingleton('core/session')->getSelectedStoreId());
        $movement
            ->setMvtSrcWhs($place)
            ->setMvtDstWhs($_POST['cs_transit_destination_office'])
            ->setMvtDstPlannedDate($_POST['cs_transit_limit_date'])
        ;

        $movement
            ->setTransportModeCode($_POST['transport_mode_code'])
            ->setTransportNationalityCode($_POST['transport_nationality_code'])
            ->setTransportMeansId($_POST['transport_means_id'])
            ->setConveyanceReferenceId($_POST['conveyance_reference_id'])
        ;

        $movement
            ->setCarrierNameFirst($_POST['carrier_name_first'])
            ->setCarrierNameLast($_POST['carrier_name_last'])
            ->setConsigneeAddressLine($_POST['consignee_address_line'])
            ->setConsigneeAddressCity($_POST['consignee_address_city'])
            ->setConsigneeAddressCountry($_POST['consignee_address_country'])
            ->setConsigneeAddressPostcode($_POST['consignee_address_postcode'])
        ;


        $link = Mage::getModel('catalog/product_link');
        $link->setProductId($parcel_id);
        $productsLinkedAsCrossSell[] = $link;

        $fitdec_body = Custom::buildFITDEC($movement, $productsLinkedAsCrossSell);
        Mage::getSingleton('core/session')->setFitdecBody($fitdec_body);

        $this->_redirect('pochta/custom/showFITDEC/parcel_id/'.$parcel_id);
    }

    public function showFITDECAction() {

        $parcel_id = $this->getRequest()->getParams()['parcel_id'];
        Mage::register('parcel_id', $parcel_id);

        $fitdec_body = Mage::getSingleton('core/session')->getFitdecBody();
        Mage::register('fitdec_body', $fitdec_body);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function sendFITDECAction() {
        $parcel_id = $_POST['parcel_id'];

        $fitdec_body = $_POST['fitdec_body'];
        $declaration = Custom::sendDeclaration($fitdec_body);

        /** @var $parcel Mage_Catalog_Model_Product */
        $parcel = Mage::getModel('catalog/product')->load($parcel_id);
        $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        $relation_data = Mage::helper('pochta')->getCustomData($parcel);
        $relation_data[$declaration->getId()] = array('position' => 0);
        $parcel->setCustomLinkData($relation_data);

        $parcel->save();

        $this->_redirect('pochta/custom/showFITDECResult/custom_id/' . $declaration->getId());
    }

    public function showFITDECResultAction() {
        $custom_id = $this->getRequest()->getParams()['custom_id'];
        Mage::register('custom_id', $custom_id);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function renewStatusPostAction()
    {
        $renewType = $_POST['renewType'];

        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];

        $response = Custom::globalRenew($renewType, $startDate, $endDate);
        Mage::getSingleton('core/session')->setCustomResponse($response);

        $this->_redirect('pochta/custom/list');
    }

    public function listAction()
    {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function viewAction() {

        $custom_id = $this->getRequest()->getParams()['id'];
        Mage::register('custom_id', $custom_id);

        $this->loadLayout('pochta');
        $this->renderLayout();

    }

}