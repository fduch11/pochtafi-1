<?php

require_once("BaseController.php");

class Iwings_Pochta_ParcelreceptionController extends Iwings_Pochta_BaseController
{
    public function indexAction()
    {
        $this->initAttributeSets();

//        /** @var $category Mage_Catalog_Helper_Product */
//        $productHelper = Mage::helper('catalog/product');
//        if (!$params) {
//            $params = new Varien_Object();
//        }
//
//        $product = $productHelper->initProduct($productId, $controller, $params);

        Mage::getSingleton('core/session')->unsCurrentProductId();
        Mage::getSingleton('core/session')->unsCurrentArrivalId();
        Mage::getSingleton('core/session')->unsReceptionType();

		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function moveAction()
    {
        $reception_type = $this->getRequest()->getParams()['reception_type'];
        Mage::getSingleton('core/session')->setReceptionType($reception_type);

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function postMoveAction()
    {
        $this->initAttributeSets();

        $receptionTime = strtotime('now');
        $reception_type = Mage::getSingleton('core/session')->getReceptionType();
        switch ($reception_type) {
            case Mage::registry('reception_type_portland'):
            case Mage::registry('reception_type_vantaa'):
            case Mage::registry('reception_type_lappeenranta'):
            case Mage::registry('reception_type_imatra'):
                $mvt_dst_whs = Mage::getResourceSingleton('catalog/product')->getAttribute('reception_type')->getSource()->getOptionText($reception_type);
                break;
            case Mage::registry('reception_type_eu_lappeenranta'):
                $mvt_dst_whs = Mage::getResourceSingleton('catalog/product')->getAttribute('reception_type')->getSource()->getOptionText(Mage::registry('reception_type_lappeenranta'));
                break;
            case Mage::registry('reception_type_eu_imatra'):
                $mvt_dst_whs = Mage::getResourceSingleton('catalog/product')->getAttribute('reception_type')->getSource()->getOptionText(Mage::registry('reception_type_imatra'));
                break;
            case Mage::registry('reception_type_na'):
                $mvt_dst_whs = 'just register';
                break;
        }

        try{

            /** @var $arrival Mage_Catalog_Model_Product */
            $arrival = Mage::getModel('catalog/product');
            $arrival
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                ->setTypeId('virtual')                              //product type
                ->setSku(uniqid('sku'))
                ->setName('Move from External to ' . $arrival->getResource()->getAttribute('reception_type')->getSource()->getOptionText($reception_type))
                ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//                ->setVisibility(?)
                ->setCreatedAt($receptionTime)                    //product creation time

                ->setWebsiteIds(array($this->backendSiteId))        //website ID the product is assigned to, as an array
                ->setAttributeSetId($this->movementSetId)            //ID of a attribute set named 'Arrival'

                ->setReceptionType($reception_type)
                ->setCarrierNameFirst($_POST['carrier_name_first'])
                ->setCarrierNameLast($_POST['carrier_name_last'])
                ->setComment($_POST['comment'])

                ->setTransportModeCode($_POST['transport_mode_code'])
                ->setTransportNationalityCode($_POST['transport_nationality_code'])
                ->setTransportMeansId($_POST['transport_means_id'])
                ->setConveyanceReferenceId($_POST['conveyance_reference_id'])

                ->setMvtSrcWhs('external')
                ->setMvtSrcStatus(Mage::registry('mvt_src_status_left'))
                ->setMvtSrcActualDate($_POST['carrier_date_out'])
                ->setMvtSrcPlannedDate()

                ->setMvtDstWhs($mvt_dst_whs)
                ->setMvtDstStatus(Mage::registry('mvt_dst_status_complete'))
                ->setMvtDstActualDate($_POST['carrier_date_in'])
            ;
            //$arrival->save();

            /** @var $previousDocument Mage_Catalog_Model_Product */
            $previousDocument = Mage::getModel('catalog/product');
            $previousDocument
                ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                ->setTypeId('virtual')                              //product type
                ->setSku(uniqid('sku'))
                ->setName('Document, Type: ' . $_POST['prev_document_typecode'] . ' Number: ' . $_POST['prev_document_id'])
                ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//                ->setVisibility(?)
                ->setCreatedAt($receptionTime)                    //product creation time

                ->setWebsiteIds(array($this->backendSiteId))        //website ID the product is assigned to, as an array
                ->setAttributeSetId($this->documentTypeSetId)       //ID of a attribute set named 'PD'

                ->setPrevDocumentId($_POST['prev_document_id'])
                ->setPrevDocumentTypecode($_POST['prev_document_typecode'])
                ->setDate($receptionTime)
            ;
            $previousDocument->save();

            $prevDocumentsIds[] = $previousDocument->getId();
            $relation_data = array();
            foreach ($prevDocumentsIds as $prevDocumentsId) {
                $relation_data[$prevDocumentsId] = array('position' => 0);
            }
            $arrival->setRelatedLinkData($relation_data);

            $arrival->save();


            // save values for following methods
            Mage::getSingleton('core/session')->setCurrentArrivalId($arrival->getId());

        } catch(Exception $e){
            Mage::log($e->getMessage());
        }

        $this->_redirect('pochta/parcelreception/getParcelSku');
    }

    public function getParcelSkuAction() {
        Mage::getSingleton('core/session')->unsCurrentProductId();

        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function setParcelSkuAction() {
        $id_type = $_POST['id_type'];

        Mage::getSingleton('core/session')->unsCurrentTracking();

        if ($id_type == 'SKU') {
            $parcel_sku = $_POST["parcel_id"];
            $parcel_id = Mage::getModel('catalog/product')->getIdBySku($parcel_sku);
        } else {
            $parcel_track = $_POST["parcel_id"];
            Mage::getSingleton('core/session')->setCurrentTracking($parcel_track);

            $parcel = Mage::getModel('catalog/product')->loadByAttribute('box_tracking_num', $parcel_track);
            if ($parcel != null) {
                $parcel_id = $parcel->getId();
            } else {
                $parcel = Mage::getModel('catalog/product')->loadByAttribute('box_original_tracking', $parcel_track);
                if ($parcel != null) {
                    $parcel_id = $parcel->getId();
                }
            }
        }

        Mage::getSingleton('core/session')->setCurrentProductId($parcel_id);

        // TODO: temporary skip photo
        $this->_redirect('pochta/parcelreception/step3');
    }

//    public function step1Action()
//    {
//        $this->loadLayout('pochta');
//        $this->renderLayout();
//    }

    public function step3Action()
    {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function createParcelAction()
    {
        $this->initAttributeSets();

        $arrivalId = Mage::getSingleton('core/session')->getCurrentArrivalId();
        if ($arrivalId == null) {
            Mage::log("createParcelAction: session expired", Zend_log::EMERG);
        }

        $arrival = Mage::getModel('catalog/product')->load($arrivalId);
        $receptionType = $arrival->getReceptionType();
        $receptionTime = $arrival->getCreatedAt();
        $documentLinks = $arrival->getRelatedLinkCollection();

        $parcel_id = Mage::getSingleton('core/session')->getCurrentProductId();

        try{

            /** @var $product Mage_Catalog_Model_Product */
            $product = Mage::getModel('catalog/product');

            if ($parcel_id != null) {
                $product->load($parcel_id);
                $product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

                // TODO: multiple parcels in reception
                if (Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText($product->getBoxStore()) != $arrival->getMvtSrcWhs()) {
                    $arrival->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $arrival->setMvtSrcWhs(Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText($product->getBoxStore()));
                    //$arrival->setMvtSrcStatus(Mage::registry('mvt_src_status_left'));
                    //$arrival>setMvtSrcActualDate($_POST['carrier_date_out']);
                    $arrival->setName('Move from ' . $arrival->getMvtSrcWhs() . ' to ' . $arrival->getMvtDstWhs());
                    $arrival->save();
                }
            } else {
                $product
                    ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
                    ->setWebsiteIds(array($this->backendSiteId, $this->frontendSiteId)) //website ID the product is assigned to, as an array
                    ->setAttributeSetId($this->parcelSetId) //ID of a attribute set named 'Parcel'
                    ->setTypeId('bundle') //product type
                    ->setCreatedAt($receptionTime) //product creation time
                    ->setStatus(1) //product status (1 - enabled, 2 - disabled)

                    ->setSkuType(1)                                                         // sku type (0 - dynamic, 1 - fixed)
                    ->setPriceType(Mage_Bundle_Model_Product_Price::PRICE_TYPE_DYNAMIC)     // price type (0 - dynamic, 1 - fixed)
                    ->setPriceView(1)                                                       // price view (0 - price range, 1 - as low as)
                    ->setWeightType(1)                                                      // weight type (0 - dynamic, 1 - fixed)
                    ->setShipmentType(Mage_Bundle_Model_Product_Type::SHIPMENT_TOGETHER)    // shipment type (0 - together, 1 - separately)


                    ->setName('Parcel')
                    ->setSku(uniqid('sku'))

//                ->setStatePhysical(?)               // default value is enough here
//                ->setStateLogistic(?)               // default value is enough here
//                ->setStateCustom(?)                 // default value is enough here
//                ->setStatePayment(?)                // default value is enough here
//                ->setStateControl(?)                // default value is enough here

                    ->setMediaGallery(array('images' => array())) //media gallery initialization
//                ->addImageToMediaGallery('media/catalog/product/pony.gif', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery

//                ->setCategoryIds(array($categoryId, $inboxCategoryId))      //assign product to categories
                ;

                // HACK: prepare data for service fees
                $default_service_fee = Mage::getModel('core/variable')->setStoreId(Mage::app()->getStore()->getId())->loadByCode('default_cost')->getValue('text');
                if (Mage::getModel('catalog/product')->loadByAttribute('box_suite_num', $_POST['box_suite_num']) === false) {
                    $default_service_fee = 0.0;
                }
                $_POST['sfee_id'] = array('');
                $_POST['sfee_name'] = array('Service Fee');
                $_POST['sfee_user_price'] = array($default_service_fee);
                $_POST['sfee_cancelable'] = array();
                $_POST['sfee_price'] = array($default_service_fee);
                $_POST['sfee_type'] = array(Mage::registry('fee_type_base'));
                if ($receptionType == Mage::registry('reception_type_portland') || $receptionType == Mage::registry('reception_type_vantaa')) {
                    // deposit for overseas parcels
                    $depositFeeValue = $this->getOverallParcelPrice() * 0.3;
                    $_POST['sfee_id'][] = '';
                    $_POST['sfee_name'][] = 'Deposit';
                    $_POST['sfee_user_price'][] = $depositFeeValue;
                    $_POST['sfee_cancelable'][] = 1;    // set Deposit index
                    $_POST['sfee_price'][] = $depositFeeValue;
                    $_POST['sfee_type'][] = Mage::registry('fee_type_deposit');
                }
            }

            $product
                ->setComment($_POST['comment']);

            switch ($receptionType) {
                case Mage::registry('reception_type_portland'):
                    $product->setBoxStore(Mage::registry('box_store_portland'));
                    $product->setWarehouseType(Mage::registry('warehouse_type_vv'));    // VV
                    $product->setBoxOrigin(Mage::registry('box_origin_overseas'));
                    break;
                case Mage::registry('reception_type_vantaa'):
                    $product->setBoxStore(Mage::registry('box_store_vantaa'));
                    $product->setWarehouseType(Mage::registry('warehouse_type_vv'));    // VV
                    $product->setBoxOrigin(Mage::registry('box_origin_overseas'));
                    break;
                case Mage::registry('reception_type_lappeenranta'):
                    $product->setBoxStore(Mage::registry('box_store_lappeenranta'));
                    if ($parcel_id == null) {
                        $product->setWarehouseType(Mage::registry('warehouse_type_eu'));    // EU
                        $product->setBoxOrigin(Mage::registry('box_origin_european'));
                    }
                    break;
                case Mage::registry('reception_type_imatra'):
                    $product->setBoxStore(Mage::registry('box_store_imatra'));
                    if ($parcel_id == null) {
                        $product->setWarehouseType(Mage::registry('warehouse_type_eu'));    // EU
                        $product->setBoxOrigin(Mage::registry('box_origin_european'));
                    }
                    break;
                case Mage::registry('reception_type_eu_lappeenranta'):
                    $product->setBoxStore(Mage::registry('box_store_lappeenranta'));
                    $product->setWarehouseType(Mage::registry('warehouse_type_eu'));    // EU
                    $product->setBoxOrigin(Mage::registry('box_origin_european'));
                    break;
                case Mage::registry('reception_type_eu_imatra'):
                    $product->setBoxStore(Mage::registry('box_store_imatra'));
                    $product->setWarehouseType(Mage::registry('warehouse_type_eu'));    // EU
                    $product->setBoxOrigin(Mage::registry('box_origin_european'));
                    break;
                case Mage::registry('reception_type_na'):
                    $product->setBoxStore(Mage::registry('box_store_na'));
                    $product->setWarehouseType(Mage::registry('warehouse_type_eu'));    // EU
                    $product->setBoxOrigin(Mage::registry('box_origin_european'));
                    break;
            }

            $relation_data = Mage::helper('pochta')->getMovementData($product);
            $relation_data[$arrivalId] = array('position' => 0);
            $product->setCrossSellLinkData($relation_data);

            $relation_data = Mage::helper('pochta')->getRelatedDocsData($product);
            foreach ($documentLinks as $documentLink) {
                $relation_data[$documentLink->getLinkedProductId()] = array('position' => 0);
            }
            $product->setRelatedLinkData($relation_data);

            //$product->save();

            parent::saveParcelAction($product);

            Mage::getSingleton('core/session')->setCurrentProductId($product->getId());

        } catch(Exception $e){
            Mage::log($e->getMessage());
        }

        $this->_redirect('pochta/parcelreception/step4');
    }

    public function step4Action()
    {
		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function nextParcelAction()
    {
        Mage::getSingleton('core/session')->unsCurrentProductId();

        // TODO: temporary skip photo
        $this->_redirect('pochta/parcelreception/getParcelSku');
    }

    public function endReceptionAction()
    {
        Mage::getSingleton('core/session')->unsCurrentProductId();
        Mage::getSingleton('core/session')->unsCurrentArrivalId();

        $this->_redirect('pochta/index');
    }

    private function getOverallParcelPrice() {

        $result = 0.0;

        for ($i = 0; $i < 5; $i++) {
            if ($_POST['sproduct_name'][$i] == "") continue;

            $rate = Mage::helper('directory')->currencyConvert(1, 'EUR', $_POST['sproduct_user_currency'][$i]);
            $result += $_POST['sproduct_user_price'][$i] / $rate * round($_POST['sproduct_qty'][$i]);
        }

        return $result;
    }
}