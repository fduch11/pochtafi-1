<?php

require_once("BaseController.php");

class Iwings_Pochta_TakeoutController extends Iwings_Pochta_BaseController
{
    public function indexAction()
    {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function searchAction()
    {
        Mage::getSingleton('core/session')->unsCurrentParcels();

		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function processTakeoutAction() {

        if (Mage::getSingleton('core/session')->getCurrentParcels() == null) {
            $parcels = array();
        } else {
            $parcels = Mage::getSingleton('core/session')->getCurrentParcels();
        }

        if (!empty($parcels)) {
            $this->_redirect('pochta/takeout/move');
        } else {
            $this->_redirect('pochta/takeout/index');
        }

    }

    public function moveAction()
    {
        $this->loadLayout('pochta');
        $this->renderLayout();
    }

    public function postMoveAction()
    {
        $this->initAttributeSets();

        $parcels = Mage::getSingleton('core/session')->getCurrentParcels();

        if (!$parcels) {

        }

        $movementInfo = array();
        $movementInfo['carrier_name_first'] = $_POST['carrier_name_first'];
        $movementInfo['carrier_name_last'] = $_POST['carrier_name_last'];
        $movementInfo['consignee_address_line'] = $_POST['consignee_address_city'];
        $movementInfo['consignee_address_city'] = $_POST['consignee_address_city'];
        $movementInfo['consignee_address_country'] = $_POST['consignee_address_country'];
        $movementInfo['consignee_address_postcode'] = $_POST['consignee_address_postcode'];

        $movementInfo['transport_mode_code'] = $_POST['transport_mode_code'];
        $movementInfo['transport_nationality_code'] = $_POST['transport_nationality_code'];
        $movementInfo['transport_means_id'] = $_POST['transport_means_id'];
        $movementInfo['borderline'] = $_POST['borderline'];
        $movementInfo['carrier_date_in'] = $_POST['carrier_date_in'];
        $movementInfo['comment'] = $_POST['comment'];

        $movementInfo['parcels'] = $parcels;

        $this->prepareMovement($movementInfo);


        $this->_redirect('pochta/takeout/finish');
    }

    public function finishAction()
    {
		$this->loadLayout('pochta');
		$this->renderLayout();
    }

    public function nextIssuanceAction()
    {
        Mage::getSingleton('core/session')->unsCurrentParcels();

        $this->_redirect('pochta/takeout/index');
    }

    public function endIssuanceAction()
    {
        Mage::getSingleton('core/session')->unsCurrentParcels();

        $this->_redirect('pochta/index');
    }
}