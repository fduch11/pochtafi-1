<?php

require_once("BaseController.php");
require_once 'lib/Braintree.php';

class Iwings_Pochta_PaymentsController  extends Iwings_Pochta_BaseController
{
    public function payBraintreeByQuoteAction() {

        $store = Mage::app()->getStore();

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();


        $quote = Mage::getModel('sales/quote');
        $quote->setStore($store);
        $quote->assignCustomer($customer);
        $quote->setIsVirtual(true);

        $product1 = Mage::getModel('catalog/product')->load(68);
        $buyInfo1 = array('qty' => 1);

        $product2 = Mage::getModel('catalog/product')->load(69);
        $buyInfo2 = array('qty' => 1);

        $product3 = Mage::getModel('catalog/product')->load(17);
        $buyInfo3 = array('qty' => 1);

//        $product2 = Mage::getModel('catalog/product')->load(18); /* Sony Ericsson W810i */
//        $buyInfo2 = array('qty' => 3);

        $quote->addProduct($product1, new Varien_Object($buyInfo1));
        $quote->addProduct($product2, new Varien_Object($buyInfo2));
        $quote->addProduct($product3, new Varien_Object($buyInfo3));

        $billingAddress = $quote->getBillingAddress()->addData($customer->getPrimaryBillingAddress());
        $shippingAddress = $quote->getShippingAddress()->addData($customer->getPrimaryShippingAddress());

        $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
            ->setShippingMethod('flatrate_flatrate')
            ->setPaymentMethod('braintree');

        $quote->getPayment()->importData(array('method' => 'braintree'));

        $quote->collectTotals()->save();

        $service = Mage::getModel('sales/service_quote', $quote);
        $service->submitAll();
        $order = $service->getOrder();

        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();

        $transaction = Mage::getModel('core/resource_transaction')
            ->addObject($invoice)
            ->addObject($invoice->getOrder());

        $transaction->save();

    }

    public function payBraintreeAction()
    {
//        $_POST['number'] = '4111 1111 1111 1111';
//        $_POST['month'] = '11';
//        $_POST['year'] = '15';
//        $_POST['cardholderName'] = 'SAD ASD';
//        $_POST['cvv'] = '111';
////        $_POST['deposit_amount'] = '93';
//        $_POST['service_amount'] = '15';
//        $_POST['parcelId'] = '12';

//        $_POST['carrier_name_first'] = 1;
//        $_POST['carrier_name_last'] = 6;
//        $_POST['consignee_address_line'] = 2;
//        $_POST['consignee_address_city'] = 3;
//        $_POST['consignee_address_country'] = 4;
//        $_POST['consignee_address_postcode'] = 5;

        //$this->createOrder($product, $_POST['deposit_amount'], $_POST['service_amount']);

        $websiteID = Mage::app()->getWebsite()->getId();

        $environment = Mage::app()->getWebsite($websiteID)->getConfig('payment/braintree/environment');
        $merchantId = Mage::app()->getWebsite($websiteID)->getConfig('payment/braintree/merchant_id');
        $publicKey = Mage::app()->getWebsite($websiteID)->getConfig('payment/braintree/public_key');
        $privateKey = Mage::app()->getWebsite($websiteID)->getConfig('payment/braintree/private_key');

        $encryption_key = Mage::app()->getWebsite($websiteID)->getConfig('payment/braintree/client_side_encryption_key');
        $title = Mage::getStoreConfig('payment/braintree/title');

        Braintree_Configuration::environment($environment);
        Braintree_Configuration::merchantId($merchantId);
        Braintree_Configuration::publicKey($publicKey);
        Braintree_Configuration::privateKey($privateKey);

        if ($_POST['deposit_amount'] > 0) {
            $transaction_deposit = Braintree_Transaction::sale(array(
                'amount' => $_POST['deposit_amount'],
                'creditCard' => array(
                    'cardholderName' => $_POST["cardholderName"],
                    'number' => $_POST["number"],
                    'expirationDate' => $_POST["month"] . '/' . $_POST["year"],
                    'cvv' => $_POST['cvv']
                ),
                'options' => array(//'submitForSettlement'    => true
                ),
                'customFields' => array(
                    'parcel_id' => $_POST['parcelId'],
                )
//            'customer' => array(
//                'firstName' => $firstname,
//                'lastName' => $lastname,
//                'id' => '1234567'
//            )
            ));
        }

        if ($_POST['service_amount'] > 0) {
            $transaction_service = Braintree_Transaction::sale(array(
                'amount' => $_POST['service_amount'],
                'creditCard' => array(
                    'cardholderName' => $_POST["cardholderName"],
                    'number' => $_POST["number"],
                    'expirationDate' => $_POST["month"] . '/' . $_POST["year"],
                    'cvv' => $_POST['cvv']
                ),
                'options' => array(
                    'submitForSettlement' => true
                ),
                'customFields' => array(
                    'parcel_id' => $_POST['parcelId'],
                )
//            'customer' => array(
//                'firstName' => $firstname,
//                'lastName' => $lastname,
//                'id' => '1234567'
//            )
	        ));
        }
		
        if (($_POST['deposit_amount'] == 0 || $transaction_deposit->success) && ($_POST['service_amount'] == 0 || $transaction_service->success)) {

            /** @var $transaction_deposit Braintree_Result_Successful */
            /** @var $transaction_service Braintree_Result_Successful */
            $product = Mage::getModel('catalog/product')->load($_POST['parcelId']);
            $product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

//            $product->setStatePayment(Mage::registry('state_payment_paid'));
//            $product->save();

            $payment_transactions = array();
            if ($transaction_service->success) {
                $payment_transactions[$transaction_service->transaction->id] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE;
            }
            if ($transaction_deposit->success) {
                $payment_transactions[$transaction_deposit->transaction->id] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            self::createOrder($product, $customer, $customer->getDefaultBillingAddress(), $payment_transactions);



            $successUrl = $this->getRequest()->getParam(self::PARAM_NAME_SUCCESS_URL);
            $routeParams = array('_query' => array());
            if (empty($successUrl)) {
                $successUrl = 'customer/account/paymentSuccess';
            } else {
                $successUrlParams = $this->getRequest()->getParam('success_url_params');
                if (!empty($successUrlParams)) {
                    foreach (explode('&', $successUrlParams) as $query) {
                        $parts = explode('=', $query);
                        $routeParams['_query'][$parts[0]] = $parts[1];
                    }
                }
            }
            $this->_redirect($successUrl, $routeParams);

        } else {
            $errorDesc = '';

            if ($transaction_deposit && $transaction_deposit->success) {
                $result_deposit_void = Braintree_Transaction::void($transaction_deposit->transaction->id);
            } else if ($transaction_deposit) {
                /** @var $transaction_deposit Braintree_Result_Error */
                $errorDesc .= PHP_EOL . $transaction_deposit->message;
                //$transaction_deposit->valueForHtmlField('cardholderName');
            }

            if ($transaction_service && $transaction_service->success) {
                $result_service_void = Braintree_Transaction::void($transaction_service->transaction->id);
            } else if ($transaction_service) {
                /** @var $transaction_service Braintree_Result_Error */
                $errorDesc .= PHP_EOL . $transaction_service->message;
                //$transaction_service->valueForHtmlField('cardholderName');
            }

            $errorDesc = trim($errorDesc);

            $errorUrl = $this->getRequest()->getParam(self::PARAM_NAME_ERROR_URL);
            $routeParams = array('_query' => array('errorDesc' => $errorDesc));
            if (empty($errorUrl)) {
                $errorUrl = 'customer/account/paymentError';
            } else {
                $errorUrlParams = $this->getRequest()->getParam('error_url_params');
                if (!empty($errorUrlParams)) {
                    foreach (explode('&', $errorUrlParams) as $query) {
                        $parts = explode('=', $query);
                        $routeParams['_query'][$parts[0]] = $parts[1];
                    }
                }
            }
            $this->_redirect($errorUrl, $routeParams);

        }

    }

    public static function createOrder($parcels, $customer, $billingAddress, $payment_transactions)
    {

        $store = Mage::app()->getStore();
        $storeId = $store->getId();


//        $storeId = $customer->getStoreId();
        $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);

        /** @var $order Mage_Sales_Model_Order */
        $order = Mage::getModel('sales/order')
            ->setIncrementId($reservedOrderId)
            ->setStoreId($storeId)
            ->setIsVirtual(true)
//            ->setQuoteId(0)
            ->setGlobal_currency_code('EUR')
            ->setBase_currency_code('EUR')
            ->setStore_currency_code('EUR')
            ->setOrder_currency_code('EUR');


        // set Customer data
        $order
            ->setCustomerEmail($customer->getEmail())
            ->setCustomerFirstname($customer->getFirstname())
            ->setCustomerMiddlename($customer->getMiddlename())
            ->setCustomerLastname($customer->getLastname())
            ->setCustomerDob($customer->getDob())
            ->setCustomerGender($customer->getGender())
            ->setCustomerGroupId($customer->getGroupId())
            ->setCustomerIsGuest(!$customer->getId())
            ->setCustomer($customer);

        // set Billing Address
        /** @var $billing Mage_Customer_Model_Address */
        $billing = $customer->getDefaultBillingAddress();
        if ($billing) {
            /** @var $billingAddress Mage_Sales_Model_Order_Address */
            $billingAddress = Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($billing->getId())
//                ->setCustomerAddressId($billing->getEntityId())
                ->setPrefix($billing->getPrefix())
                ->setFirstname($customer->getFirstname())
                ->setMiddlename($billing->getMiddlename())
                ->setLastname($customer->getLastname())
                ->setSuffix($billing->getSuffix())
                ->setCompany($billing->getCompany())
                ->setStreet($billing->getStreet())
                ->setCity($billing->getCity())
                ->setCountryId($billing->getCountryId())
                ->setRegion($billing->getRegion())
                ->setRegionId($billing->getRegionId())
                ->setPostcode($billing->getPostcode())
                ->setTelephone($billing->getTelephone())
                ->setFax($billing->getFax());

            $order->setBillingAddress($billingAddress);
        } else {
            // should be passed as argument

//            $billingAddress = Mage::getModel('sales/order_address')
//                ->setStoreId($storeId)
//                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
//                ->setCustomerId($customer->getId())
//                ->setFirstname($_POST['carrier_name_first'])
//                ->setLastname($_POST['carrier_name_last'])
//                ->setStreet($_POST['consignee_address_line'])
//                ->setCity($_POST['consignee_address_city'])
//                ->setCountryId($_POST['consignee_address_country'])
//                ->setPostcode($_POST['consignee_address_postcode']);

            $order->setBillingAddress($billingAddress);
        }


        /** @var $shipping Mage_Customer_Model_Address  */
        $shipping = $customer->getDefaultShippingAddress();
        if ($shipping && false) {    // NOTE: actually Virtual order does not need shipping address
            /** @var $shippingAddress Mage_Sales_Model_Order_Address  */
            $shippingAddress = Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($billing->getId())
//                ->setCustomerAddressId($billing->getEntityId())
                ->setPrefix($billing->getPrefix())
                ->setFirstname($customer->getFirstname())
                ->setMiddlename($billing->getMiddlename())
                ->setLastname($customer->getLastname())
                ->setSuffix($billing->getSuffix())
                ->setCompany($billing->getCompany())
                ->setStreet($billing->getStreet())
                ->setCity($billing->getCity())
                ->setCountryId($billing->getCountryId())
                ->setRegion($billing->getRegion())
                ->setRegionId($billing->getRegionId())
                ->setPostcode($billing->getPostcode())
                ->setTelephone($billing->getTelephone())
                ->setFax($billing->getFax());

            $order
                ->setShippingAddress($shippingAddress)
                //->setShipping_method('flatrate_flatrate');
                ->setShipping_method('freeshipping_freeshipping');
            $order->getShippingAddress()->setFreeShipping(1);
        }


        if (!is_array($parcels)) {
            $parcels = array($parcels);
        }

        $feesAmount = 0.0;

        foreach ($parcels as $parcelId) {

            $parcel = Mage::getModel('catalog/product')->load($parcelId);

            $declaredGoodsPrice = 0.0;

            $bundleItems = Iwings_Pochta_BaseController::getBundledData($parcel);
            foreach ($bundleItems as $bundleItem) {
                $orderItem = Mage::getModel('sales/order_item')
                    ->setStoreId($storeId)
//                ->setQuoteItemId(0)
//                ->setQuoteParentItemId(NULL)
                    ->setProductId($bundleItem->getId())
                    ->setProductType($bundleItem->getTypeId())
                    ->setIsQtyDecimal(false)
                    ->setQtyBackordered(NULL)
                    ->setTotalQtyOrdered($bundleItem->getSelectionQty())
                    ->setQtyOrdered($bundleItem->getSelectionQty())
                    ->setName($bundleItem->getName())
                    ->setSku($bundleItem->getSku())
                    ->setPrice($bundleItem->getPrice())
                    ->setBasePrice($bundleItem->getPrice())
                    ->setOriginalPrice($bundleItem->getPrice())
                    ->setRowTotal(0)//$bundleItem->getPrice() * $bundleItem->getSelectionQty())
                    ->setBaseRowTotal(0)//$bundleItem->getPrice() * $bundleItem->getSelectionQty())
                ;

                $declaredGoodsPrice += $bundleItem->getPrice();

                $order->addItem($orderItem);
            }

            $orderItem = Mage::getModel('sales/order_item')
                ->setStoreId($storeId)
//                ->setQuoteItemId(0)
//                ->setQuoteParentItemId(NULL)
                ->setProductId($parcel->getId())
                ->setProductType($parcel->getTypeId())
                ->setIsQtyDecimal(false)
                ->setQtyBackordered(NULL)
                ->setTotalQtyOrdered(1)
                ->setQtyOrdered(1)
                ->setName($parcel->getName())
                ->setSku($parcel->getSku())
                ->setPrice($declaredGoodsPrice)
                ->setBasePrice($declaredGoodsPrice)
                ->setOriginalPrice($declaredGoodsPrice)
                ->setRowTotal(0)//$simpleProduct->getPrice() * $simpleProduct->getQty())
                ->setBaseRowTotal(0)//$simpleProduct->getPrice() * $simpleProduct->getQty())
            ;
            $order->addItem($orderItem);


            $parcel->getLinkInstance()->useUpSellLinks();
            foreach ($parcel->getUpSellProductCollection() as $serviceFee) {
                $serviceFee->load($serviceFee->getId());

                $orderItem = Mage::getModel('sales/order_item')
                    ->setStoreId($storeId)
//                ->setQuoteItemId(0)
//                ->setQuoteParentItemId(NULL)
                    ->setProductId($serviceFee->getId())
                    ->setProductType($serviceFee->getTypeId())
                    ->setQtyBackordered(NULL)
                    ->setTotalQtyOrdered(1)
                    ->setQtyOrdered(1)
                    ->setName($serviceFee->getName())
                    ->setSku($serviceFee->getSku())
                    ->setPrice($serviceFee->getUserPrice())
                    ->setBasePrice($serviceFee->getPrice())
                    ->setOriginalPrice($serviceFee->getPrice())
                    ->setRowTotal($serviceFee->getUserPrice())
                    ->setBaseRowTotal($serviceFee->getUserPrice());

                $order->addItem($orderItem);

                $feesAmount += $serviceFee->getUserPrice();
            }

        }   // foreach ($parcels)

        $order->setSubtotal($feesAmount)
            ->setBaseSubtotal($feesAmount)
            ->setGrandTotal($feesAmount)
            ->setBaseGrandTotal($feesAmount);


        foreach ($payment_transactions as $payment_transaction_id => $transaction_status) {
            /** @var $orderPayment Braintree_Payments_Model_Rewrite_Sales_Order_Payment  */
            $orderPayment = Mage::getModel('sales/order_payment')
                ->setStoreId($storeId)
                ->setCustomerPaymentId('0')
                ->setMethod('paypal_standard')
                ->setTransactionId($payment_transaction_id)
                ->setParentTransactionId(null)
//                ->setAnetTransType('VOID')
                //->setSkipTransactionCreation(true)
//                ->setXTransId($payment_transaction)
//                ->setLastTransId($payment_transaction)
//                ->setCcTransId($payment_transaction)
                ->setIsTransactionClosed(0);
//                ->setPo_number(' – ');

            $order->addPayment($orderPayment);

            if ($payment_transaction_id) {
                $transaction = $orderPayment->addTransaction($transaction_status);
            }
        }


        $order->save();


        try {
            if (!$order->canInvoice()) {
                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
                echo "Cannot create an invoice.";
            }

            $refundable_invoice = array();
            $non_refundable_invoice = array();

            $items = $order->getItemsCollection();
            foreach ($items as $item) {
                if ($item->getProductType() == 'virtual' && $item->getProduct()->getAttributeSetId() == Mage::registry('attr_set_id_fee')) {
                    if ($item->getProduct()->getIsCancelable()) {
                        $refundable_invoice[$item->getId()] = $item->getQtyOrdered();
                    } else {
                        $non_refundable_invoice[$item->getId()] = $item->getQtyOrdered();
                    }
                } else {
                    $refundable_invoice[$item->getId()] = $item->getQtyOrdered();
                    $non_refundable_invoice[$item->getId()] = $item->getQtyOrdered();
                }
            }


            if (array_search(Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, $payment_transactions) !== false)
            {
                // save non refundable

                /** @var $invoiceApi Mage_Sales_Model_Order_Invoice_Api */
                $invoiceApi = Mage::getModel('sales/order_invoice_api');

                $invoiceIncrementId = $invoiceApi->create($order->getIncrementId(), $non_refundable_invoice);

                /** @var $invoice Mage_Sales_Model_Order_Invoice */
                $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceIncrementId);
                $invoice->pay();
                $invoice->setTransactionId(array_search(Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, $payment_transactions));
                $invoice->save();
                //$invoiceApi->capture($invoiceIncrementId);
            }

            if (array_search(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH, $payment_transactions) !== false)
            {
                // save refundable

                /** @var $invoiceApi Mage_Sales_Model_Order_Invoice_Api */
                $invoiceApi = Mage::getModel('sales/order_invoice_api');

                $invoiceIncrementId = $invoiceApi->create($order->getIncrementId(), $refundable_invoice);

                /** @var $invoice Mage_Sales_Model_Order_Invoice */
                $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceIncrementId);
                $invoice->pay();
                $invoice->setTransactionId(array_search(Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH, $payment_transactions));
                $invoice->save();
                //$invoiceApi->capture($invoiceIncrementId);
            }

/*
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($qtys);
//<!-- The rest is only required when handling a partial invoice as in this example-->
            $amount = $invoice->getGrandTotal();
            $invoice->register()->pay();
            $invoice->getOrder()->setIsInProcess(true);

            $history = $invoice->getOrder()->addStatusHistoryComment(
                'Partial amount of $' . $amount . ' captured automatically.', false
            );

            $history->setIsCustomerNotified(true);

            $order->save();

            Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder())
                ->save();
*/

//            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
//
//            if (!$invoice->getTotalQty()) {
//                Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
//                echo "Cannot create an invoice without products.";
//            }
//
//            $invoice->setRequestedCaptureCase($captureCase);
//            //$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
//            $invoice->register();
//            $invoice->getOrder()->setCustomerNoteNotify(true);
//            $invoice->getOrder()->setIsInProcess(true);
//            $invoice->sendEmail();
//            $transaction = Mage::getModel('core/resource_transaction');
//            $transaction
//                ->addObject($invoice)
//                ->addObject($invoice->getOrder());
//            $transaction->save();
        } catch (Mage_Core_Exception $e) {
            echo "invoice can't created";
        }

        $order->setData('state', "complete");
        $order->setStatus("complete");
        //$order->setState(Mage_Sales_Model_Order::STATE_COMPLETE, true);
        $order->save();

        return $order->getId();

    }

    public function printInvoiceAction() {
        if ($invoiceId = $this->getRequest()->getParam('invoice_id')) {
            if ($invoice = Mage::getModel('sales/order_invoice')->load($invoiceId)) {
                $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice));
                $this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').
                    '.pdf', $pdf->render(), 'application/pdf');
            }
        }
        else {
            $this->_forward('noRoute');
        }
    }

    public function billingInfoPostAction() {

        $parcelId = $_POST['parcelId'];

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $customer
            ->setFirstname($_POST['firstname'])
            ->setLastname($_POST['lastname']);

        $billing = $customer->getDefaultBillingAddress();

        if (!$billing) {
            $billing = Mage::getModel('customer/address');
            $billing
                ->setCustomerId($customer->getId())
                ->setIsDefaultBilling('1');
        }

        $billing
            ->setFirstname($_POST['firstname'])
            ->setLastname($_POST['lastname'])
            ->setStreet(array($_POST['consignee_address_line']))
            ->setCity($_POST['consignee_address_city'])
            ->setCountry($_POST['consignee_address_country'])
            ->setCountryId($_POST['consignee_address_country'])
            ->setPostcode($_POST['consignee_address_postcode']);

        $billing->save();
        $customer->save();


        $payMethod = 'paypal';
        //$payMethod = 'braintree';

        if ($payMethod == 'braintree') {
            $this->_redirect('customer/account/paymentinfo/', array('_query' => array('parcelId' => $parcelId)));
        } elseif ($payMethod = 'paypal') {
            $this->_redirect('pochta/payments/payPaypal/', array('_query' => array('parcelId' => $parcelId)));
        }

    }

    public function payPaypalAction() {

        $parcelId = $this->getRequest()->getParam('parcelId');

//        $storeId = Mage::app()->getStore()->getId();
//        $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
//        $order_id = $reservedOrderId;

        $order_id = null;
        Mage::register('order_id', $order_id);

        $this->loadLayout();
        $this->renderLayout();

    }

    public function paypalSuccessPostAction() {

        $parcelId = $this->getRequest()->getParam('custom');

        $parcel = Mage::getModel('catalog/product')->load($parcelId);
        $parcel->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        $parcel->setStatePayment(Mage::registry('state_payment_paid'));
        $parcel->save();

        $payment_transactions = array();
        $payment_transactions[$_POST['txn_id']] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE; // TODO: HACK
        //$payment_transactions[$_POST['txn_id']] = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $order_id = self::createOrder($parcelId, $customer, $customer->getDefaultBillingAddress(), $payment_transactions);

        $this->_redirect('customer/account/paymentSuccess/');

    }

    public function paypalErrorPostAction() {

        $this->_redirect('customer/account/paymentError/');

    }



}