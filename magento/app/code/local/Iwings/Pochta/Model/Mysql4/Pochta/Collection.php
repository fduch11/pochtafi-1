<?php

class Iwings_Pochta_Model_Mysql4_Pochta_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('pochta/pochta');
    }
}