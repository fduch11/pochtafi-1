<?php

class Iwings_Actionaudit_Block_Adminhtml_Actionaudit_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('actionauditGrid');
        $this->setDefaultSort('log_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        /** @var $attrCollection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
        $attrCollection = Mage::getResourceModel('catalog/product_attribute_collection');
        $attrCollection
            ->addFilter('is_user_defined', 1);

        $sortColumn = $this->getParam($this->getVarNameSort(), $this->_defaultSort);
        $sortDir    = $this->getParam($this->getVarNameDir(), $this->_defaultDir);

        /** @var $collection Iwings_Actionaudit_Model_Mysql4_Actionaudit_Collection */
        $raw_collection = Mage::getModel('actionaudit/actionaudit')->getCollection();
        if ($sortColumn != 'log_comments') {
            $raw_collection->setOrder($sortColumn, $sortDir);
        }
//        $raw_collection->load();

        $collection = new Varien_Data_Collection();
        foreach ($raw_collection as $item) {
            /** @var $item Iwings_Actionaudit_Model_Actionaudit */
            $json_old = $item->getJsonOld();
            $json_new = $item->getJsonNew();

            $obj_old = json_decode($json_old);
            $obj_new = json_decode($json_new);

            $item['source_id'] = $obj_new->entity_id;

            foreach ($attrCollection as $attribute) {
                /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
//                if (in_array('bundle', $attribute->getApplyTo()))
                {
                    $attrCode = $attribute->getAttributeCode();
                    $attr_old = $obj_old->$attrCode;
                    $attr_new = $obj_new->$attrCode;

                    if ($attribute->getSourceModel() != null) {
                        $attr_old = $attr_old == null ? null : $attribute->getSource()->getOptionText($attr_old);
                        $attr_new = $attr_new == null ? null : $attribute->getSource()->getOptionText($attr_new);
                    }

                    if ($attr_old != $attr_new) {
                        $item['log_comments'] .= $attribute->getFrontendLabel() . ': ' . ($attr_old == null ? '(empty)' : $attr_old) . ' => ' . ($attr_new == null ? '(empty)' : $attr_new) . '<br/>';
                    } else if ($item->getLogAction() == 'Deleted') {
                        $item['log_comments'] .= $attribute->getFrontendLabel() . ': ' . ($attr_old == null ? '(empty)' : $attr_old) . ' => ' . '<br/>';
                    }
                }
            }

//            if (isset($item['log_comments'])) {
                $collection->addItem($item);
//            }
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('log_id', array(
            'header' => Mage::helper('actionaudit')->__('ID'),
            'align' => 'left',
            'index' => 'log_id',
            'width' => '20px'
        ));
        $this->addColumn('log_date', array(
            'header' => Mage::helper('actionaudit')->__('Date'),
            'align' => 'left',
            'type' => 'datetime',
            'index' => 'log_date',
            'width' => '180px',
//            'type' => 'input'
        ));
        $this->addColumn('log_entity', array(
            'header' => Mage::helper('actionaudit')->__('Entity'),
            'align' => 'left',
            'index' => 'log_entity',
            'width' => '180px'
        ));
        $this->addColumn('log_action', array(
            'header' => Mage::helper('actionaudit')->__('Action'),
            'align' => 'left',
            'index' => 'log_action',
            'width' => '180px'
        ));
        $this->addColumn('log_comments', array(
            'header' => Mage::helper('actionaudit')->__('Details'),
            'align' => 'left',
            'index' => 'log_comments',
            'type'  => 'text',
            'filter'=> false,
            'sortable' => false
        ));
        $this->addColumn('updated_by', array(
            'header' => Mage::helper('actionaudit')->__('Updated By'),
            'align' => 'left',
            'index' => 'updated_by',
        ));
        $this->addColumn('adminrole', array(
            'header' => Mage::helper('actionaudit')->__('Role'),
            'align' => 'left',
            'index' => 'adminrole',
        ));
        $this->addColumn('website', array(
            'header' => Mage::helper('actionaudit')->__('Website'),
            'align' => 'left',
            'index' => 'website',
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('actionaudit')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('actionaudit')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('log_id');
        $this->getMassactionBlock()->setFormFieldName('actionaudit');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('actionaudit')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('actionaudit')->__('Are you sure?')
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        //return Mage::helper('adminhtml')->getUrl('catalog_product/edit', array('id' => $row->getSourceId()));

        // 'adminhtml' is a magic word which refers to base admin interface url
        return $this->getUrl('adminhtml/catalog_product/edit', array('id' => $row->getSourceId()));
    }
}