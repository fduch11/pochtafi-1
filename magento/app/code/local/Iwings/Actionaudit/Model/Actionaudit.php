<?php

class Iwings_Actionaudit_Model_Actionaudit extends Mage_Core_Model_Abstract
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix     = 'actionaudit';

    public function _construct()
    {
        parent::_construct();
        $this->_init('actionaudit/actionaudit');
    }
}