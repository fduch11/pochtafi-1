<?php

class Iwings_Actionaudit_Model_Mysql4_Actionaudit_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('actionaudit/actionaudit');
    }
}