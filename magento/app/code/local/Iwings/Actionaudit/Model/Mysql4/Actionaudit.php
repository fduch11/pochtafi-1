<?php

class Iwings_Actionaudit_Model_Mysql4_Actionaudit extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        // Note that the actionaudit_id refers to the key field in your database table.
        $this->_init('actionaudit/actionaudit', 'log_id');
    }
}