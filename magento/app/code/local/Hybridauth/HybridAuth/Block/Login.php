<?php
class hybridauth_HybridAuth_Block_Login extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _toHtml(){
		if(Mage::getSingleton('customer/session')->isLoggedIn() == 0){
			$current_url = Mage::getUrl('auth/index/auth');
			$this->assign('current_url', $current_url);
			return parent::_toHtml();
		}
		return '';
    }
} 