<?php
/**
 * @xmlNamespace 
 * @xmlType PartyType
 * @xmlName Notification
 * @var NotificationType
 */
class NotificationType {



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType $MovementReferenceID [optional] Movement reference number (MRN), issued by Customs.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\DateType $NotificationDate [optional] The arrival notification date.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\LanguageCodeType $DestinationLanguageCode [optional] The basic language to be used in any further communication between the Trader and the Customs system.
		@param fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType $TransitPresentationOffice [optional] Customs office of presentation, listed in permission for authorised consignee.
		@param fi\tulli\schema\external\ncts\dme\v1\GoodsLocationType $GoodsLocation [optional] Warehouse where goods can be inspected, listed in permit for authorised consignee.
		@param fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType $SealConditionIndicator [optional] Indicates (value 1) that seals are in order or seals have not been used (no mention of seals on accompanying document).
		@param fi\tulli\schema\external\ncts\dme\v1\EnRouteEventType $EnRouteEvent [optional] En route event (incident, transshipment or change in seals), if such an event has taken place.
		@param  $ActualAuthorisedConsignee [optional] Actual authorised consignee party.
		@param fi\tulli\schema\external\ncts\dme\v1\RepresentativePersonPartyType $RepresentativePerson [optional] Person lodging the declaration.
	*/                                                                        
	public function __construct($MovementReferenceID = null, $NotificationDate = null, $DestinationLanguageCode = null, $TransitPresentationOffice = null, $GoodsLocation = null, $SealConditionIndicator = null, $EnRouteEvent = null, $ActualAuthorisedConsignee = null, $RepresentativePerson = null)
	{
		$this->MovementReferenceID = $MovementReferenceID;
		$this->NotificationDate = $NotificationDate;
		$this->DestinationLanguageCode = $DestinationLanguageCode;
		$this->TransitPresentationOffice = $TransitPresentationOffice;
		$this->GoodsLocation = $GoodsLocation;
		$this->SealConditionIndicator = $SealConditionIndicator;
		$this->EnRouteEvent = $EnRouteEvent;
		$this->ActualAuthorisedConsignee = $ActualAuthorisedConsignee;
		$this->RepresentativePerson = $RepresentativePerson;
	}
	
	/**
	 * @Definition Movement reference number (MRN), issued by Customs.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MovementReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MovementReferenceIDType
	 */
	public $MovementReferenceID;
	/**
	 * @Definition The arrival notification date.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName NotificationDate
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $NotificationDate;
	/**
	 * @Definition The basic language to be used in any further communication between the Trader and the Customs system.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DestinationLanguageCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\LanguageCodeType
	 */
	public $DestinationLanguageCode;
	/**
	 * @Definition Customs office of presentation, listed in permission for authorised consignee.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitPresentationOffice
	 * @var fi\tulli\schema\external\ncts\dme\v1\CustomsOfficeContactType
	 */
	public $TransitPresentationOffice;
	/**
	 * @Definition Warehouse where goods can be inspected, listed in permit for authorised consignee.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GoodsLocation
	 * @var fi\tulli\schema\external\ncts\dme\v1\GoodsLocationType
	 */
	public $GoodsLocation;
	/**
	 * @Definition Indicates (value 1) that seals are in order or seals have not been used (no mention of seals on accompanying document).
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName SealConditionIndicator
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
	 */
	public $SealConditionIndicator;
	/**
	 * @Definition En route event (incident, transshipment or change in seals), if such an event has taken place.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs 9
	 * @xmlName EnRouteEvent
	 * @var fi\tulli\schema\external\ncts\dme\v1\EnRouteEventType
	 */
	public $EnRouteEvent;
	/**
	 * @Definition Actual authorised consignee party.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName ActualAuthorisedConsignee
	 */
	public $ActualAuthorisedConsignee;
	/**
	 * @Definition Person lodging the declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName RepresentativePerson
	 * @var fi\tulli\schema\external\ncts\dme\v1\RepresentativePersonPartyType
	 */
	public $RepresentativePerson;


} // end class NotificationType
