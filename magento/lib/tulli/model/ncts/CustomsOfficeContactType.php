<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName CustomsOfficeContactType
 * @var CustomsOfficeContactType
 * @xmlDefinition Customs office contact information.
 */
class CustomsOfficeContactType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CustomsOfficeCodeType $CustomsOfficeCode [optional] Customs contact office code.
	*/                                                                        
	public function __construct($CustomsOfficeCode = null)
	{
		$this->CustomsOfficeCode = $CustomsOfficeCode;
	}
	
	/**
	 * @Definition Customs contact office code.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName CustomsOfficeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CustomsOfficeCodeType
	 */
	public $CustomsOfficeCode;


} // end class CustomsOfficeContactType
