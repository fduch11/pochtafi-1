<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName EndorsementType
 * @var EndorsementType
 * @xmlDefinition Endorsement of the reportable event that an authority has confirmed.
 */
class EndorsementType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\udt\DateType $EndorsementDate [optional] Date of endorsement.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\DescriptionType $AuthorityDescription [optional] Confirming authority.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\DescriptionType $AuthorityLocationDescription [optional] Location of confirming authority.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $AuthorityCountryCode [optional] Country of confirming authority.
	*/                                                                        
	public function __construct($EndorsementDate = null, $AuthorityDescription = null, $AuthorityLocationDescription = null, $AuthorityCountryCode = null)
	{
		$this->EndorsementDate = $EndorsementDate;
		$this->AuthorityDescription = $AuthorityDescription;
		$this->AuthorityLocationDescription = $AuthorityLocationDescription;
		$this->AuthorityCountryCode = $AuthorityCountryCode;
	}
	
	/**
	 * @Definition Date of endorsement.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName EndorsementDate
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $EndorsementDate;
	/**
	 * @Definition Confirming authority.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName AuthorityDescription
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\DescriptionType
	 */
	public $AuthorityDescription;
	/**
	 * @Definition Location of confirming authority.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName AuthorityLocationDescription
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\DescriptionType
	 */
	public $AuthorityLocationDescription;
	/**
	 * @Definition Country of confirming authority.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName AuthorityCountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $AuthorityCountryCode;


} // end class EndorsementType
