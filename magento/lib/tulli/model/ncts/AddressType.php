<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName AddressType
 * @var AddressType
 * @xmlDefinition Party address.
 */
class AddressType
	{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\NameType $Line [optional] The postal delivery point such as street and number or post office box.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\PostcodeIDType $PostcodeID [optional] Code specifying a postal zone or address.
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\NameType $CityName [optional] City name.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType $CountryCode [optional] Country code.
	*/                                                                        
	public function __construct($Line = null, $PostcodeID = null, $CityName = null, $CountryCode = null)
	{
		$this->Line = $Line;
		$this->PostcodeID = $PostcodeID;
		$this->CityName = $CityName;
		$this->CountryCode = $CountryCode;
	}
	
	/**
	 * @Definition The postal delivery point such as street and number or post office box.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName Line
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $Line;
	/**
	 * @Definition Code specifying a postal zone or address.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName PostcodeID
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\PostcodeIDType
	 */
	public $PostcodeID;
	/**
	 * @Definition City name.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName CityName
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $CityName;
	/**
	 * @Definition Country code.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName CountryCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
	 */
	public $CountryCode;


} // end class AddressType
