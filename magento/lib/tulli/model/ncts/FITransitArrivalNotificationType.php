<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName FITransitArrivalNotification
 * @var FITransitArrivalNotificationType
 * @xmlDefinition FITARR - Notification of arrival message (IE07).
 */
class FITransitArrivalNotificationType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType $MessageInterchange [optional] Message interchange information.
		@param fi\tulli\schema\external\ncts\dme\v1\NotificationType $Notification [optional] Arrival notification.
	*/                                                                        
	public function __construct($MessageInterchange = null, $Notification = null)
	{
		$this->MessageInterchange = $MessageInterchange;
		$this->Notification = $Notification;
	}
	
	/**
	 * @Definition Message interchange information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageInterchange
	 * @var fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType
	 */
	public $MessageInterchange;
	/**
	 * @Definition Arrival notification.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Notification
	 * @var fi\tulli\schema\external\ncts\dme\v1\NotificationType
	 */
	public $Notification;


} // end class FITransitArrivalNotificationType
