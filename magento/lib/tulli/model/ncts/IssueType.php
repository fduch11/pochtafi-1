<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName IssueType
 * @var IssueType
 * @xmlDefinition Issuing details.
 */
class IssueType
	{

	/**
		@param datetime $IssueDate Date of declaration.
		@param string $LocationName Place of declaration.
	*/                                                                        
	public function __construct($IssueDate, $LocationName)
	{
		$this->IssueDate = $IssueDate;
		$this->LocationName = $LocationName;
	}
	
	/**
	 * @Definition Date of declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName IssueDate
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $IssueDate;
	/**
	 * @Definition Place of declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName LocationName
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
	 */
	public $LocationName;

} // end class IssueType
