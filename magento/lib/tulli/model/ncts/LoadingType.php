<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName LoadingType
 * @var LoadingType
 * @xmlDefinition Loading information.
 */
class LoadingType
	{

	/**
		@param datetime $LoadingDateTime Date and time, when loading takes place and goods can be inspected.
		@param string $LocationName Loading place.
	*/                                                                        
	public function __construct($LoadingDateTime, $LocationName)
	{
		$this->LoadingDateTime = $LoadingDateTime;
        $this->LocationName = $LocationName;
	}
	
	/**
	 * @Definition Date and time, when loading takes place and goods can be inspected.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName LoadingDateTime
	 * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateType
	 */
	public $LoadingDateTime;

    /**
     * @Definition Loading place.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName LocationName
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\NameType
     */
    public $LocationName;

} // end class LoadingType
