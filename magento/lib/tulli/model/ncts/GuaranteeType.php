<?php

/* TODO: manual generated file */

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName GuaranteeType
 * @var GuaranteeType
 * @xmlDefinition Guarantee details.
 */
class GuaranteeType
	{

	/**
		@param string $TransitGuaranteeTypeCode Transit guarantee type code, FI Customs code list 0187.
		@param string $GuaranteeReferenceID Guarantee reference number.
        @param string $OtherGuaranteeReferenceID Other guarantee reference number, e.g. TIR Carnet Number.
        @param string $AccessKeyCodeID Guarantee access key code.
        @param string $ValidityLimitationIndicator Indicating, wether not valid for EC.
        @param string ValidityLimitationCountryCode Codes of countries where guarantee is not valid.
	*/                                                                        
	public function __construct($TransitGuaranteeTypeCode, $GuaranteeReferenceID, $OtherGuaranteeReferenceID, $AccessKeyCodeID, $ValidityLimitationIndicator, $ValidityLimitationCountryCode)
	{
		$this->TransitGuaranteeTypeCode = $TransitGuaranteeTypeCode;
		$this->GuaranteeReferenceID = $GuaranteeReferenceID;
        $this->OtherGuaranteeReferenceID = $OtherGuaranteeReferenceID;
        $this->AccessKeyCodeID = $AccessKeyCodeID;
        $this->ValidityLimitationIndicator = $ValidityLimitationIndicator;
        $this->ValidityLimitationCountryCode = $ValidityLimitationCountryCode;
	}
	
	/**
	 * @Definition Transit guarantee type code, FI Customs code list 0187.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName TransitGuaranteeTypeCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\TransitGuaranteeTypeCodeType
	 */
	public $TransitGuaranteeTypeCode;

	/**
	 * @Definition Guarantee reference number.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMaxOccurs 1
	 * @xmlName GuaranteeReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\GuaranteeReferenceIDType
	 */
	public $GuaranteeReferenceID;

    /**
     * @Definition Other guarantee reference number, e.g. TIR Carnet Number.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName OtherGuaranteeReferenceID
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\OtherGuaranteeReferenceIDType
     */
    public $OtherGuaranteeReferenceID;

    /**
     * @Definition Guarantee access key code.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName AccessKeyCodeID
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\AccessKeyCodeIDType
     */
    public $AccessKeyCodeID;

    /**
     * @Definition Indicating, wether not valid for EC.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName ValidityLimitationIndicator
     * @var fi\tulli\schema\external\common\dme\v1_0\udt\IndicatorType
     */
    public $ValidityLimitationIndicator;

    /**
     * @Definition Codes of countries where guarantee is not valid.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlMaxOccurs 99
     * @xmlName ValidityLimitationCountryCode
     * @var fi\tulli\schema\external\common\dme\v1_0\cdt\CountryCodeType
     */
    public $ValidityLimitationCountryCode;

} // end class GuaranteeType
