<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName CommodityType
 * @var CommodityType
 * @xmlDefinition Goods commodity and classification.
 */
class CommodityType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\TariffClassificationType $TariffClassification [optional] Commodity classification information.
		@param fi\tulli\schema\external\common\dme\v1_0\cdt\DangerousGoodsCodeType $DangerousGoodsCode [optional] United Nations Dangerous Goods" (UNDG) identifier is the unique serial number assigned within the United Nations to substances and articles contained in a list of the dangerous goods most commonly carried.
		@param fi\tulli\schema\external\ncts\dme\v1\GoodsDescriptionType $GoodsDescription [optional] Goods description text.
	*/                                                                        
	public function __construct($TariffClassification = null, $DangerousGoodsCode = null, $GoodsDescription = null)
	{
		$this->TariffClassification = $TariffClassification;
		$this->DangerousGoodsCode = $DangerousGoodsCode;
		$this->GoodsDescription = $GoodsDescription;
	}
	
	/**
	 * @Definition Commodity classification information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName TariffClassification
	 * @var fi\tulli\schema\external\ncts\dme\v1\TariffClassificationType
	 */
	public $TariffClassification;
	/**
	 * @Definition United Nations Dangerous Goods" (UNDG) identifier is the unique serial number assigned within the United Nations to substances and articles contained in a list of the dangerous goods most commonly carried.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName DangerousGoodsCode
	 * @var fi\tulli\schema\external\common\dme\v1_0\cdt\DangerousGoodsCodeType
	 */
	public $DangerousGoodsCode;
	/**
	 * @Definition Goods description text.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName GoodsDescription
	 * @var fi\tulli\schema\external\ncts\dme\v1\GoodsDescriptionType
	 */
	public $GoodsDescription;


} // end class CommodityType
