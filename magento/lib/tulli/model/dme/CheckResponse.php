<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName CheckResponse
 * @var CheckResponse
 * @xmlDefinition Returns Base64-coded EchoContent.
 */
class CheckResponse
	{



	/**                                                                       
		@param string $EchoResponse [optional] Not present when an error has occurred.
	*/                                                                        
	public function __construct($ResponseHeader = null, $EchoResponse = null)
	{
		$this->ResponseHeader = $ResponseHeader;
		$this->EchoResponse = $EchoResponse;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\ResponseHeader
	 */
	public $ResponseHeader;
	/**
	 * @Definition Not present when an error has occurred.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName EchoResponse
	 * @var string
	 */
	public $EchoResponse;


} // end class CheckResponse
