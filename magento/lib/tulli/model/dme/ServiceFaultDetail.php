<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName ServiceFaultDetail
 * @var ServiceFaultDetail
 */
class ServiceFaultDetail
	{



	/**                                                                       
	*/                                                                        
	public function __construct($code = null, $text = null)
	{
		$this->code = $code;
		$this->text = $text;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName code
	 * @var string
	 */
	public $code;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName text
	 * @var string
	 */
	public $text;


} // end class ServiceFaultDetail
