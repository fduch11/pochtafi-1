<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName ApplicationContent
 * @var ApplicationContent
 * @xmlDefinition Customs application content
 */
class ApplicationContent
	{



	/**                                                                       
		@param  $Content [optional] The actual content, e.g. customs declaration.
	*/                                                                        
	public function __construct($Content = null, $ContentFormat = null)
	{
		$this->Content = $Content;
		$this->ContentFormat = $ContentFormat;
	}
	
	/**
	 * @Definition The actual content, e.g. customs declaration.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/v1
	 * @xmlName Content
	 */
	public $Content;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/v1
	 * @xmlName ContentFormat
	 * @var fi\tulli\schema\corporateservice\v1\ContentFormat
	 */
	public $ContentFormat;


} // end class ApplicationContent
