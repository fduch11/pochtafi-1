<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName ApplicationResponseType
 * @var ApplicationResponseType
 * @xmlDefinition Use for transmitting (response) data. Can be XML Digital Signed
 */
class ApplicationResponseType
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $DeclarantBusinessId [optional] The business identity code of the party who is declarant.
		@param fi\tulli\schema\corporateservice\v1\Reference $ControlReference [optional] An identifier originally given by the customer (Message Builder) to identify this particular interchange.
		@param fi\tulli\schema\corporateservice\v1\ApplicationContent $ApplicationResponseContent [optional] The actual response message from Customs application and information for it.
		@param fi\tulli\schema\corporateservice\v1\ApplicationContent $AttachmentOfApplicationResponseContent [optional] A file attachment that is related to the response content.
	*/                                                                        
	public function __construct($DeclarantBusinessId = null, $Timestamp = null, $Application = null, $ControlReference = null, $MessageStorageId = null, $ApplicationResponseContent = null, $AttachmentOfApplicationResponseContent = null, $Signature = null)
	{
		$this->DeclarantBusinessId = $DeclarantBusinessId;
		$this->Timestamp = $Timestamp;
		$this->Application = $Application;
		$this->ControlReference = $ControlReference;
		$this->MessageStorageId = $MessageStorageId;
		$this->ApplicationResponseContent = $ApplicationResponseContent;
		$this->AttachmentOfApplicationResponseContent = $AttachmentOfApplicationResponseContent;
		$this->Signature = $Signature;
	}
	
	/**
	 * @Definition The business identity code of the party who is declarant.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName DeclarantBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $DeclarantBusinessId;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Timestamp
	 * @var string
	 */
	public $Timestamp;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName Application
	 */
	public $Application;
	/**
	 * @Definition An identifier originally given by the customer (Message Builder) to identify this particular interchange.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlMinOccurs 0
	 * @xmlName ControlReference
	 * @var fi\tulli\schema\corporateservice\v1\Reference
	 */
	public $ControlReference;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlName MessageStorageId
	 * @var fi\tulli\schema\corporateservice\v1\MessageStorageId
	 */
	public $MessageStorageId;
	/**
	 * @Definition The actual response message from Customs application and information for it.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlMinOccurs 0
	 * @xmlName ApplicationResponseContent
	 * @var fi\tulli\schema\corporateservice\v1\ApplicationContent
	 */
	public $ApplicationResponseContent;
	/**
	 * @Definition A file attachment that is related to the response content.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/corporateservice/appl/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs unbounded
	 * @xmlName AttachmentOfApplicationResponseContent
	 * @var fi\tulli\schema\corporateservice\v1\ApplicationContent
	 */
	public $AttachmentOfApplicationResponseContent;
	/**
	 * @xmlType element
	 * @xmlNamespace http://www.w3.org/2000/09/xmldsig#
	 * @xmlMinOccurs 0
	 * @xmlName Signature
	 * @var org\w3\www\_2000\_09\xmldsig#\Signature[]
	 */
	public $Signature;


} // end class ApplicationResponseType
