<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DownloadListResponse
 * @var DownloadListResponse
 */
class DownloadListResponse
	{



	/**                                                                       
	*/                                                                        
	public function __construct($ResponseHeader = null, $DownloadMessageListFilteringCriteria = null, $MessageInformation = null)
	{
		$this->ResponseHeader = $ResponseHeader;
		$this->DownloadMessageListFilteringCriteria = $DownloadMessageListFilteringCriteria;
		$this->MessageInformation = $MessageInformation;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\ResponseHeader
	 */
	public $ResponseHeader;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName DownloadMessageListFilteringCriteria
	 * @var fi\tulli\ws\corporateservicetypes\v1\DownloadMessageListFilteringCriteria
	 */
	public $DownloadMessageListFilteringCriteria;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlMaxOccurs unbounded
	 * @xmlName MessageInformation
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageInformation
	 */
	public $MessageInformation;


} // end class DownloadListResponse
