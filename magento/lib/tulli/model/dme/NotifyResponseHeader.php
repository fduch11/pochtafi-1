<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName NotifyResponseHeader
 * @var NotifyResponseHeader
 * @xmlDefinition Header containing basic metadata for a notification response.
 */
class NotifyResponseHeader
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $IntermediaryBusinessId [optional] The business identity code of the message intermediary. It should be identical to the IntermediaryBusinessId in the notification request.
		@param string $Timestamp [optional] Date and time when the notification response was returned from the intermediary's notification service. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
	*/                                                                        
	public function __construct($IntermediaryBusinessId = null, $Timestamp = null)
	{
		$this->IntermediaryBusinessId = $IntermediaryBusinessId;
		$this->Timestamp = $Timestamp;
	}
	
	/**
	 * @Definition The business identity code of the message intermediary. It should be identical to the IntermediaryBusinessId in the notification request.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName IntermediaryBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $IntermediaryBusinessId;
	/**
	 * @Definition Date and time when the notification response was returned from the intermediary's notification service. Data type is ISODateTime. If no timezone is specified, the Finnish timezone is assumed.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/notificationservicetypes/v1
	 * @xmlName Timestamp
	 * @var string
	 */
	public $Timestamp;


} // end class NotifyResponseHeader
