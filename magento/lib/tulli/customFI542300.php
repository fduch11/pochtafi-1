<?php

require "fixedParams.php";

$fixedParams['Principal'] = array(
    'ID' => $PrincipalID,
    'IDExtension' => 'T0001',
    'Name' => 'Pochta.fi Oy',
    'Address' => array(
        'Line' => 'Pelkolankatu 5',
        'PostcodeID' => '53420',
        'CityName' => 'Lappeenranta',
        'CountryCode' => 'FI'
    ),
    'NatureQualifierCode' => 'R',
    'LanguageCode' => 'FI',
    'TIRHolderID' => null
);

$fixedParams['AuthorisedConsignee'] = array(
    'ID' => $BusinessId,
    'IDExtension' => 'T0001'
);

$fixedParams['TransitDepartureOffice'] = 'FI542300';

$fixedParams['GoodsLocation'] = array(
    'LocationQualifierCode' => 'L',
    'LocationID' => $BusinessId . 'R0001'
);

$fixedParams['LocationName'] = 'Lappeenranta';

?>