<?php
/**
 * Created by IntelliJ IDEA.
 * User: VMikhaylov
 * Date: 22.01.2015
 * Time: 16:42
 */

require_once Mage::getModuleDir('controllers', 'Iwings_Pochta').DS.'BaseController.php';

require_once('lib/tulli/Tulli.php');

class Custom
{

    public static function buildFITDEC($movement, $productsLinkedAsCrossSell = null) {

        $fixedParams = array();
        $place_imatra = Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::registry('box_store_imatra'));
        $place_lappa = Mage::getResourceSingleton('catalog/product')->getAttribute('box_store')->getSource()->getOptionText(Mage::registry('box_store_lappeenranta'));
        if ($movement->getMvtSrcWhs() == $place_imatra) {
            require("lib/tulli/customFI556100.php");
        } else if ($movement->getMvtSrcWhs() == $place_lappa) {
            require("lib/tulli/customFI542300.php");
        }


        $params = self::fillFITDECParams($fixedParams, $movement, $productsLinkedAsCrossSell);

        $tulli = new Tulli();
        $result = $tulli->buildT1($fixedParams, $params);

        return $result;
    }

    private static function fillFITDECParams($fixedParams, $movement, $productsLinkedAsCrossSell) {

        $params = array(

            'TraderReferenceID' => $movement->getSku(),

            'TransitControlResultCode' => 'A3',

            'DepartureTransportMeans' => array(
                'TransportModeCode' => $movement->getTransportModeCode(),
                'TransportMeansNationalityCode' => $movement->getTransportNationalityCode(),
                'TransportMeansID' => $movement->getTransportMeansId() != '' ? $movement->getTransportMeansId() : $movement->getCarrierNameFirst(),
                'ConveyanceReferenceID' => $movement->getConveyanceReferenceId(),
            ),

            'DispatchCountryCode' => null,
            'DestinationCountryCode' => null,

            'TransitDestinationOffice' => $movement->getMvtDstWhs(),

//            'TotalGrossMassMeasure' => array(
//                'UnitCode' => 'KGM',
//                'Value' => $product->getWeight()
//            ),

//            'GoodsLocation' => array(
//                'LocationQualifierCode' => $fitdec->getCsLocationQualifierCode(),
//                'LocationID' => $fitdec->getCsLocationId(),
//            ),
            'Loading' => array(
                'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("+30 minutes")),
                'LocationName' => $fixedParams['LocationName']
            ),
            'Unloading' => array(
                'LocationName' => $movement->getConsigneeAddressCity()
            ),
            'Itinerary' => array(
                1 => array('RoutingCountryCode' => 'FI'),
//                2 => array('RoutingCountryCode' => 'NO'),
            ),
            'Issue' => array(
                'IssueDate' => date('Y-m-d'),
                'LocationName' => $fixedParams['LocationName']
            ),
            'TransitLimitDate' => date('Y-m-d', strtotime($movement->getMvtDstPlannedDate())),
            'ContainerTransportIndicator' => 'false',
            'Sealing' => null,
//            'Sealing' => array(
//                'SealQuantity' => 1,
//                'SealID' => 'HE046-E'
//            ),


            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => $movement->getConsigneeAddressCountry(),

        );

        $goodItemsIndex = 1;
        $goodItems = array();

        $TotalPackageQuantity = 0;
        $TotalWeight = 0.0;

        /** @var $productsLinkedAsCrossSell Mage_Catalog_Model_Resource_Product_Link_Collection */
        if ($productsLinkedAsCrossSell == null) {
            $productsLinkedAsCrossSell = Mage::helper('pochta')->getProductsByMovement($movement->getId());
        }
        foreach ($productsLinkedAsCrossSell as $link) {
            /** @var $parcel Mage_Catalog_Model_Product */
            $parcel = Mage::getModel('catalog/product')->load($link->getProductId());
            if ($parcel->getBoxOrigin() == Mage::registry('box_origin_european')) {
                continue;
            }

            {
                // find previous documents
                $prevDocs = array();

                $relatedDocumentCollection = $parcel->getRelatedProductCollection();
                $relatedDocumentCollection
                    ->setOrder('date', 'asc');
                $lastDoc = $relatedDocumentCollection->getLastItem();

                if ($lastDoc && $lastDoc->getId()) {
                    $lastDoc = Mage::getModel('catalog/product')->load($lastDoc->getId());
                    $prevDocs[1] = array(
                        'DocumentTypeCode' => $lastDoc->getPrevDocumentTypecode(),
                        'DocumentID' => $lastDoc->getPrevDocumentId()
                    );
                }
            }

            $firstItem = true;
            $bundleItems = Iwings_Pochta_BaseController::getBundledData($parcel);
            foreach ($bundleItems as $bundleItem) {

                /** @var $simpleProduct Mage_Catalog_Model_Product */
                $simpleProduct = Mage::getModel('catalog/product')->load($bundleItem->getId());

                $goodItem = array(
                    'UniqueConsignmentReferenceID' => null,
                    'TransitTypeCode' => 'T1',
                    //'DispatchCountryCode' => 'FI',
                    //'DestinationCountryCode' => $parcel->getConsigneeAddressCountry(),
                    'Consignor' => array(
                        'ID' => null,
                        'IDExtension' => null,
                        'Name' => $parcel->getConsignorName(),
                        'Address' => array(
                            'Line' => $parcel->getConsignorAddressLine(),
                            'PostcodeID' => $parcel->getConsignorAddressPostcode(),
                            'CityName' => $parcel->getConsignorAddressCity(),
                            'CountryCode' => $parcel->getConsignorAddressCountry()
                        ),
                    ),
                    'ConsignorSecurity' => array(
                        'ID' => null,
                        'IDExtension' => null,
                        'Name' => $parcel->getConsignorName(),
                        'Address' => array(
                            'Line' => $parcel->getConsignorAddressLine(),
                            'PostcodeID' => $parcel->getConsignorAddressPostcode(),
                            'CityName' => $parcel->getConsignorAddressCity(),
                            'CountryCode' => $parcel->getConsignorAddressCountry()
                        ),
                    ),

                    'Consignee' => array(
                        'ID' => null,
                        'IDExtension' => null,
                        'Name' => Mage::helper('pochta')->getCarrierName($movement),
                        'Address' => array(
                            'Line' => $movement->getConsigneeAddressLine(),
                            'PostcodeID' => $movement->getConsigneeAddressPostcode(),
                            'CityName' => $movement->getConsigneeAddressCity(),
                            'CountryCode' => $movement->getConsigneeAddressCountry()
                        ),
                    ),
                    'ConsigneeSecurity' => array(
                        'ID' => null,
                        'IDExtension' => null,
                        'Name' => Mage::helper('pochta')->getCarrierName($movement),
                        'Address' => array(
                            'Line' => $movement->getConsigneeAddressLine(),
                            'PostcodeID' => $movement->getConsigneeAddressPostcode(),
                            'CityName' => $movement->getConsigneeAddressCity(),
                            'CountryCode' => $movement->getConsigneeAddressCountry()
                        ),
                    ),

                    'Commodity' => array(
                        'TariffClassification' => array($simpleProduct->getTariffClassification()), // TODO:
                        'DangerousGoodsCode' => null,
                        'GoodsDescription' => $simpleProduct->getName(),
                    ),
                    'SensitiveGoods' => null,
                    'GrossMassMeasure' => array(
                        'UnitCode' => 'KGM',
                        'Value' => $simpleProduct->getWeight() //* $bundleItem->getSelectionQty()
                    ),
//                'NetWeightMeasure' => array(
//                    'UnitCode' => 'KGM',
//                    'Value' => $simpleProduct->getWeight()
//                ),
                    'Packaging' => array(
                        1 => array(
                            'PackagingTypeCode' => 'CS', //$simpleProduct->getPackagingTypeCode(),
                            'PackagingMarksID' => $parcel->getSku(), // $simpleProduct->getPackagingMarksId(),
                            'PackageQuantity' => $firstItem ? '1' : '0',//round($bundleItem->getSelectionQty()),
                            //'PieceCountQuantity' => round($bundleItem->getSelectionQty())
                        )
                    ),

                    'AdditionalDocument' => $goodItemsIndex == 1 ? array(
                            1 => array(
                                'DocumentTypeCode' => '740',
                                'DocumentID' => $parcel->getBoxTrackingNum()
                            ),
                        )  : null,

                );

                $TotalWeight += $simpleProduct->getWeight();// * $bundleItem->getSelectionQty()

                $goodItem['PreviousDocument'] = count($prevDocs) > 0 ? $prevDocs : null;

                $goodItems[$goodItemsIndex++] = $goodItem;

                $firstItem = false;
            }

            $TotalPackageQuantity++;

//            break;
        }

        $params['TotalGrossMassMeasure'] = array(
            'UnitCode' => 'KGM',
            'Value' => $TotalWeight
        );

        $params['GoodsItemQuantity'] = count($goodItems);
        $params['TotalPackageQuantity'] = $TotalPackageQuantity;

        $params['GoodsItem'] = $goodItems;
        $params['XMessageType'] = 'FITDEC';

        return $params;
    }

    public static function sendDeclaration($fitdec_body) {

        $currentTime = strtotime('now');

        $fixedParams = array();
        require("lib/tulli/fixedParams.php");

        $tulli = new Tulli();
        $result = $tulli->sendT1($fixedParams, null, $fitdec_body, $dummy);

        {
            // extract MessageReferenceID and ControlReferenceID
            $contentObj = new SimpleXMLElement($fitdec_body);
            $MessageInterchange = $contentObj->children("ncts", true)->MessageInterchange;
            $MessageReferenceID = (string)$MessageInterchange->MessageReferenceID;
            $ControlReferenceID = (string)$MessageInterchange->ControlReferenceID;

            $declType = self::getDeclarationType($contentObj->getName());

            //$MessageInterchange = $contentObj->children("ncts", true)->MessageInterchange;
        }


        $declaration = Mage::getModel('catalog/product');

        $declaration
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID) //you can set data in store scope
            ->setTypeId('virtual')                              //product type
            ->setSku($ControlReferenceID)
            ->setName($declType . ' ' . date('Y-m-d\TH:i:s', $currentTime))
            ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//            ->setVisibility(?)
            ->setCreatedAt($currentTime)                    //product creation time

            ->setWebsiteIds(array(Mage::registry('website_backoffice')))        //website ID the product is assigned to, as an array
            ->setAttributeSetId(Mage::registry('attr_set_id_declaration'))            //ID of a attribute set named 'Declaration'
        ;

        $declaration
            ->setDate($currentTime)
            ->setCsResponseCode($result->ResponseHeader->ResponseCode)
            ->setCsResponseText($result->ResponseHeader->ResponseText)
            ->setCsBody($fitdec_body);

        if ($result->ResponseHeader->ResponseCode === '000') {
            $declaration->setCsControlReferenceId($result->ApplicationRequestMessageInformation->ControlReference);
            $declaration->setCsMessageReferenceId($MessageReferenceID);
        }

        $declaration->save();

        return $declaration;
    }


    public static function globalRenew($renewType, $startDate, $endDate)
    {
        $fixedParams = array();
        require("lib/tulli/fixedParams.php");

        $tulli = new Tulli();
        $result = $tulli->refresh($fixedParams, $renewType, $startDate, $endDate);

        /** @var $result DownloadListResponse */
        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode === '000' && $responseHeader->ResponseText == 'OK') {
            if (is_array($result->MessageInformation)) {
                foreach ($result->MessageInformation as $responseMessage) {
                    self::downloadDeclaration($responseMessage->MessageStorageId, $responseMessage->ControlReference);
//                    $this->updateParcels($responseMessage->ControlReference, $responseMessage->MessageStorageId);
                }
            } else if (is_object($result->MessageInformation)) {
                self::downloadDeclaration($result->MessageInformation->MessageStorageId, $result->MessageInformation->ControlReference);
//                $this->updateParcels($result->MessageInformation->ControlReference, $result->MessageInformation->MessageStorageId);
            }
        }

        return $responseHeader->ResponseText;
    }

    private static function downloadDeclaration($messageStorageId, $controlReference) {
        $fixedParams = array();
        require("lib/tulli/fixedParams.php");

        $tulli = new Tulli();
        $result = $tulli->download($fixedParams, $messageStorageId);

        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode === '000' && $responseHeader->ResponseText == 'OK') {

            $responseMessage = new SimpleXMLElement($result->ApplicationResponseMessage);

            $responseContent = $responseMessage->children("v1", true)->ApplicationResponseContent;
            $responseContent = base64_decode($responseContent->children("v11", true)->Content);

            $attachmentContent = $responseMessage->children("v1", true)->AttachmentOfApplicationResponseContent;
            if (!empty($attachmentContent)) {
                $attachmentContent = base64_decode($attachmentContent->children("v11", true)->Content);
            } else {
                $attachmentContent = null;
            }

            $messageReferenceId = null; // out param
            $custom_id = self::checkAndCreateDeclaration($responseContent, $attachmentContent, $messageStorageId, $controlReference, $messageReferenceId);
            self::bindDeclaration($controlReference, $messageReferenceId, $custom_id);

        } else {
            Mage::register('response', $responseHeader->ResponseText);
        }

    }

    private static function checkAndCreateDeclaration($responseContent, $attachmentContent, $messageStorageId, $controlReference, &$messageReference) {

        $responseObj = new SimpleXMLElement($responseContent);
        $declType = self::getDeclarationType($responseObj->getName());

        $currentTime = strtotime('now');
        $messageDateTime = (string)$responseObj->MessageInterchange->MessageDateTime;

        $declaration = Mage::getModel('catalog/product')->loadByAttribute('sku', $messageStorageId);
        if (!$declaration) {
            $declaration = Mage::getModel('catalog/product');
        }
        $declaration
            ->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)
            ->setTypeId('virtual')                              //product type
            ->setSku($messageStorageId)
            ->setName($declType . ' ' . $controlReference)
            ->setStatus(1)                                      //product status (1 - enabled, 2 - disabled)
//            ->setVisibility(?)
            ->setCreatedAt($currentTime)                    //product creation time

            ->setWebsiteIds(array(Mage::registry('website_backoffice')))        //website ID the product is assigned to, as an array
            ->setAttributeSetId(Mage::registry('attr_set_id_declaration'))            //ID of a attribute set named 'Declaration'
        ;

//        $mrn = (string)$responseObj->Release->MovementReferenceID;
        // MovementReferenceID can have various parent tags
        foreach($responseObj->getDocNamespaces() as $strPrefix => $strNamespace) {
            if(strlen($strPrefix)==0) {
                $strPrefix="pochta"; //Assign an arbitrary namespace prefix.
            }
            $responseObj->registerXPathNamespace($strPrefix,$strNamespace);
        }
        $mrn = (string)($responseObj->xpath('//pochta:MovementReferenceID')[0]);
        $messageReference = (string)($responseObj->xpath('//pochta:MessageReferenceID')[0]);

        $doc = new DOMDocument();
        $doc->loadXML($responseContent);
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $text = $doc->saveXML();

        $declaration
            ->setDate(str_replace('T', ' ', $messageDateTime))
            ->setCsMrn($mrn)
            ->setCsMessageStorageId($messageStorageId)
            ->setCsControlReferenceId($controlReference)
            ->setCsMessageReferenceId($messageReference)
            ->setCsBody($text)
        ;

        $declaration->save();

        if (isset($attachmentContent)) {

            $folderName = Mage::getBaseDir('media').DS.'product_custom_files';
            $fullPath = $folderName .DS. $messageStorageId . '.zip';
            file_put_contents($fullPath, $attachmentContent);
            //Mage::helper('core/file_storage_database')->saveFile($fullPath);

            $filesModel = Mage::getModel('files/files');
            $filesModel->setProdId($declaration->getId())
                ->setFileTitle($messageStorageId . '.zip')
                ->setProdFile(DS . $messageStorageId . '.zip')
                ->setFileType('zip')
                ->setFileSize(filesize($fullPath))
                ->setFileSortorder(0)
                ->setFileCreation(date("Y-m-d H:i:s"))
                ->setFileLastmodification(date("Y-m-d H:i:s"))
                ->setFileTimestamp(time())
            ;
            $filesModel->save();
        }

        return $declaration->getId();
    }

    private static function bindDeclaration($controlReference, $messageReference, $custom_id) {

        /** @var $declarations Mage_Catalog_Model_Resource_Product_Link_Product_Collection */
        $declarations = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('attribute_set_id', Mage::registry('attr_set_id_declaration'))
            ->addAttributeToFilter(array(
                array('attribute' => 'cs_control_reference_id', 'eq' => $controlReference),
                array('attribute' => 'cs_message_reference_id', 'eq' => $messageReference)
            ));

        foreach ($declarations as $declaration) {

            // both parcels and movements
            $links = Mage::helper('pochta')->getProductsByCustom($declaration->getId());
            foreach ($links as $link) {

                $product = Mage::getModel('catalog/product')->load($link->getProductId());

                $relation_data =  Mage::helper('pochta')->getCustomData($product);
                if (!array_key_exists($custom_id, $relation_data)) {
                    $relation_data[$custom_id] = array('position' => 0);
                    $product->setCustomLinkData($relation_data);
                    $product->save();
                }
            }

        }

    }

    public static function getDeclarationType($declFullType) {
        switch ($declFullType) {
            case 'FITransitDeclaration':
                $declType = 'FITDEC';
                break;
            case 'FITransitAcceptance':
                $declType = 'FITACC';
                break;
            case 'FITransitError':
                $declType = 'FITERR';
                break;
            case 'FITransitProcedure':
                $declType = 'FITREL';
                break;
            case 'FITransitCancellationConfirmation':
                $declType = 'FITCAN';
                break;
            case 'FITransitArrivalConfirmation':
                $declType = 'FITCTR';
                break;
            case 'FITransitCompletion':
                $declType = 'FITWRO';
                break;
            case 'FITransitArrivalNotification':
                $declType = 'FITARR';
                break;
            case 'FITransitUnloadPermission':
                $declType = 'FITULD';
                break;
            case 'FITransitUnloadReport':
                $declType = 'FITULR';
                break;
            case 'FITransitRelease':
                $declType = 'FITGDR';
                break;
            case 'FITransitRefusal':
                $declType = 'FITREF';
                break;
            case 'FITransitInformationRequest':
                $declType = 'FITREQ';
                break;
            default:
                $declType = 'FITZZZ';
                return;
        }

        return $declType;
    }

}