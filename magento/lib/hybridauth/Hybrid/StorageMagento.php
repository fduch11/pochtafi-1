<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html 
*/

/**
 * HybridAuth storage manager
 */
class Hybrid_StorageMagento 
{
	function __construct()
	{ 
		if ( ! Mage::getSingleton("customer/session")->getEncryptedSessionId()){
			throw new Exception( "Hybridauth requires the use of 'session_start()' at the start of your script, which appears to be disabled.", 1 );
		}

		$this->config( "php_session_id", Mage::getSingleton("customer/session")->getEncryptedSessionId() );
		$this->config( "version", Hybrid_Auth::$version );
		$session = Mage::getSingleton("customer/session")->getHauth();
		if(!is_array($session)) Mage::getSingleton("customer/session")->setHauth(array());
		
	}
	function setSession($key, $value=null){
		$session = Mage::getSingleton("customer/session")->getHauth();
		$session[$key] = $value;
		Mage::getSingleton("customer/session")->setHauth($session);
	}
	function getSession($key){
		$session = Mage::getSingleton("customer/session")->getHauth();
		return $session[$key];
	}
	public function config($key, $value=null) 
	{
		$key = strtolower( $key );  

		if( $value ){
			$this->setSession($key, serialize( $value ));
		}
		elseif( $this->getSession($key) ){ 
			return unserialize( $this->getSession($key) );  
		}

		return NULL; 
	}

	public function get($key) 
	{
		$key = strtolower( $key );  

		if( $this->getSession($key) ){ 
			return unserialize( $this->getSession($key) );  
		}

		return NULL; 
	}

	public function set( $key, $value )
	{
		$key = strtolower( $key ); 

		$this->setSession($key, serialize( $value ));
	}

	function clear()
	{ 
		Mage::getSingleton("customer/session")->setHauth(array());
	} 

	function delete($key)
	{
		$key = strtolower( $key );  

		if( $this->getSession($key) ){
		    $session = Mage::getSingleton("customer/session")->getHauth();
			unset($session[$key]);
			Mage::getSingleton("customer/session")->setHauth($session);
		} 
	}

	function deleteMatch($key)
	{
		$key = strtolower( $key ); 
		$session = Mage::getSingleton("customer/session")->getHauth();
		if( count( $session ) ) {
		    $f = $session;
		    foreach( $f as $k => $v ){ 
				if( strstr( $k, $key ) ){
					unset( $f[ $k ] ); 
				}
			}
			$session = $f;
			
		}
		Mage::getSingleton("customer/session")->setHauth($session);
	}

	function getSessionData()
	{
		$session = Mage::getSingleton("customer/session")->getHauth();
		if($session){ 
			return serialize( $session ); 
		}

		return NULL; 
	}

	function restoreSessionData( $sessiondata = NULL )
	{ 
		Mage::getSingleton("customer/session")->setHauth( $sessiondata );
	} 
}
