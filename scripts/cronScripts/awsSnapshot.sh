#!/bin/bash
0 4 * * 1-6 /usr/local/bin/aws ec2 create-snapshot --volume-id vol-c8547ac3 --description "This is daily snapshot"
0 4 * * 7 /usr/local/bin/aws ec2 create-snapshot --volume-id vol-c8547ac3 --description "This is weekly snapshot"
0 4 1 * * /usr/local/bin/aws ec2 create-snapshot --volume-id vol-c8547ac3 --description "This is monthly snapshot"