<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '14FI000000000442T4',
	
	'FunctionCode' => '4',
	'TransitTypeCode' => 'T1',
	
	'UnloadingRemarks' => array(
		'RemarksDate' => '2014-11-14',
		'LocationName' => 'Helsinki'
	),		
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),

	'GoodsItemQuantity' => '2',
	'TotalPackageQuantity' => '25',
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 2500
    ),
	
    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
    ),
	
    'GoodsItem' => array(
        1 => array(
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'PUUTTUU',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 5000
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'Nike FB:1-1500',
                    'PackageQuantity' => 50,
                    'PieceCountQuantity' => null
                )
            )
        ),
        2 => array(
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'todettu pakkausten',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2500
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => null,
                    'PackageQuantity' => 25,
                    'PieceCountQuantity' => null
                )
            )
        )
			
    )
	
	
		
);

?>