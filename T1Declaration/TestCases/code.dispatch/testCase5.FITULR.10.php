<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '14FI000000000532T7',
	
	'FunctionCode' => '4',
	'TransitTypeCode' => 'T1',
	
	'UnloadingRemarks' => array(
		'RemarksDate' => date('Y-m-d'),
		'LocationName' => 'Helsinki'
	),		
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI534200'
	),

	'GoodsItemQuantity' => '4',
	'TotalPackageQuantity' => '262',
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 556
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
    ),
	
    'GoodsItem' => array(
        1 => array(
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Uudenkaupungin rahti Oy',
                'Address' => array(
                    'Line' => 'Suupakuja 5',
                    'PostcodeID' => '23500',
                    'CityName' => 'Uusikaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Canon Oy',
                'Address' => array(
                    'Line' => 'PL 100',
                    'PostcodeID' => '00700',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '900600',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Kameroita',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 200
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'Canon 1879',
                    'PackageQuantity' => 170,
                    'PieceCountQuantity' => null
                )
            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '730',
                    'DocumentID' => '1',
                    'SupplementaryInformation' => null
                ),
            ),
        ),
        2 => array(
            'DispatchCountryCode' => 'CH',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Uudenkaupungin rahti Oy',
                'Address' => array(
                    'Line' => 'Suupakuja 5',
                    'PostcodeID' => '23500',
                    'CityName' => 'Uusikaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Konepojat Oy',
                'Address' => array(
                    'Line' => 'Teollisuustie 3',
                    'PostcodeID' => '00700',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '846593',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Hiomakoneita',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 200
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'NE',
                    'PackagingMarksID' => '1-7',
                    'PackageQuantity' => 7,
                    'PieceCountQuantity' => null
                )
            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '380',
                    'DocumentID' => '17/03',
                    'SupplementaryInformation' => null
                ),
            ),
        ),
        3 => array(
            'DispatchCountryCode' => 'CH',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Uudenkaupungin rahti Oy',
                'Address' => array(
                    'Line' => 'Suupakuja 5',
                    'PostcodeID' => '23500',
                    'CityName' => 'Uusikaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Metsälän Tukku Oy',
                'Address' => array(
                    'Line' => 'Tukkutie 2',
                    'PostcodeID' => '006200',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '845210',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Hikinauhoja',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 150
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'PUUTTUU',
                    'PackageQuantity' => 25,
                    'PieceCountQuantity' => null
                ),
                2 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'HIKI 50',
                    'PackageQuantity' => 50,
                    'PieceCountQuantity' => null
                )
            )
        ),
        4 => array(
            'DispatchCountryCode' => 'CH',
            'DestinationCountryCode' => 'FI',

            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Uudenkaupungin rahti Oy',
                'Address' => array(
                    'Line' => 'Suupakuja 5',
                    'PostcodeID' => '23500',
                    'CityName' => 'Uusikaupunki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Metsälän Tukku Oy',
                'Address' => array(
                    'Line' => 'Tukkutie 2',
                    'PostcodeID' => '006200',
                    'CityName' => 'Helsinki',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => '262019',
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'valaisimia',
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 6
            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'BX',
                    'PackagingMarksID' => 'valo 13',
                    'PackageQuantity' => 10,
                    'PieceCountQuantity' => null
                ),

            )
        ),
    )
);

?>