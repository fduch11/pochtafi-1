﻿<?php

$params = array(

    'TraderReferenceID' => 'TEHTÄVÄ2-EFGH-987654',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'AKE-123 PYH-697',
        'ConveyanceReferenceID' => null
    ),
//            'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'Itinerary' => array(
        1 => array('RoutingCountryCode' => 'FI'),
        2 => array('RoutingCountryCode' => 'NO'),
    ),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'NO02061B',

    'TransitBorderOffice' => array('NO02061B'),

    'GoodsItemQuantity' => '3',
    'TotalPackageQuantity' => 1350,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 6600
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("today 09:15")), // текущая 09:15 SV
        'LocationName' => 'SV'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+2 days")), // +2 дней
    'ContainerTransportIndicator' => 'false',
    'Sealing' => array(
        'SealQuantity' => 1,
        'SealID' => 'HE046-E'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T2',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'NO',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Altia Corporation',
                'Address' => array(
                    'Line' => 'Valta-akseli',
                    'PostcodeID' => '5200',
                    'CityName' => 'Rajamaki',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Altia Corporation',
                'Address' => array(
                    'Line' => 'Valta-akseli',
                    'PostcodeID' => '5200',
                    'CityName' => 'Rajamaki',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS Vinmonopolet',
                'Address' => array(
                    'Line' => 'Dronning Mauts gate 1',
                    'PostcodeID' => '0125',
                    'CityName' => 'Oslo',
                    'CountryCode' => 'NO'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS Vinmonopolet',
                'Address' => array(
                    'Line' => 'Dronning Mauts gate 1',
                    'PostcodeID' => '0125',
                    'CityName' => 'Oslo',
                    'CountryCode' => 'NO'),
            ),
            'Commodity' => array(
                'TariffClassification' => array(220860),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Koskenkorva 40%',
            ),
            'SensitiveGoods' => array(
                'SensitiveGoodsCode' => null,
                'SensitiveGoodsMeasure' => 5.1
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 1000
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '1-1000',
                    'PackageQuantity' => 1000,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                2 => array(
                    'DocumentTypeCode' => '10',
                    'DocumentID' => '14FI000000000366E1',
                    'ItemSequenceNumber' => 2
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => array(
                'SpecialMentionCode' => 'DG2',
                'StatementDescription' => null,
                'EUExportIndicator' => null, // true,
                'ExportingCountryCode' => null
            ),
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        ),
        2 => array( // GoodsItem
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T2',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'NO',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'RÖNNVIKIN VIINITILA',
                'Address' => array(
                    'Line' => 'HÄMEENLINNANTIE 270',
                    'PostcodeID' => '36660',
                    'CityName' => 'LAITIKKALA',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'RÖNNVIKIN VIINITILA',
                'Address' => array(
                    'Line' => 'HÄMEENLINNANTIE 270',
                    'PostcodeID' => '36660',
                    'CityName' => 'LAITIKKALA',
                    'CountryCode' => 'FI'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'VINARIUS AS',
                'Address' => array(
                    'Line' => 'BILLINGSTADSLETTA 14',
                    'PostcodeID' => '1377',
                    'CityName' => 'BILLINGSTAD',
                    'CountryCode' => 'NO'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'VINARIUS AS',
                'Address' => array(
                    'Line' => 'BILLINGSTADSLETTA 14',
                    'PostcodeID' => '1377',
                    'CityName' => 'BILLINGSTAD',
                    'CountryCode' => 'NO'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(220860),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'KYLMÄNKUKKA KUOHUVIINI 10%',
            ),
            'SensitiveGoods' => array(
                'SensitiveGoodsCode' => null,
                'SensitiveGoodsMeasure' => 5.12
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2100
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '1-100',
                    'PackageQuantity' => 100,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                2 => array(
                    'DocumentTypeCode' => '10',
                    'DocumentID' => '14FI000000000366E1',
                    'ItemSequenceNumber' => 2
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => array(
                'SpecialMentionCode' => 'DG2',
                'StatementDescription' => null,
                'EUExportIndicator' => null, //true,
                'ExportingCountryCode' => null
            ),
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        ),

        3 => array( // GoodsItem
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T2',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'NO',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'ALTIA CORPORATION',
                'Address' => array(
                    'Line' => 'VALTA-AKSELI',
                    'PostcodeID' => '5200',
                    'CityName' => 'RAJAMÄKI',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'ALTIA CORPORATION',
                'Address' => array(
                    'Line' => 'VALTA-AKSELI',
                    'PostcodeID' => '5200',
                    'CityName' => 'RAJAMÄKI',
                    'CountryCode' => 'FI'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS VINMONOPOLET',
                'Address' => array(
                    'Line' => 'DRONNING MAUDS GATE 1',
                    'PostcodeID' => '0125',
                    'CityName' => 'OSLO',
                    'CountryCode' => 'NO'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'AS VINMONOPOLET',
                'Address' => array(
                    'Line' => 'DRONNING MAUDS GATE 1',
                    'PostcodeID' => '0125',
                    'CityName' => 'OSLO',
                    'CountryCode' => 'NO'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(220890),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'VADELMA VÄKEVÄ VIINI 15%',
            ),
            'SensitiveGoods' => array(
                'SensitiveGoodsCode' => 1,
                'SensitiveGoodsMeasure' => 10
            ),
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 3500
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => '1-250',
                    'PackageQuantity' => 250,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                2 => array(
                    'DocumentTypeCode' => '10',
                    'DocumentID' => '14FI000000000366E1',
                    'ItemSequenceNumber' => 2
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => array(
                'SpecialMentionCode' => 'DG2',
                'StatementDescription' => null,
                'EUExportIndicator' => null,
                'ExportingCountryCode' => null
            ),
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>