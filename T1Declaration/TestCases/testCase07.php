<?php

$params = array(

    'TraderReferenceID' => 'Tehtävä7-merkit2345-9',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'B78987722Y 148999KA',
        'ConveyanceReferenceID' => null
    ),
//          'BorderTransportMeans' => array('TransportModeCode' => 1, 'TransportMeansNationalityCode' => 'EE', 'TransportMeansID' => 'Eestiship', 'ConveyanceReferenceID' => null),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'GB000080',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 300,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 2500
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s', strtotime("today 11:15")), // текущая 11:15 FI
        'LocationName' => 'FI'
    ),
    'Issue' => array(
        'IssueDate' => date('Y-m-d'), //текущая
        'LocationName' => 'Imatra'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+3 days")), // +3 от текущей
    'ContainerTransportIndicator' => 'false',
    'Sealing' => array(
        'SealQuantity' => 2,
        'SealID' => 'HE1234-1235'
    ),
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'RU',
            'DestinationCountryCode' => 'GB',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Woodmakers Ltd.',
                'Address' => array(
                    'Line' => 'Ul. Novy Arbat 46/5',
                    'PostcodeID' => '127018',
                    'CityName' => 'Moscow',
                    'CountryCode' => 'RU'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Woodmakers Ltd.',
                'Address' => array(
                    'Line' => 'Ul. Novy Arbat 46/5',
                    'PostcodeID' => '127018',
                    'CityName' => 'Moscow',
                    'CountryCode' => 'RU'
                ),
            ),
            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Baltic Wood Trading',
                'Address' => array(
                    'Line' => '9 Elton Avenue',
                    'PostcodeID' => 'L23 8UN',
                    'CityName' => 'Liverpool',
                    'CountryCode' => 'GB'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Baltic Wood Trading',
                'Address' => array(
                    'Line' => '9 Elton Avenue',
                    'PostcodeID' => 'L23 8UN',
                    'CityName' => 'Liverpool',
                    'CountryCode' => 'GB'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => null,
                'DangerousGoodsCode' => null,
                'GoodsDescription' => '50 CR puisia keittokauhoja, 140 CR metallisia vispilöitä, 60 CR pullasuteja, 50 CT kruunukorkkeja',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 2500
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CR',
                    'PackagingMarksID' => 'Baltic soup',
                    'PackageQuantity' => 50,
                    'PieceCountQuantity' => null
                ),
                2 => array(
                    'PackagingTypeCode' => 'CR',
                    'PackagingMarksID' => 'Baltic herring ',
                    'PackageQuantity' => 140,
                    'PieceCountQuantity' => null
                ),
                3 => array(
                    'PackagingTypeCode' => 'CR',
                    'PackagingMarksID' => 'Baltijaama',
                    'PackageQuantity' => 60,
                    'PieceCountQuantity' => null
                ),
                4 => array(
                    'PackagingTypeCode' => 'CT',
                    'PackagingMarksID' => 'Baltika',
                    'PackageQuantity' => 50,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '71',
                    'DocumentID' => 'SST545'
                )
            ),

            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '271',
                    'DocumentID' => '177',
                    'SupplementaryInformation' => null
                ),
                2 => array(
                    'DocumentTypeCode' => '325',
                    'DocumentID' => '177/07',
                    'SupplementaryInformation' => null
                )
            ),
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null

        )
    )
);

?>