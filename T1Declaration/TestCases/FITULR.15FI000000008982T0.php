<?php

$params = array(

    'XMessageType' => 'FITULR',

    'MovementReferenceID' => '15FI000000008982T0',

    'FunctionCode' => '6',
    'TransitTypeCode' => 'T1',

    'TransitPresentationOffice' => array(
        'CustomsOfficeCode' => 'FI542300'
    ),

    'TransitDepartureOffice' => 'FI015300',

    'DepartureTransportMeans' => array(
//        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'RU',
        'TransportMeansID' => 'Itella',
//        'ConveyanceReferenceID' => null
    ),

    'UnloadingRemarks' => array(
        'RemarksDate' => '2015-01-26',
        'LocationName' => 'FI534200'
    ),

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => 1
    ),

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
//        'Name' => 'Pochta.fi Oy',             // not allowed for ActualAuthorisedConsignee
//        'Address' => array(
//            'Line' => 'Kultakuusenkuja 4',
//            'PostcodeID' => '55610',
//            'CityName' => 'Imatra',
//            'CountryCode' => 'FI'
//        ),
    ),

    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'FI',
            'DestinationCountryCode' => 'RU',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'VIVICATER',
                'Address' => array(
                    'Line' => 'BAOLONG BUILDING. MINZHI INDUSTRIAL',
                    'PostcodeID' => '518131',
                    'CityName' => 'SHENZHEN',
                    'CountryCode' => 'CH'
                ),
            ),

            'Consignee' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),

            'Commodity' => array(
                'TariffClassification' => array(830170),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'KEY BLANKS',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => 1
            ),
//            'NetWeightMeasure' => array(
//                'UnitCode' => 'KGM',
//                'Value' => '29.123'
//            ),
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'EA069541875CN',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
//            'PreviousDocument' => array(
//                1 => array(
//                    'DocumentTypeCode' => '740',
//                    'DocumentID' => 'TR-2527'
//                ),
//            ),
            'AdditionalDocument' => array(
                1 => array(
                    'DocumentTypeCode' => '740',
                    'DocumentID' => 'EA069541875CN'
                ),
            ),
            'AdditionalInformation' => null,
//            'TransportEquipment' => array(
//                1 => array('TransportEquipmentID' => 'ABCD1234-5'),
//            ),
            'FreightPaymentMethodCode' => null
        )
    )
);

?>