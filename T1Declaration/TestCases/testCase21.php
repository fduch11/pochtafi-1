﻿<?php

$params = array(

    'TraderReferenceID' => 'Shipping 1',

    'TransitControlResultCode' => 'A3',

    'DepartureTransportMeans' => array(
        'TransportModeCode' => 3,
        'TransportMeansNationalityCode' => 'FI',
        'TransportMeansID' => 'DHL',
        'ConveyanceReferenceID' => null
    ),

    'DispatchCountryCode' => null,
    'DestinationCountryCode' => null,

    'TransitDestinationOffice' => 'FI534200',

    'GoodsItemQuantity' => 1,
    'TotalPackageQuantity' => 1,
    'TotalGrossMassMeasure' => array(
        'UnitCode' => 'KGM',
        'Value' => '5.5'
    ),

    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI0108210-8R0029'
    ),
    'Loading' => array(
        'LoadingDateTime' => date('Y-m-d\TH:i:s'),
        'LocationName' => 'FI015300'
    ),

    'Unloading' => array(
        'LocationName' => 'FI2628792-7R0001'
    ),
	
    'Issue' => array(
        'IssueDate' => date('Y-m-d'),
        'LocationName' => 'Lappeenranta'
    ),
    'TransitLimitDate' => date('Y-m-d', strtotime("+3 days")),
    'ContainerTransportIndicator' => 'false',
    'Sealing' => null,
    'GoodsItem' => array(
        1 => array(
            'UniqueConsignmentReferenceID' => null,
            'TransitTypeCode' => 'T1',
            'DispatchCountryCode' => 'US',
            'DestinationCountryCode' => 'FI',
            'Consignor' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Quality Distributing LLC',
                'Address' => array(
                    'Line' => '1982 NE 25th Ave Ste 12',
                    'PostcodeID' => 'OR 97124',
                    'CityName' => 'Hillsboro',
                    'CountryCode' => 'US'
                ),
            ),
            'ConsignorSecurity' => array(
                'ID' => null,
                'IDExtension' => null,
                'Name' => 'Quality Distributing LLC',
                'Address' => array(
                    'Line' => '1982 NE 25th Ave Ste 12',
                    'PostcodeID' => 'OR 97124',
                    'CityName' => 'Hillsboro',
                    'CountryCode' => 'US'
                ),
            ),
            'Consignee' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),
            'ConsigneeSecurity' => array(
                'ID' => 'FI2628792-7',
                'IDExtension' => 'T0001',
                'Name' => 'Pochta.fi Oy',
                'Address' => array(
                    'Line' => 'Pelkolankatu 5',
                    'PostcodeID' => '53420',
                    'CityName' => 'Lappeenranta',
                    'CountryCode' => 'FI'
                ),
            ),
            'Commodity' => array(
                'TariffClassification' => array(850940),
                'DangerousGoodsCode' => null,
                'GoodsDescription' => 'Blender',
            ),
            'SensitiveGoods' => null,
            'GrossMassMeasure' => array(
                'UnitCode' => 'KGM',
                'Value' => '5.5'
            ),
            'NetWeightMeasure' => null,
            'Packaging' => array(
                1 => array(
                    'PackagingTypeCode' => 'CS',
                    'PackagingMarksID' => 'KSB1575MY 5-Speed',
                    'PackageQuantity' => 1,
                    'PieceCountQuantity' => null
                )
            ),
            'PreviousDocument' => array(
                1 => array(
                    'DocumentTypeCode' => 'T1',
                    'DocumentID' => '1270314673'
                )
            ),
            'AdditionalDocument' => null,
            'AdditionalInformation' => null,
            'TransportEquipment' => null,
            'FreightPaymentMethodCode' => null
        )
    )
);

?>