<?php

$params = array(

    'XMessageType' => 'FITARR',

    'TraderReferenceID' => '15FI000000008982T0',

    'MovementReferenceID' => '15FI000000008982T0',
	
    'GoodsLocation' => array(
        'LocationQualifierCode' => 'L',
        'LocationID' => 'FI2628792-7R0001'
    ),
	'TransitPresentationOffice' => array(
		'CustomsOfficeCode' => 'FI542300'
	),
	'NotificationDate' => '2015-01-26', //date('Y-m-d'), // date('Y-m-d'), //20040304
	'SealConditionIndicator' => 1,      // 'OK',
	'DestinationLanguageCode' => 'FI',

    'ActualAuthorisedConsignee' => array(
        'ID' => 'FI2628792-7',
        'IDExtension' => 'T0001',
        'Name' => 'Pochta.fi Oy',
        'Address' => array(
            'Line' => 'Pelkolankatu 5',
            'PostcodeID' => '53420',
            'CityName' => 'Lappeenranta',
            'CountryCode' => 'FI'
        ),
    )

);

?>