<?xml version="1.0" encoding="UTF-8"?>
<!-- NCTS Direct Message Exchange schema of FITransitArrivalNotification message -->
<xs:schema xmlns:ncts="http://tulli.fi/schema/external/ncts/dme/v1" xmlns:udt="http://tulli.fi/schema/external/common/dme/v1_0/udt" xmlns:cdt="http://tulli.fi/schema/external/common/dme/v1_0/cdt" xmlns:qdt="http://tulli.fi/schema/external/common/dme/v1_0/qdt" xmlns:doc="http://tulli.fi/schema/external/common/dme/v1_0/doc" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://tulli.fi/schema/external/ncts/dme/v1" elementFormDefault="qualified" version="v1_1">
	<!-- Common schema components -->
	<xs:include schemaLocation="NctsDme_QualifiedType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/qdt" schemaLocation="Dme_QualifiedType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/cdt" schemaLocation="Dme_CodeListType.xsd"/>
	<xs:import namespace="http://tulli.fi/schema/external/common/dme/v1_0/udt" schemaLocation="Dme_UnqualifiedType.xsd"/>
	<!-- Root Element -->
	<xs:element name="FITransitArrivalNotification">
		<xs:annotation>
			<xs:documentation xml:lang="en">FITARR - Notification of arrival message (IE07).</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:complexContent>
				<xs:extension base="ncts:FITransitArrivalNotificationType">
					<xs:attribute name="schemaVersion" type="ncts:CompatibleSchemaVersionType" use="required">
						<xs:annotation>
							<xs:documentation xml:lang="en">List of compatible schema versions, containing this version and all accepted earlier compatible versions.</xs:documentation>
						</xs:annotation>
					</xs:attribute>
				</xs:extension>
			</xs:complexContent>
		</xs:complexType>
	</xs:element>
	<xs:complexType name="FITransitArrivalNotificationType">
		<xs:sequence>
			<xs:element name="MessageInterchange" type="ncts:RelatedMessageInterchangeType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Message interchange information.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Notification" type="ncts:NotificationType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Arrival notification.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<!--Notification-->
	<xs:complexType name="NotificationType">
		<xs:sequence>
			<xs:element name="MovementReferenceID" type="qdt:MovementReferenceIDType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Movement reference number (MRN), issued by Customs.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="NotificationDate" type="udt:DateType">
				<xs:annotation>
					<xs:documentation xml:lang="en">The arrival notification date.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="DestinationLanguageCode" type="cdt:LanguageCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">The basic language to be used in any further communication between the Trader and the Customs system.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="TransitPresentationOffice" type="ncts:CustomsOfficeContactType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Customs office of presentation, listed in permission for authorised consignee.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="GoodsLocation" type="ncts:GoodsLocationType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Warehouse where goods can be inspected, listed in permit for authorised consignee.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="SealConditionIndicator" type="udt:IndicatorType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Indicates (value 1) that seals are in order or seals have not been used (no mention of seals on accompanying document).</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="EnRouteEvent" type="ncts:EnRouteEventType" minOccurs="0" maxOccurs="9">
				<xs:annotation>
					<xs:documentation xml:lang="en">En route event (incident, transshipment or change in seals), if such an event has taken place.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ActualAuthorisedConsignee">
				<xs:annotation>
					<xs:documentation xml:lang="en">Actual authorised consignee party.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:complexContent>
						<xs:restriction base="ncts:PartyType">
							<xs:sequence>
								<xs:element name="ID" type="ncts:TraderIDType"/>
								<xs:element name="IDExtension" type="qdt:IDExtensionType"/>
								<xs:element name="Name" type="qdt:NameType" minOccurs="0"/>
								<xs:element name="Address" type="ncts:AddressType" minOccurs="0"/>
							</xs:sequence>
						</xs:restriction>
					</xs:complexContent>
				</xs:complexType>
			</xs:element>
			<xs:element name="RepresentativePerson" type="ncts:RepresentativePersonPartyType">
				<xs:annotation>
					<xs:documentation xml:lang="en">Person lodging the declaration.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EnRouteEventType">
		<xs:sequence>
			<xs:element name="LocationName" type="qdt:NameType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Place for en route event.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="CountryCode" type="cdt:CountryCodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Country code for en route event.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Incident" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">En route event incident details.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="IncidentIndicator" type="udt:IndicatorType" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">Idicator for reporting an incident.</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="Description" type="qdt:DescriptionType" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">Incident descriping text.</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="Endorsement" type="ncts:EndorsementType" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">Endorsement of the reportable event that an authority has confirmed.</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="Control" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Control for transshipment or change in seals.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="NotifiedIndicator" type="udt:IndicatorType" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">Idicator of transshipment and/or change in seals having already been notified in NCTS.</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="TransShipment" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Details for transshipment en route.</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element name="Endorsement" type="ncts:EndorsementType" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">Endorsement of the reportable event that an authority has confirmed.</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element name="NewTransportMeans" minOccurs="0">
							<xs:annotation>
								<xs:documentation xml:lang="en">New transport means information.</xs:documentation>
							</xs:annotation>
							<xs:complexType>
								<xs:complexContent>
									<xs:restriction base="ncts:TransportMeansType">
										<xs:sequence>
											<xs:element name="TransportMeansNationalityCode" type="cdt:CountryCodeType" minOccurs="0"/>
											<xs:element name="TransportMeansID" type="ncts:TransportMeansIDType" minOccurs="0"/>
										</xs:sequence>
									</xs:restriction>
								</xs:complexContent>
							</xs:complexType>
						</xs:element>
						<xs:element name="NewTransportEquipment" type="ncts:TransportEquipmentType" minOccurs="0" maxOccurs="99">
							<xs:annotation>
								<xs:documentation xml:lang="en">The new container number, written together: four letters (if they exist) + six numbers + hyphen + control number.</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
				</xs:complexType>
			</xs:element>
			<xs:element name="Sealing" type="ncts:SealingType" minOccurs="0">
				<xs:annotation>
					<xs:documentation xml:lang="en">Seals information.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
