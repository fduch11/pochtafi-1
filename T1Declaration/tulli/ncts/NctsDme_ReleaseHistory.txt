﻿NCTS Direct Message Exchange schema - Release History
=====================================================

2014-03-05
	Release v1_1 defining target namespace http://tulli.fi/schema/external/ncts/dme/v1
	- Version v1_1 added as a new enumeration value For CompatibleSchemaVersionType
	  in NctsDme_QualifiedType.xsd and value updated for version attribute in all schema files.
	- In NctsDme_QualifiedType.xsd a new complex type TransitBorderOfficeContactType 
	  is created to change optional the element TransitBorderOffice/CustomsOfficeCode 
	  (this is uded in NctsDme_FITransitDeclaration.xsd instead of CustomsOfficeContactType).
	- In NctsDme_QualifiedType.xsd element DocumentID in complex type PreviousDocumentType 
	  changed to optional (this is uded in NctsDme_FITransitDeclaration.xsd).
	- In NctsDme_QualifiedType.xsd for simple type PreviousDocumentIDType the maxLength 
	  facet value inreased from 20 to 35 (this is uded in NctsDme_FITransitDeclaration.xsd).

2013-09-30
	Release v1_0 defining target namespace http://tulli.fi/schema/external/ncts/dme/v1