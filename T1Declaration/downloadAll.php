<?php

set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme');
set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/ncts');
//set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme/fi/tu');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';

try {

    $local_cert = 'certificates/pem_pochta.cer';

    $soapUrl = 'https://ws-customertest.tulli.fi/services/DirectMessageExchange';
//    $soapUrl = 'https://ws.tulli.fi/services/DirectMessageExchange';
//    $soapUrl = 'http://localhost:33816/CorporateMessageServiceHttpBinding.asmx';

    $soapParams = array(
//        'proxy_host' => 'localhost',
//        'proxy_port' => 8888,

        'trace' => 1,
        'exceptions' => true,
        'local_cert' => $local_cert,
        'encoding' => 'UTF-8'
    );

    $outputDir = 'C:\\Sandbox\\T1Declaration\\results\\all\\';

    include('TestCases/fixedCase.php');
//    $fixedParams['Environment'] = 'PRODUCTION';

    /* ---------  Download List -------------- */

    $requestHeader = new RequestHeader();
    $requestHeader->IntermediaryBusinessId = $fixedParams['BusinessId'];
    $requestHeader->IntermediarySoftwareInfo = $fixedParams['SoftwareInfo'];
    $requestHeader->Timestamp = time();
    $requestHeader->Language = "EN";

    $criteria = new DownloadMessageListFilteringCriteria(date('Y-m-d', strtotime("-2 days")), date('Y-m-d'), null, null, 'NEW');
    //$criteria = new DownloadMessageListFilteringCriteria('2014-11-05', date('Y-m-d'), null, null, 'ALL');
    //$criteria = new DownloadMessageListFilteringCriteria(null, null, null, null, 'NEW');

    $downloadListRequest = new DownloadListRequest();
    $downloadListRequest->RequestHeader = $requestHeader;
    $downloadListRequest->DownloadMessageListFilteringCriteria = $criteria;

    $soapClient = new SoapClient('tulli\dme\CustomsCorporateService_soap1_2.wsdl', $soapParams);
    $soapClient->__setLocation($soapUrl);
    $result = $soapClient->__soapCall('DownloadList', array($downloadListRequest));

    $storageReferences = array();

    /** @var $result DownloadListResponse */
    $responseHeader = $result->ResponseHeader;
    if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {
        if (is_array($result->MessageInformation)) {
            foreach ($result->MessageInformation as $responseMessage) {
                $storageReferences[] = $responseMessage->MessageStorageId;

                $text = $responseMessage->ControlReference . PHP_EOL . $responseMessage->MessageStorageId;
                file_put_contents($outputDir . $responseMessage->MessageStorageId . '.properties.txt', $text);
            }
        } else if (is_object($result->MessageInformation)) {
            $storageReferences[] = $result->MessageInformation->MessageStorageId;

            $text = $result->MessageInformation->ControlReference . PHP_EOL . $result->MessageInformation->MessageStorageId;
            file_put_contents($outputDir . $result->MessageInformation->MessageStorageId . '.properties.txt', $text);
        }
    } else {
        echo $responseHeader->ResponseText;
    }

    foreach ($storageReferences as $storageReference) {

        /* ---------  Download  -------------- */

        $criteria = new DownloadMessageFilteringCriteria($storageReference);

        $downloadRequest = new DownloadRequest();
        $requestHeader->Timestamp = time();
        $downloadRequest->RequestHeader = $requestHeader;
        $downloadRequest->DownloadMessageFilteringCriteria = $criteria;

        $result = $soapClient->__soapCall('Download', array($downloadRequest));

        /** @var $result DownloadResponse */
        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {

            //file_put_contents($outputDir . $storageReference . '.content.enc', $result->ApplicationResponseMessage);

            $xmlObject = new SimpleXMLElement($result->ApplicationResponseMessage);
            $content = $xmlObject->children("v1", true)->ApplicationResponseContent;
            $text = base64_decode($content->children("v11", true)->Content);

            $doc = new DOMDocument();
            $doc->loadXML($text);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            $doc->save($outputDir . $storageReference . '.response.xml');

            $content = $xmlObject->children("v1", true)->AttachmentOfApplicationResponseContent;
            if (!empty($content)) {
                $text = base64_decode($content->children("v11", true)->Content);
                file_put_contents($outputDir . $storageReference . '.zip', $text);
            }
        }

        sleep(2);
    }


}
catch (SoapFault $soapFault) {
    echo $soapFault;
    var_dump($soapFault);
}
catch (Exception $ex) {
    echo $ex;
    var_dump($ex);
}

?>