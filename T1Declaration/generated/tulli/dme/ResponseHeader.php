<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName ResponseHeader
 * @var ResponseHeader
 * @xmlDefinition This element contains response information related to the transmitted data.
 */
class ResponseHeader
	{



	/**                                                                       
		@param fi\tulli\schema\corporateservice\v1\BusinessId $IntermediaryBusinessId [optional] The business identity code of the message intermediary of request message. The intermediary can be a third party organization.
		@param  $Timestamp [optional] Time and date when the response was sent.
		@param  $ResponseCode [optional] This code is used to indicate the message delivery condition.
		@param  $ResponseText [optional] The textual explanation of the condition.
	*/                                                                        
	public function __construct($IntermediaryBusinessId = null, $Timestamp = null, $ResponseCode = null, $ResponseText = null, $TransactionId = null)
	{
		$this->IntermediaryBusinessId = $IntermediaryBusinessId;
		$this->Timestamp = $Timestamp;
		$this->ResponseCode = $ResponseCode;
		$this->ResponseText = $ResponseText;
		$this->TransactionId = $TransactionId;
	}
	
	/**
	 * @Definition The business identity code of the message intermediary of request message. The intermediary can be a third party organization.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName IntermediaryBusinessId
	 * @var fi\tulli\schema\corporateservice\v1\BusinessId
	 */
	public $IntermediaryBusinessId;
	/**
	 * @Definition Time and date when the response was sent.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName Timestamp
	 */
	public $Timestamp;
	/**
	 * @Definition This code is used to indicate the message delivery condition.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseCode
	 */
	public $ResponseCode;
	/**
	 * @Definition The textual explanation of the condition.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseText
	 */
	public $ResponseText;
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName TransactionId
	 * @var fi\tulli\ws\corporateservicetypes\v1\TransactionId
	 */
	public $TransactionId;


} // end class ResponseHeader
