<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DownloadMessageFilteringCriteria
 * @var DownloadMessageFilteringCriteria
 * @xmlDefinition These elements are used to specify one message to download
 */
class DownloadMessageFilteringCriteria
	{



	/**                                                                       
	*/                                                                        
	public function __construct($MessageStorageId = null)
	{
		$this->MessageStorageId = $MessageStorageId;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName MessageStorageId
	 * @var fi\tulli\schema\corporateservice\v1\MessageStorageId
	 */
	public $MessageStorageId;


} // end class DownloadMessageFilteringCriteria
