<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName DownloadResponse
 * @var DownloadResponse
 * @xmlDefinition Response element for download a message operation.
 */
class DownloadResponse
	{



	/**                                                                       
		@param fi\tulli\ws\corporateservicetypes\v1\MessageInformation $ApplicationResponseMessageInformation [optional] Not present when an error has occurred.
		@param string $ApplicationResponseMessage [optional] Not present when an error has occurred.
	*/                                                                        
	public function __construct($ResponseHeader = null, $ApplicationResponseMessageInformation = null, $ApplicationResponseMessage = null)
	{
		$this->ResponseHeader = $ResponseHeader;
		$this->ApplicationResponseMessageInformation = $ApplicationResponseMessageInformation;
		$this->ApplicationResponseMessage = $ApplicationResponseMessage;
	}
	
	/**
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlName ResponseHeader
	 * @var fi\tulli\ws\corporateservicetypes\v1\ResponseHeader
	 */
	public $ResponseHeader;
	/**
	 * @Definition Not present when an error has occurred.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName ApplicationResponseMessageInformation
	 * @var fi\tulli\ws\corporateservicetypes\v1\MessageInformation
	 */
	public $ApplicationResponseMessageInformation;
	/**
	 * @Definition Not present when an error has occurred.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/ws/corporateservicetypes/v1
	 * @xmlMinOccurs 0
	 * @xmlName ApplicationResponseMessage
	 * @var string
	 */
	public $ApplicationResponseMessage;


} // end class DownloadResponse
