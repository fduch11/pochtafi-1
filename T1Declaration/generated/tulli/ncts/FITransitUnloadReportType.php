<?php

/**
 * @xmlNamespace 
 * @xmlType 
 * @xmlName FITransitUnloadReport
 * @var FITransitUnloadReportType
 * @xmlDefinition GoodsItem/ItemSequenceNumber is unique within message.
 */
class FITransitUnloadReportType
	{



	/**                                                                       
		@param fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType $MessageInterchange [optional] Message interchange information.
		@param fi\tulli\schema\external\ncts\dme\v1\ReportType $Report [optional] Unloading report.
		@param fi\tulli\schema\external\ncts\dme\v1\ReportGoodsItemType $GoodsItem [optional] Goods item information.
	*/                                                                        
	public function __construct($MessageInterchange = null, $Report = null, $GoodsItem = null)
	{
		$this->MessageInterchange = $MessageInterchange;
		$this->Report = $Report;
		$this->GoodsItem = $GoodsItem;
	}
	
	/**
	 * @Definition Message interchange information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName MessageInterchange
	 * @var fi\tulli\schema\external\ncts\dme\v1\RelatedMessageInterchangeType
	 */
	public $MessageInterchange;
	/**
	 * @Definition Unloading report.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlName Report
	 * @var fi\tulli\schema\external\ncts\dme\v1\ReportType
	 */
	public $Report;
	/**
	 * @Definition Goods item information.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMaxOccurs 9999
	 * @xmlName GoodsItem
	 * @var fi\tulli\schema\external\ncts\dme\v1\ReportGoodsItemType
	 */
	public $GoodsItem;


} // end class FITransitUnloadReportType
