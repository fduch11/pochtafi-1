<?php

/**
 * @xmlNamespace 
 * @xmlType MessageInterchangeType
 * @xmlName RelatedMessageInterchangeType
 * @var RelatedMessageInterchangeType
 */
class RelatedMessageInterchangeType
		{



	/**                                                                       
		@param fi\tulli\schema\external\common\dme\v1_0\qdt\MessageReferenceIDType $ControlReferenceID [optional] The identifier of the previous message interchange refered to.
	*/                                                                        
	public function __construct($ControlReferenceID = null)
	{
		$this->ControlReferenceID = $ControlReferenceID;
	}

    /**
     * @Definition Unique business identifier of the sender.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName MessageSenderID
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType
     */
    public $MessageSenderID;
    /**
     * @Definition Unique business identifier of the recipient.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName MessageRecipientID
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\BusinessIDType
     */
    public $MessageRecipientID;
    /**
     * @Definition Date and time of preparation of this message.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName MessageDateTime
     * @var fi\tulli\schema\external\common\dme\v1_0\udt\DateTimeType
     */
    public $MessageDateTime;
    /**
     * @Definition Unique identifier of this message interchange.
     * @xmlType element
     * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
     * @xmlName MessageReferenceID
     * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MessageReferenceIDType
     */
    public $MessageReferenceID;
	
	/**
	 * @Definition The identifier of the previous message interchange refered to.
	 * @xmlType element
	 * @xmlNamespace http://tulli.fi/schema/external/ncts/dme/v1
	 * @xmlMinOccurs 0
	 * @xmlName ControlReferenceID
	 * @var fi\tulli\schema\external\common\dme\v1_0\qdt\MessageReferenceIDType
	 */
	public $ControlReferenceID;


} // end class RelatedMessageInterchangeType
