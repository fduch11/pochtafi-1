<?php
/**
 * Created by IntelliJ IDEA.
 * User: VMikhaylov
 * Date: 24.10.2014
 * Time: 12:03
 */

set_include_path(get_include_path().PATH_SEPARATOR.'tulli/dme');
set_include_path(get_include_path().PATH_SEPARATOR.'tulli/ncts');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';

class Tulli
{

    /**
     * @var string
     */
    private $soapUrl;

    /**
     * @var array
     */
    private $soapParams;


    /**
     * @param string $soapUrl
     * @param array  $soapParams
     */
    function __construct($soapUrl, $soapParams)
    {
        $this->soapUrl = $soapUrl;
        $this->soapParams = $soapParams;
    }

    /**
     *
     */
    public function sendT1($fixedParams, $params, &$tmpContent)
    {
        $ts = substr(date('U'), 1);
        $MessageReferenceID = '00000' . $ts;  // 5 + 9 chars
        $ControlReferenceID = 'POCHT' . $ts;

        $requestHeader = new RequestHeader();
        $requestHeader->IntermediaryBusinessId = $fixedParams['BusinessId'];
        $requestHeader->IntermediarySoftwareInfo = $fixedParams['SoftwareInfo'];
        $requestHeader->Timestamp = time();
        $requestHeader->Language = "EN";


        $messageInterchange = new RelatedMessageInterchangeType();
        $messageInterchange->MessageSenderID = $fixedParams['BusinessId'];
        $messageInterchange->MessageRecipientID = $fixedParams['RecipientId'];
        $messageInterchange->MessageDateTime = date('Y-m-d\TH:i:s');
        $messageInterchange->MessageReferenceID = $MessageReferenceID;
        $messageInterchange->ControlReferenceID = $ControlReferenceID;


        if ($params['XMessageType'] == 'FITARR') {
            $tmpContent = $this->buildFITransitArrivalNotification($fixedParams, $params, $messageInterchange);
        } else if ($params['XMessageType'] == 'FITULR') {
            $tmpContent = $this->buildFITransitUnloadReport($fixedParams, $params, $messageInterchange);
        } else {
            $tmpContent = $this->buildFITransitDeclaration($fixedParams, $params, $messageInterchange);
        }

        $appContent = new ApplicationContent();
        $appContent->Content = base64_encode($tmpContent);
        $appContent->ContentFormat = 'application/xml';


        $appRequest = new ApplicationRequestType();
        $appRequest->MessageBuilderBusinessId = $fixedParams['BusinessId'];
        $appRequest->MessageBuilderSoftwareInfo = $fixedParams['SoftwareInfo'];
        $appRequest->DeclarantBusinessId = $fixedParams['BusinessId'];
        $appRequest->Timestamp = date('c');
        $appRequest->Application = 'NCTS';
        $appRequest->Reference = $ControlReferenceID;
        $appRequest->Environment = $fixedParams['Environment'];
        $appRequest->ApplicationContent = $appContent;

        // sign request
        $php2xml = new \com\mikebevz\xsd2php\Php2Xml('');
        $text = $php2xml->getXml($appRequest);
        $xmlDocument = new DOMDocument();
        $xmlDocument->loadXML($text);

        $xmlTool = new FR3D\XmlDSig\Adapter\XmlseclibsAdapter();
        $xmlTool->setPublicKey(file_get_contents('C:\Dropbox\Biz.Pochta.fi\Certificate\pem_pochta.cer'), 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256');
        $xmlTool->setPrivateKey(file_get_contents('C:\Dropbox\Biz.Pochta.fi\Certificate\pochta.fi.pvtk.txt'), 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256');
        $xmlTool->addTransform(FR3D\XmlDSig\Adapter\XmlseclibsAdapter::ENVELOPED);
        $xmlTool->addTransform('http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments');
        $xmlTool->setDigestAlgorithm('http://www.w3.org/2001/04/xmlenc#sha256');
        $xmlTool->sign($xmlDocument);
        $applicationRequestMessage = $xmlDocument->saveXML();


        $uploadRequest = new UploadRequest();
        $uploadRequest->RequestHeader = $requestHeader;
        $uploadRequest->ApplicationRequestMessage = $applicationRequestMessage;

        $soapClient = new SoapClient('tulli\dme\CustomsCorporateService_soap1_2.wsdl', $this->soapParams);
        $soapClient->__setLocation($this->soapUrl);

        $result = $soapClient->__soapCall('Upload', array($uploadRequest));

        return $result;
    }

    private function buildFITransitDeclaration($fixedParams, $params, $messageInterchange)
    {
        $transitBorderOffice = null;
        if (isset($params['TransitBorderOffice']) && is_array($params['TransitBorderOffice']) && count($params['TransitBorderOffice']) > 0) {
            foreach ($params['TransitBorderOffice'] as $borderOfficeKey => $borderOfficeValue) {
                $transitBorderOffice[] = new CustomsOfficeContactType($borderOfficeValue);
            }
        }

        $itinerary = null;
        if (isset($params['Itinerary']) && is_array($params['Itinerary']) && count($params['Itinerary']) > 0) {
            $routingCountryCodes = array();
            foreach ($params['Itinerary'] as $itineraryKey => $itineraryValue) {
                $routingCountryCodes[] = $itineraryValue['RoutingCountryCode'];
            }
            $itinerary = new ItineraryType($routingCountryCodes);
        }

        $declaration = new DeclarationType();
        $declaration->MovementReferenceID = $params['MovementReferenceID'];
        $declaration->TransitControlResultCode = $params['TransitControlResultCode'];
        $declaration->TraderReferenceID = $params['TraderReferenceID'];
        $declaration->FunctionCode = isset($params['FunctionCode']) ? $params['FunctionCode'] : 9;
        $declaration->Issue = isset($params['Issue']) ? new IssueType($params['Issue']['IssueDate'], $params['Issue']['LocationName']) : null;
        $declaration->Itinerary = $itinerary;
        $declaration->TransitLimitDate = $params['TransitLimitDate'];
        $declaration->TransitDepartureOffice = isset($params['TransitDepartureOffice']) ? new CustomsOfficeContactType($params['TransitDepartureOffice']) : isset($fixedParams['TransitDepartureOffice']) ? new CustomsOfficeContactType($fixedParams['TransitDepartureOffice']) : null;
        $declaration->TransitDestinationOffice = isset($params['TransitDestinationOffice']) ? new CustomsOfficeContactType($params['TransitDestinationOffice']) : null;
        $declaration->TransitBorderOffice = $transitBorderOffice;
        $declaration->Loading = isset($params['Loading']) ? new LoadingType($params['Loading']['LoadingDateTime'], $params['Loading']['LocationName']) : null;
        $declaration->Unloading = isset($params['Unloading']) ? new LoadingType($params['Unloading']['UnloadingDateTime'], $params['Unloading']['LocationName']) : null;
        $declaration->GoodsLocation = isset($params['GoodsLocation']) ? new GoodsLocationType($params['GoodsLocation']['LocationQualifierCode'], $params['GoodsLocation']['LocationID'] ) : null;
        $declaration->DepartureTransportMeans = isset($params['DepartureTransportMeans']) ? new TransportMeansType($params['DepartureTransportMeans']['TransportModeCode'], $params['DepartureTransportMeans']['TransportMeansNationalityCode'],
            $params['DepartureTransportMeans']['TransportMeansID'], $params['DepartureTransportMeans']['ConveyanceReferenceID']) : null;
        $declaration->BorderTransportMeans = isset($params['BorderTransportMeans']) ? new TransportMeansType($params['BorderTransportMeans']['TransportModeCode'], $params['BorderTransportMeans']['TransportMeansNationalityCode'],
            $params['BorderTransportMeans']['TransportMeansID'], $params['BorderTransportMeans']['ConveyanceReferenceID']) : null;
        $declaration->ContainerTransportIndicator = isset($params['ContainerTransportIndicator']) ? $params['ContainerTransportIndicator'] : 'false';
        $declaration->Sealing = isset($params['Sealing']) ? new SealingType($params['Sealing']['SealQuantity'], $params['Sealing']['SealID']) : null;
        $declaration->GoodsItemQuantity = $params['GoodsItemQuantity'];
        $declaration->TotalPackageQuantity = $params['TotalPackageQuantity'];
        $declaration->TotalGrossMassMeasure = isset($params['TotalGrossMassMeasure']) ? new MeasureType($params['TotalGrossMassMeasure']['UnitCode'], $params['TotalGrossMassMeasure']['Value']) : null;

        $declaration->RepresentativePerson = isset($fixedParams['RepresentativePerson']) ? new RepresentativePersonPartyType($fixedParams['RepresentativePerson']) : null;
        $declaration->Principal = isset($fixedParams['Principal']) ? new PrincipalPartyType($fixedParams['BusinessId'], $fixedParams['Principal']['IDExtension'], $fixedParams['Principal']['Name'],
            isset($fixedParams['Principal']['Address']) ? new AddressType($fixedParams['Principal']['Address']['Line'], $fixedParams['Principal']['Address']['PostcodeID'],
                $fixedParams['Principal']['Address']['CityName'], $fixedParams['Principal']['Address']['CountryCode']) : null,
            $fixedParams['Principal']['NatureQualifierCode'], $fixedParams['Principal']['LanguageCode'], $fixedParams['Principal']['TIRHolderID']) : null;
        $declaration->Guarantee = isset($fixedParams['Guarantee']) ? new GuaranteeType($fixedParams['Guarantee']['TransitGuaranteeTypeCode'], $fixedParams['Guarantee']['GuaranteeReferenceID'],
            $fixedParams['Guarantee']['OtherGuaranteeReferenceID'], $fixedParams['Guarantee']['AccessKeyCodeID'], $fixedParams['Guarantee']['ValidityLimitationIndicator'],
            $fixedParams['Guarantee']['ValidityLimitationCountryCode']) : null;

        $declaration->TransitTypeCode = $params['TransitTypeCode'];
        $declaration->DestinationCountryCode = $params['DestinationCountryCode'];
        $declaration->DispatchCountryCode = $params['DispatchCountryCode'];

        $declaration->Consignee = isset($params['Consignee']) ? new PartyType($params['Consignee']['ID'], $params['Consignee']['IDExtension'], $params['Consignee']['Name'],
            isset($params['Consignee']['Address']) ? new AddressType($params['Consignee']['Address']['Line'], $params['Consignee']['Address']['PostcodeID'],
                $params['Consignee']['Address']['CityName'], $params['Consignee']['Address']['CountryCode']) : null) : null;
        $declaration->ConsigneeSecurity = isset($params['ConsigneeSecurity']) ? new PartyType($params['ConsigneeSecurity']['ID'], $params['ConsigneeSecurity']['IDExtension'], $params['ConsigneeSecurity']['Name'],
            isset($params['ConsigneeSecurity']['Address']) ? new AddressType($params['ConsigneeSecurity']['Address']['Line'], $params['ConsigneeSecurity']['Address']['PostcodeID'],
                $params['ConsigneeSecurity']['Address']['CityName'], $params['ConsigneeSecurity']['Address']['CountryCode']) : null) : null;
        $declaration->Consignor = isset($params['Consignor']) ? new PartyType($params['Consignor']['ID'], $params['Consignor']['IDExtension'], $params['Consignor']['Name'],
            isset($params['Consignor']['Address']) ? new AddressType($params['Consignor']['Address']['Line'], $params['Consignor']['Address']['PostcodeID'],
                $params['Consignor']['Address']['CityName'], $params['Consignor']['Address']['CountryCode']) : null) : null;
        $declaration->ConsignorSecurity = isset($params['ConsignorSecurity']) ? new PartyType($params['ConsignorSecurity']['ID'], $params['ConsignorSecurity']['IDExtension'], $params['ConsignorSecurity']['Name'],
            isset($params['ConsignorSecurity']['Address']) ? new AddressType($params['ConsignorSecurity']['Address']['Line'], $params['ConsignorSecurity']['Address']['PostcodeID'],
                $params['ConsignorSecurity']['Address']['CityName'], $params['ConsignorSecurity']['Address']['CountryCode']) : null) : null;


        $fiTransitDeclaration = new FITransitDeclarationType();
        $fiTransitDeclaration->MessageInterchange = $messageInterchange;


        $transitTypeCode = null;
        $dispatchCountryCode = null;
        $destinationCountryCode = null;
        $consignor = null;
        $consignorSecurity = null;
        $consignee = null;
        $consigneeSecurity = null;

        $goodsItem = null;
        if (isset($params['GoodsItem'])) {
            foreach ($params['GoodsItem'] as $key => $value) {

                $previousDocument = null;
                if (isset($value['PreviousDocument'])) {
                    foreach ($value['PreviousDocument'] as $prevDocKey => $prevDocValue) {
                        $previousDocument[] = new PreviousDocumentType($prevDocValue['DocumentTypeCode'], $prevDocValue['DocumentID'], $prevDocValue['ItemSequenceNumber']);
                    }
                }

                $packaging = null;
                if (isset($value['Packaging'])) {
                    foreach ($value['Packaging'] as $packagingKey => $packagingValue) {
                        $packaging[] = new PackagingType($packagingValue['PackagingTypeCode'], $packagingValue['PackagingMarksID'], $packagingValue['PackageQuantity'], $packagingValue['PieceCountQuantity']);
                    }
                }

                $tariffClassification = null;
                if (isset($value['Commodity']) && isset($value['Commodity']['TariffClassification'])) {
                    foreach ($value['Commodity']['TariffClassification'] as $tariffKey => $tariffValue) {
                        $tariffClassification[] = new TariffClassificationType($tariffValue);
                    }
                }

                $additionalDocument = null;
                if (isset($value['AdditionalDocument'])) {
                    foreach ($value['AdditionalDocument'] as $addDocKey => $addDocValue) {
                        $additionalDocument[] = new DocumentType($addDocValue['DocumentTypeCode'], $addDocValue['DocumentID'], $addDocValue['SupplementaryInformation']);
                    }
                }

                $transportEquipment = null;
                if (isset($value['TransportEquipment'])) {
                    foreach ($value['TransportEquipment'] as $transEquipKey => $transEquipValue) {
                        $transportEquipment[] = new TransportEquipmentType($transEquipValue['TransportEquipmentID']);
                    }
                }


                if (isset($transitTypeCode) && $transitTypeCode != $value['TransitTypeCode']) {
                    $transitTypeCode = '';
                } else {
                    $transitTypeCode = $value['TransitTypeCode'];
                }
                if (isset($destinationCountryCode) && $destinationCountryCode != $value['DestinationCountryCode']) {
                    $destinationCountryCode = '';
                } else {
                    $destinationCountryCode = $value['DestinationCountryCode'];
                }
                if (isset($dispatchCountryCode) && $dispatchCountryCode != $value['DispatchCountryCode']) {
                    $dispatchCountryCode = '';
                } else {
                    $dispatchCountryCode = $value['DispatchCountryCode'];
                }

                $tmpConsignor = null;
                $tmpConsignorSecurity = null;
                $tmpConsignee = null;
                $tmpConsigneeSecurity = null;

                $tmpConsignor = isset($value['Consignor']) ? new PartyType($value['Consignor']['ID'], $value['Consignor']['IDExtension'], $value['Consignor']['Name'],
                    isset($value['Consignor']['Address']) ? new AddressType($value['Consignor']['Address']['Line'], $value['Consignor']['Address']['PostcodeID'],
                        $value['Consignor']['Address']['CityName'], $value['Consignor']['Address']['CountryCode']) : null) : null;

                $tmpConsignorSecurity = isset($value['ConsignorSecurity']) ? new PartyType($value['ConsignorSecurity']['ID'], $value['ConsignorSecurity']['IDExtension'], $value['ConsignorSecurity']['Name'],
                    isset($value['ConsignorSecurity']['Address']) ? new AddressType($value['ConsignorSecurity']['Address']['Line'], $value['ConsignorSecurity']['Address']['PostcodeID'],
                        $value['ConsignorSecurity']['Address']['CityName'], $value['ConsignorSecurity']['Address']['CountryCode']) : null) : null;

                $tmpConsignee = isset($value['Consignee']) ? new PartyType($value['Consignee']['ID'], $value['Consignee']['IDExtension'], $value['Consignee']['Name'],
                    isset($value['Consignee']['Address']) ? new AddressType($value['Consignee']['Address']['Line'], $value['Consignee']['Address']['PostcodeID'],
                        $value['Consignee']['Address']['CityName'], $value['Consignee']['Address']['CountryCode']) : null) : null;

                $tmpConsigneeSecurity = isset($value['ConsigneeSecurity']) ? new PartyType($value['ConsigneeSecurity']['ID'], $value['ConsigneeSecurity']['IDExtension'], $value['ConsigneeSecurity']['Name'],
                    isset($value['ConsigneeSecurity']['Address']) ? new AddressType($value['ConsigneeSecurity']['Address']['Line'], $value['ConsigneeSecurity']['Address']['PostcodeID'],
                        $value['ConsigneeSecurity']['Address']['CityName'], $value['ConsigneeSecurity']['Address']['CountryCode']) : null) : null;

                if ($consignor === true || (isset($consignor) && $consignor != $tmpConsignor)) {
                    $consignor = true;
                } else {
                    $consignor = $tmpConsignor;
                }
                if ($consignorSecurity === true || (isset($consignorSecurity) && $consignorSecurity != $tmpConsignorSecurity)) {
                    $consignorSecurity = true;
                } else {
                    $consignorSecurity = $tmpConsignorSecurity;
                }
                if ($consignee === true || (isset($consignee) && $consignee != $tmpConsignee)) {
                    $consignee = true;
                } else {
                    $consignee = $tmpConsignee;
                }
                if ($consigneeSecurity === true || (isset($consigneeSecurity) && $consigneeSecurity != $tmpConsigneeSecurity)) {
                    $consigneeSecurity = true;
                } else {
                    $consigneeSecurity = $tmpConsigneeSecurity;
                }


                $item = new DeclarationGoodsItemType($key,
                    $value['UniqueConsignmentReferenceID'],
                    $value['TransitTypeCode'],
                    $previousDocument,
                    $additionalDocument,
                    isset($value['AdditionalInformation']) ? new AdditionalInformationType($value['AdditionalInformation']['SpecialMentionCode'], $value['AdditionalInformation']['StatementDescription'],
                        $value['AdditionalInformation']['EUExportIndicator'], $value['AdditionalInformation']['ExportingCountryCode']) : null,
                    $value['DispatchCountryCode'],
                    $value['DestinationCountryCode'],
                    $transportEquipment,
                    //isset($value['TransportEquipment']) ? new TransportEquipmentType($value['TransportEquipment']['TransportEquipmentID']) : null,
                    $packaging,
                    isset($value['Commodity']) ? new CommodityType($tariffClassification, $value['Commodity']['DangerousGoodsCode'], $value['Commodity']['GoodsDescription']) : null,
                    isset($value['SensitiveGoods']) ? new SensitiveGoodsType($value['SensitiveGoods']['SensitiveGoodsCode'], $value['SensitiveGoods']['SensitiveGoodsMeasure']) : null,
                    isset($value['GrossMassMeasure']) ? new MeasureType($value['GrossMassMeasure']['UnitCode'], $value['GrossMassMeasure']['Value']) : null,
                    isset($value['NetWeightMeasure']) ? new MeasureType($value['NetWeightMeasure']['UnitCode'], $value['NetWeightMeasure']['Value']) : null,

                    $tmpConsignor, $tmpConsignorSecurity, $tmpConsignee, $tmpConsigneeSecurity,

                    $value['FreightPaymentMethodCode']
                );

                $goodsItem[] = $item;
            }
        }

        if (isset($transitTypeCode)) {
            if ($transitTypeCode == '') {
                $declaration->TransitTypeCode = 'T-';
            } else {
                $declaration->TransitTypeCode = $transitTypeCode;
                foreach ($goodsItem as $item) {
                    $item->TransitTypeCode = null;
                }
            }
        }

        if (isset($destinationCountryCode)) {
            if ($destinationCountryCode == '') {
                $declaration->DestinationCountryCode = null;
            } else {
                $declaration->DestinationCountryCode = $destinationCountryCode;
                foreach ($goodsItem as $item) {
                    $item->DestinationCountryCode = null;
                }
            }
        }
        if (isset($dispatchCountryCode)) {
            if ($dispatchCountryCode == '') {
                $declaration->DispatchCountryCode = null;
            } else {
                $declaration->DispatchCountryCode = $dispatchCountryCode;
                foreach ($goodsItem as $item) {
                    $item->DispatchCountryCode = null;
                }
            }
        }

        if (isset($consignee)) {
            if ($consignee === true) {
                $declaration->Consignee = null;
            } else {
                $declaration->Consignee = $consignee;
                foreach ($goodsItem as $item) {
                    $item->Consignee = null;
                }
            }
        }
        if (isset($consigneeSecurity)) {
            if ($consigneeSecurity === true) {
                $declaration->ConsigneeSecurity = null;
            } else {
                $declaration->ConsigneeSecurity = $consigneeSecurity;
                foreach ($goodsItem as $item) {
                    $item->ConsigneeSecurity = null;
                }
            }
        }
        if (isset($consignor)) {
            if ($consignor === true) {
                $declaration->Consignor = null;
            } else {
                $declaration->Consignor = $consignor;
                foreach ($goodsItem as $item) {
                    $item->Consignor = null;
                }
            }
        }
        if (isset($consignorSecurity)) {
            if ($consignorSecurity === true) {
                $declaration->ConsignorSecurity = null;
            } else {
                $declaration->ConsignorSecurity = $consignorSecurity;
                foreach ($goodsItem as $item) {
                    $item->ConsignorSecurity = null;
                }
            }
        }

        $fiTransitDeclaration->GoodsItem = $goodsItem;
        $fiTransitDeclaration->Declaration = $declaration;


        $namespaces = array(
            'http://tulli.fi/schema/external/ncts/dme/v1' => 'ncts'
        );

        $php2xml = new \com\mikebevz\xsd2php\Php2Xml('', null, $namespaces);
        $tmpContent = $php2xml->getXml($fiTransitDeclaration);
        $tmpContent = str_replace('<FITransitDeclaration>', '<ncts:FITransitDeclaration schemaVersion="v1_1" xsi:schemaLocation="http://tulli.fi/schema/external/ncts/dme/v1 NctsDme_FITransitDeclaration.xsd" xmlns:ncts="http://tulli.fi/schema/external/ncts/dme/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', $tmpContent);
        $tmpContent = str_replace('</FITransitDeclaration>', '</ncts:FITransitDeclaration>', $tmpContent);

        return $tmpContent;
    }

    private function buildFITransitArrivalNotification($fixedParams, $params, $messageInterchange)
    {
        $notification = new NotificationType();
        $notification->MovementReferenceID = $params['MovementReferenceID'];
        $notification->NotificationDate = $params['NotificationDate'];
        $notification->DestinationLanguageCode = $params['DestinationLanguageCode'];
        $notification->TransitPresentationOffice = isset($params['TransitPresentationOffice']) ? new CustomsOfficeContactType($params['TransitPresentationOffice']) : null;
        $notification->GoodsLocation = isset($params['GoodsLocation']) ? new GoodsLocationType($params['GoodsLocation']['LocationQualifierCode'], $params['GoodsLocation']['LocationID'] ) : null;
        $notification->SealConditionIndicator = $params['SealConditionIndicator'];

        $notification->EnRouteEvent = isset($params['EnRouteEvent']) ? new EnRouteEventType($params['EnRouteEvent']['LocationName'], $params['EnRouteEvent']['CountryCode'],
            isset($params['EnRouteEvent']['Incident']) ? new IncidentType($params['EnRouteEvent']['Incident']['IncidentIndicator'], $params['EnRouteEvent']['Incident']['Description'],
                isset($params['EnRouteEvent']['Incident']['Endorsement']) ? new EndorsementType($params['EnRouteEvent']['Incident']['Endorsement']['EndorsementDate'], $params['EnRouteEvent']['Incident']['Endorsement']['AuthorityDescription'], $params['EnRouteEvent']['Incident']['Endorsement']['AuthorityLocationDescription'], $params['EnRouteEvent']['Incident']['Endorsement']['AuthorityCountryCode']) : null
            ) : null,
            isset($params['EnRouteEvent']['Control']) ? new ControlType($params['EnRouteEvent']['Control']['NotifiedIndicator']) : null,
            isset($params['EnRouteEvent']['TransShipment']) ? new TransShipmentType(
                isset($params['EnRouteEvent']['TransShipment']['Endorsement']) ? new EndorsementType($params['EnRouteEvent']['TransShipment']['Endorsement']['EndorsementDate'], $params['EnRouteEvent']['TransShipment']['Endorsement']['AuthorityDescription'], $params['EnRouteEvent']['TransShipment']['Endorsement']['AuthorityLocationDescription'], $params['EnRouteEvent']['TransShipment']['Endorsement']['AuthorityCountryCode']) : null,
                isset($params['EnRouteEvent']['TransShipment']['NewTransportMeans']) ? new TransportMeansType($params['EnRouteEvent']['TransShipment']['NewTransportMeans']['TransportModeCode'], $params['EnRouteEvent']['TransShipment']['NewTransportMeans']['TransportMeansNationalityCode'], $params['EnRouteEvent']['TransShipment']['NewTransportMeans']['TransportMeansID'], $params['EnRouteEvent']['TransShipment']['NewTransportMeans']['ConveyanceReferenceID']) : null,
                isset($params['EnRouteEvent']['TransShipment']['NewTransportEquipment']) ? new TransportEquipmentType($params['EnRouteEvent']['TransShipment']['NewTransportEquipment']['TransportEquipmentID']) : null
            ) : null,
            isset($params['EnRouteEvent']['Sealing']) ? new SealingType($params['EnRouteEvent']['Sealing']['SealQuantity'], $params['EnRouteEvent']['Sealing']['SealID']) : null
        ) : null;

        $notification->ActualAuthorisedConsignee = isset($params['ActualAuthorisedConsignee']) ? new PartyType($params['ActualAuthorisedConsignee']['ID'], $params['ActualAuthorisedConsignee']['IDExtension'], $params['ActualAuthorisedConsignee']['Name'],
            isset($params['ActualAuthorisedConsignee']['Address']) ? new AddressType($params['ActualAuthorisedConsignee']['Address']['Line'], $params['ActualAuthorisedConsignee']['Address']['PostcodeID'],
                $params['ActualAuthorisedConsignee']['Address']['CityName'], $params['ActualAuthorisedConsignee']['Address']['CountryCode']) : null) : null;
        $notification->RepresentativePerson = isset($fixedParams['RepresentativePerson']) ? new RepresentativePersonPartyType($fixedParams['RepresentativePerson']) : null;


        $fiTransitArrivalNotification = new FITransitArrivalNotificationType();
        $fiTransitArrivalNotification->MessageInterchange = $messageInterchange;
        $fiTransitArrivalNotification->Notification = $notification;

        $namespaces = array(
            'http://tulli.fi/schema/external/ncts/dme/v1' => 'ncts'
        );

        $php2xml = new \com\mikebevz\xsd2php\Php2Xml('', null, $namespaces);
        $tmpContent = $php2xml->getXml($fiTransitArrivalNotification);
        $tmpContent = str_replace('<FITransitArrivalNotification>', '<ncts:FITransitArrivalNotification schemaVersion="v1_1" xsi:schemaLocation="http://tulli.fi/schema/external/ncts/dme/v1 NctsDme_FITransitArrivalNotification.xsd" xmlns:ncts="http://tulli.fi/schema/external/ncts/dme/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', $tmpContent);
        $tmpContent = str_replace('</FITransitArrivalNotification>', '</ncts:FITransitArrivalNotification>', $tmpContent);

        return $tmpContent;
    }

    private function buildFITransitUnloadReport($fixedParams, $params, $messageInterchange)
    {
        $report = new ReportType();
        $report->MovementReferenceID = $params['MovementReferenceID'];
        $report->FunctionCode = $params['FunctionCode'];
        $report->TransitDepartureOffice = isset($params['TransitDepartureOffice']) ? new CustomsOfficeContactType($params['TransitDepartureOffice']) : isset($fixedParams['TransitDepartureOffice']) ? new CustomsOfficeContactType($fixedParams['TransitDepartureOffice']) : null;
        $report->TransitPresentationOffice = isset($params['TransitPresentationOffice']) ? new CustomsOfficeContactType($params['TransitPresentationOffice']) : null;
        $report->UnloadingRemarks = isset($params['UnloadingRemarks']) ? new UnloadingRemarksType($params['UnloadingRemarks']['RemarksDate'], $params['UnloadingRemarks']['LocationName']) : null;
        $report->DepartureTransportMeans = isset($params['DepartureTransportMeans']) ? new TransportMeansType($params['DepartureTransportMeans']['TransportModeCode'], $params['DepartureTransportMeans']['TransportMeansNationalityCode'],
            $params['DepartureTransportMeans']['TransportMeansID'], $params['DepartureTransportMeans']['ConveyanceReferenceID']) : null;
        $report->ContainerTransportIndicator = isset($params['ContainerTransportIndicator']) ? $params['ContainerTransportIndicator'] : 'false';
        $report->Sealing = isset($params['Sealing']) ? new SealingType($params['Sealing']['SealQuantity'], $params['Sealing']['SealID']) : null;
        $report->GoodsItemQuantity = $params['GoodsItemQuantity'];
        $report->TotalPackageQuantity = $params['TotalPackageQuantity'];
        $report->TotalGrossMassMeasure = isset($params['TotalGrossMassMeasure']) ? new MeasureType($params['TotalGrossMassMeasure']['UnitCode'], $params['TotalGrossMassMeasure']['Value']) : null;

        $report->AuthorisedConsignee = isset($params['AuthorisedConsignee']) ? new PartyType($params['AuthorisedConsignee']['ID'], $params['AuthorisedConsignee']['IDExtension'], $params['AuthorisedConsignee']['Name'],
            isset($params['AuthorisedConsignee']['Address']) ? new AddressType($params['AuthorisedConsignee']['Address']['Line'], $params['AuthorisedConsignee']['Address']['PostcodeID'],
                $params['AuthorisedConsignee']['Address']['CityName'], $params['AuthorisedConsignee']['Address']['CountryCode']) : null) : null;

        $report->ActualAuthorisedConsignee = isset($params['ActualAuthorisedConsignee']) ? new PartyType($params['ActualAuthorisedConsignee']['ID'], $params['ActualAuthorisedConsignee']['IDExtension'], $params['ActualAuthorisedConsignee']['Name'],
            isset($params['ActualAuthorisedConsignee']['Address']) ? new AddressType($params['ActualAuthorisedConsignee']['Address']['Line'], $params['ActualAuthorisedConsignee']['Address']['PostcodeID'],
                $params['ActualAuthorisedConsignee']['Address']['CityName'], $params['ActualAuthorisedConsignee']['Address']['CountryCode']) : null) : null;

        // NOTE: unload report requires less fields than general Principal
        $report->Principal = isset($fixedParams['Principal']) ? new PrincipalPartyType($fixedParams['BusinessId'], null, $fixedParams['Principal']['Name'],
            isset($fixedParams['Principal']['Address']) ? new AddressType($fixedParams['Principal']['Address']['Line'], $fixedParams['Principal']['Address']['PostcodeID'],
                $fixedParams['Principal']['Address']['CityName'], $fixedParams['Principal']['Address']['CountryCode']) : null,
            null, null, null) : null;


        $report->TransitTypeCode = $params['TransitTypeCode'];
        $report->DestinationCountryCode = $params['DestinationCountryCode'];
        $report->DispatchCountryCode = $params['DispatchCountryCode'];

        $report->Consignee = isset($params['Consignee']) ? new PartyType($params['Consignee']['ID'], $params['Consignee']['IDExtension'], $params['Consignee']['Name'],
            isset($params['Consignee']['Address']) ? new AddressType($params['Consignee']['Address']['Line'], $params['Consignee']['Address']['PostcodeID'],
                $params['Consignee']['Address']['CityName'], $params['Consignee']['Address']['CountryCode']) : null) : null;
//        $report->ConsigneeSecurity = isset($params['ConsigneeSecurity']) ? new PartyType($params['ConsigneeSecurity']['ID'], $params['ConsigneeSecurity']['IDExtension'], $params['ConsigneeSecurity']['Name'],
//            isset($params['ConsigneeSecurity']['Address']) ? new AddressType($params['ConsigneeSecurity']['Address']['Line'], $params['ConsigneeSecurity']['Address']['PostcodeID'],
//                $params['ConsigneeSecurity']['Address']['CityName'], $params['ConsigneeSecurity']['Address']['CountryCode']) : null) : null;
        $report->Consignor = isset($params['Consignor']) ? new PartyType($params['Consignor']['ID'], $params['Consignor']['IDExtension'], $params['Consignor']['Name'],
            isset($params['Consignor']['Address']) ? new AddressType($params['Consignor']['Address']['Line'], $params['Consignor']['Address']['PostcodeID'],
                $params['Consignor']['Address']['CityName'], $params['Consignor']['Address']['CountryCode']) : null) : null;
//        $report->ConsignorSecurity = isset($params['ConsignorSecurity']) ? new PartyType($params['ConsignorSecurity']['ID'], $params['ConsignorSecurity']['IDExtension'], $params['ConsignorSecurity']['Name'],
//            isset($params['ConsignorSecurity']['Address']) ? new AddressType($params['ConsignorSecurity']['Address']['Line'], $params['ConsignorSecurity']['Address']['PostcodeID'],
//                $params['ConsignorSecurity']['Address']['CityName'], $params['ConsignorSecurity']['Address']['CountryCode']) : null) : null;



        $fiTransitUnloadReport = new FITransitUnloadReportType();
        $fiTransitUnloadReport->MessageInterchange = $messageInterchange;

        $transitTypeCode = null;
        $dispatchCountryCode = null;
        $destinationCountryCode = null;
        $consignor = null;
        $consignorSecurity = null;
        $consignee = null;
        $consigneeSecurity = null;

        $goodsItem = null;
        if (isset($params['GoodsItem'])) {
            foreach ($params['GoodsItem'] as $key => $value) {

                $previousDocument = null;
                if (isset($value['PreviousDocument'])) {
                    foreach ($value['PreviousDocument'] as $prevDocKey => $prevDocValue) {
                        $previousDocument[] = new PreviousDocumentType($prevDocValue['DocumentTypeCode'], $prevDocValue['DocumentID'], $prevDocValue['ItemSequenceNumber']);
                    }
                }

                $packaging = null;
                if (isset($value['Packaging'])) {
                    foreach ($value['Packaging'] as $packagingKey => $packagingValue) {
                        $packaging[] = new PackagingType($packagingValue['PackagingTypeCode'], $packagingValue['PackagingMarksID'], $packagingValue['PackageQuantity'], $packagingValue['PieceCountQuantity']);
                    }
                }

                $tariffClassification = null;
                if (isset($value['Commodity']) && isset($value['Commodity']['TariffClassification'])) {
                    if (is_array($value['Commodity']['TariffClassification'])) {
                        foreach ($value['Commodity']['TariffClassification'] as $tariffKey => $tariffValue) {
                            $tariffClassification[] = new TariffClassificationType($tariffValue);
                        }
                    } else {
                        $tariffClassification[] = new TariffClassificationType($value['Commodity']['TariffClassification']);
                    }
                }

                $additionalDocument = null;
                if (isset($value['AdditionalDocument'])) {
                    foreach ($value['AdditionalDocument'] as $addDocKey => $addDocValue) {
                        $additionalDocument[] = new DocumentType($addDocValue['DocumentTypeCode'], $addDocValue['DocumentID'], $addDocValue['SupplementaryInformation']);
                    }
                }

                $transportEquipment = null;
                if (isset($value['TransportEquipment'])) {
                    foreach ($value['TransportEquipment'] as $transEquipKey => $transEquipValue) {
                        $transportEquipment[] = new TransportEquipmentType($transEquipValue['TransportEquipmentID']);
                    }
                }


                if (isset($transitTypeCode) && $transitTypeCode != $value['TransitTypeCode']) {
                    $transitTypeCode = '';
                } else {
                    $transitTypeCode = $value['TransitTypeCode'];
                }
                if (isset($destinationCountryCode) && $destinationCountryCode != $value['DestinationCountryCode']) {
                    $destinationCountryCode = '';
                } else {
                    $destinationCountryCode = $value['DestinationCountryCode'];
                }
                if (isset($dispatchCountryCode) && $dispatchCountryCode != $value['DispatchCountryCode']) {
                    $dispatchCountryCode = '';
                } else {
                    $dispatchCountryCode = $value['DispatchCountryCode'];
                }

                $tmpConsignor = null;
                $tmpConsignorSecurity = null;
                $tmpConsignee = null;
                $tmpConsigneeSecurity = null;

                $tmpConsignor = isset($value['Consignor']) ? new PartyType($value['Consignor']['ID'], $value['Consignor']['IDExtension'], $value['Consignor']['Name'],
                    isset($value['Consignor']['Address']) ? new AddressType($value['Consignor']['Address']['Line'], $value['Consignor']['Address']['PostcodeID'],
                        $value['Consignor']['Address']['CityName'], $value['Consignor']['Address']['CountryCode']) : null) : null;

                $tmpConsignorSecurity = isset($value['ConsignorSecurity']) ? new PartyType($value['ConsignorSecurity']['ID'], $value['ConsignorSecurity']['IDExtension'], $value['ConsignorSecurity']['Name'],
                    isset($value['ConsignorSecurity']['Address']) ? new AddressType($value['ConsignorSecurity']['Address']['Line'], $value['ConsignorSecurity']['Address']['PostcodeID'],
                        $value['ConsignorSecurity']['Address']['CityName'], $value['ConsignorSecurity']['Address']['CountryCode']) : null) : null;

                $tmpConsignee = isset($value['Consignee']) ? new PartyType($value['Consignee']['ID'], $value['Consignee']['IDExtension'], $value['Consignee']['Name'],
                    isset($value['Consignee']['Address']) ? new AddressType($value['Consignee']['Address']['Line'], $value['Consignee']['Address']['PostcodeID'],
                        $value['Consignee']['Address']['CityName'], $value['Consignee']['Address']['CountryCode']) : null) : null;

                $tmpConsigneeSecurity = isset($value['ConsigneeSecurity']) ? new PartyType($value['ConsigneeSecurity']['ID'], $value['ConsigneeSecurity']['IDExtension'], $value['ConsigneeSecurity']['Name'],
                    isset($value['ConsigneeSecurity']['Address']) ? new AddressType($value['ConsigneeSecurity']['Address']['Line'], $value['ConsigneeSecurity']['Address']['PostcodeID'],
                        $value['ConsigneeSecurity']['Address']['CityName'], $value['ConsigneeSecurity']['Address']['CountryCode']) : null) : null;

                if ($consignor === true || (isset($consignor) && $consignor != $tmpConsignor)) {
                    $consignor = true;
                } else {
                    $consignor = $tmpConsignor;
                }
                if ($consignorSecurity === true || (isset($consignorSecurity) && $consignorSecurity != $tmpConsignorSecurity)) {
                    $consignorSecurity = true;
                } else {
                    $consignorSecurity = $tmpConsignorSecurity;
                }
                if ($consignee === true || (isset($consignee) && $consignee != $tmpConsignee)) {
                    $consignee = true;
                } else {
                    $consignee = $tmpConsignee;
                }
                if ($consigneeSecurity === true || (isset($consigneeSecurity) && $consigneeSecurity != $tmpConsigneeSecurity)) {
                    $consigneeSecurity = true;
                } else {
                    $consigneeSecurity = $tmpConsigneeSecurity;
                }


                $item = new DeclarationGoodsItemType($key,
                    $value['UniqueConsignmentReferenceID'],
                    $value['TransitTypeCode'],
                    $previousDocument,
                    $additionalDocument,
                    isset($value['AdditionalInformation']) ? new AdditionalInformationType($value['AdditionalInformation']['SpecialMentionCode'], $value['AdditionalInformation']['StatementDescription'],
                        $value['AdditionalInformation']['EUExportIndicator'], $value['AdditionalInformation']['ExportingCountryCode']) : null,
                    $value['DispatchCountryCode'],
                    $value['DestinationCountryCode'],
                    $transportEquipment,
                    //isset($value['TransportEquipment']) ? new TransportEquipmentType($value['TransportEquipment']['TransportEquipmentID']) : null,
                    $packaging,
                    isset($value['Commodity']) ? new CommodityType($tariffClassification, $value['Commodity']['DangerousGoodsCode'], $value['Commodity']['GoodsDescription']) : null,
                    isset($value['SensitiveGoods']) ? new SensitiveGoodsType($value['SensitiveGoods']['SensitiveGoodsCode'], $value['SensitiveGoods']['SensitiveGoodsMeasure']) : null,
                    isset($value['GrossMassMeasure']) ? new MeasureType($value['GrossMassMeasure']['UnitCode'], $value['GrossMassMeasure']['Value']) : null,
                    isset($value['NetWeightMeasure']) ? new MeasureType($value['NetWeightMeasure']['UnitCode'], $value['NetWeightMeasure']['Value']) : null,

                    $tmpConsignor, $tmpConsignorSecurity, $tmpConsignee, $tmpConsigneeSecurity,

                    $value['FreightPaymentMethodCode']
                );

                $goodsItem[] = $item;
            }
        }

        if (isset($transitTypeCode)) {
            if ($transitTypeCode == '') {
                $report->TransitTypeCode = 'T-';
            } else {
                $report->TransitTypeCode = $transitTypeCode;
                foreach ($goodsItem as $item) {
                    $item->TransitTypeCode = null;
                }
            }
        }

        if (isset($destinationCountryCode)) {
            if ($destinationCountryCode == '') {
                $report->DestinationCountryCode = null;
            } else {
                $report->DestinationCountryCode = $destinationCountryCode;
                foreach ($goodsItem as $item) {
                    $item->DestinationCountryCode = null;
                }
            }
        }
        if (isset($dispatchCountryCode)) {
            if ($dispatchCountryCode == '') {
                $report->DispatchCountryCode = null;
            } else {
                $report->DispatchCountryCode = $dispatchCountryCode;
                foreach ($goodsItem as $item) {
                    $item->DispatchCountryCode = null;
                }
            }
        }

        if (isset($consignee)) {
            if ($consignee === true) {
                $report->Consignee = null;
            } else {
                $report->Consignee = $consignee;
                foreach ($goodsItem as $item) {
                    $item->Consignee = null;
                }
            }
        }
//        if (isset($consigneeSecurity)) {
//            if ($consigneeSecurity === true) {
//                $report->ConsigneeSecurity = null;
//            } else {
//                $report->ConsigneeSecurity = $consigneeSecurity;
//                foreach ($goodsItem as $item) {
//                    $item->ConsigneeSecurity = null;
//                }
//            }
//        }
        if (isset($consignor)) {
            if ($consignor === true) {
                $report->Consignor = null;
            } else {
                $report->Consignor = $consignor;
                foreach ($goodsItem as $item) {
                    $item->Consignor = null;
                }
            }
        }
//        if (isset($consignorSecurity)) {
//            if ($consignorSecurity === true) {
//                $report->ConsignorSecurity = null;
//            } else {
//                $report->ConsignorSecurity = $consignorSecurity;
//                foreach ($goodsItem as $item) {
//                    $item->ConsignorSecurity = null;
//                }
//            }
//        }

        $fiTransitUnloadReport->GoodsItem = $goodsItem;
        $fiTransitUnloadReport->Report = $report;


        $namespaces = array(
            'http://tulli.fi/schema/external/ncts/dme/v1' => 'ncts'
        );

        $php2xml = new \com\mikebevz\xsd2php\Php2Xml('', null, $namespaces);
        $tmpContent = $php2xml->getXml($fiTransitUnloadReport);
        $tmpContent = str_replace('<FITransitUnloadReport>', '<ncts:FITransitUnloadReport schemaVersion="v1_1" xsi:schemaLocation="http://tulli.fi/schema/external/ncts/dme/v1 NctsDme_FITransitUnloadReport.xsd" xmlns:ncts="http://tulli.fi/schema/external/ncts/dme/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', $tmpContent);
        $tmpContent = str_replace('</FITransitUnloadReport>', '</ncts:FITransitUnloadReport>', $tmpContent);

        return $tmpContent;
    }

} 