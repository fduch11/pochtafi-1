<?php

set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme');
set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/ncts');
//set_include_path(get_include_path().PATH_SEPARATOR.'generated/tulli/dme/fi/tu');
spl_autoload_extensions('.php');
spl_autoload_register();

require 'vendor/autoload.php';

try {

    $local_cert = 'C:\Sandbox\T1Declaration\certificates\pem_pochta.cer';

    $soapUrl = 'https://ws-customertest.tulli.fi/services/DirectMessageExchange';
//    $soapUrl = 'http://localhost:33816/CorporateMessageServiceHttpBinding.asmx';

    $soapParams = array(
//        'proxy_host' => 'localhost',
//        'proxy_port' => 8888,

        'trace' => 1,
        'exceptions' => true,
        'local_cert' => $local_cert,
        'encoding' => 'UTF-8'
    );

    $outputDir = 'C:\\Sandbox\\T1Declaration\\results\\any\\';

    include('TestCases/fixedCase.php');

    $storageReferences = array(
        'NCTS20141117125622A0419116',
        'NCTS20141110152632E184412'
//        'NCTS20141117125722C8CED14',
//        'NCTS20141117125622A041914',
//        'NCTS2014111706475213B00116',
//        'NCTS201411170643236F4E8116',
//        'NCTS2014111706355943318131',
//        'NCTS2014111706355943318129',
//        'NCTS2014111706355943318127',
//        'NCTS2014111706355943318125',
//        'NCTS2014111706355943318123',
//        'NCTS2014111706355943318121',
    );

    $requestHeader = new RequestHeader();
    $requestHeader->IntermediaryBusinessId = $fixedParams['BusinessId'];
    $requestHeader->IntermediarySoftwareInfo = $fixedParams['SoftwareInfo'];
    $requestHeader->Timestamp = time();
    $requestHeader->Language = "EN";

    $soapClient = new SoapClient('tulli\dme\CustomsCorporateService_soap1_2.wsdl', $soapParams);
    $soapClient->__setLocation($soapUrl);


    foreach ($storageReferences as $storageReference) {

        /* ---------  Download  -------------- */

        $criteria = new DownloadMessageFilteringCriteria($storageReference);

        $downloadRequest = new DownloadRequest();
        $requestHeader->Timestamp = time();
        $downloadRequest->RequestHeader = $requestHeader;
        $downloadRequest->DownloadMessageFilteringCriteria = $criteria;

        $result = $soapClient->__soapCall('Download', array($downloadRequest));

        /** @var $result DownloadResponse */
        $responseHeader = $result->ResponseHeader;
        if ($responseHeader->ResponseCode == '000' && $responseHeader->ResponseText == 'OK') {

            $xmlObject   = new SimpleXMLElement($result->ApplicationResponseMessage);
            $content = $xmlObject->children("v1", true)->ApplicationResponseContent;
            $text = base64_decode($content->children("v11", true)->Content);

            $doc = new DOMDocument();
            $doc->loadXML($text);
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            $doc->save($outputDir . $storageReference . '.response.xml');

            $content = $xmlObject->children("v1", true)->AttachmentOfApplicationResponseContent;
            if (!empty($content)) {
                $text = base64_decode($content->children("v11", true)->Content);
                file_put_contents($outputDir . $storageReference . '.zip', $text);
            }
        }

        sleep(2);
    }


}
catch (SoapFault $soapFault) {
    echo $soapFault;
    var_dump($soapFault);
}
catch (Exception $ex) {
    echo $ex;
    var_dump($ex);
}

?>