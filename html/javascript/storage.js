$(function() {

  // Init Select 2
  function toSelect2Item(term) { return {id: term, text: term} }
  $('input.js-select2').each(function(){
    $(this).select2({
      minimumInputLength: 1,
      data: $(this).data('options').split(',').map(toSelect2Item),
      createSearchChoice: toSelect2Item
    });
  });
  $('select.js-select2').each(function(){
    $(this).select2({minimumInputLength: 1});
  });

  // "Toggle all" checkboxes
  $('[data-toggle-all]').change(function(){
    var newState = $(this).prop('checked');
    $(  $(this).data('toggle-all')  ).each(function(){
      if ($(this).prop('checked') !== newState) {
        $(this).prop('checked', newState).change();
      }
    });
  });

  // Initialise bootstrap popovers
  $('[data-toggle=popover]').popover();

  // Table rows as links
  $(document).on('click', '[data-tr-link]', function() {
    location.href = $(this).data('tr-link');
  });

});
