ns('dashboard-accordion', function(exports) {

   // var allPanels = $('#dashboard-accordion .dashboard-row__content').hide();
  $('#dashboard-accordion .dashboard-row h2').click(function() {
    var dashRowContent = $(this).next(".dashboard-row__content");
    dashRowContent.slideToggle().parents(".dashboard-row").toggleClass("collapsed");
    return false;
  });

  $(".storage-select-block input[checked]").parents(".storage-select-block").find(".storage-select-block__container").addClass("active");
  $(".storage-select-block").click(function(){
    console.log($(this).next(".storage-select-block__container"));
    $(".storage-select-block__container").removeClass("active");
    $(this).find(".storage-select-block__container").addClass("active")
  });




});

