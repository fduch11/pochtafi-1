ns('behind-overlay', function(exports) {

  exports.init = function() {

    $('.js-expandable').each(function() {
      var $wrap = $(this);
      var $overlay = $('.js-expandable__overlay', this);
      var $hidden = $('.js-expandable__hidden', this);

      $wrap.height($wrap.height());
      if ($hidden.height() == 0) {
        $overlay.hide();
      }

      $overlay.click(function() {
        $overlay.css({opacity: 0, visibility: 'hidden'});
        $hidden.show();
        $wrap.height($wrap.height() + $hidden.height());
      });
    });


    $('.js-height-limit').each(function() {
      var $wrap = $(this);
      var $overlay = $('.js-height-limit__overlay', this);
      var $content = $('.js-height-limit__content', this);

      $wrap.height($wrap.height());
      $wrap.css('max-height', 'none');
      if ($wrap.height() == $content.height()) {
        $overlay.hide();
      }

      $overlay.click(function() {
        $overlay.css({opacity: 0, visibility: 'hidden'});
        $wrap.height($content.height());
      });
    });

  };

});
