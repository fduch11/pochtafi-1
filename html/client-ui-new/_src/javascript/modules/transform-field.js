ns('transform-field', function(exports) {

  exports.init = function() {

    var initd = 'transform-field-initialised';

    $('.js-transform-field').each(function() {

      var $wrap = $(this);

      if (!$wrap.data(initd) && $wrap.parents('.js-template').length === 0) {
        $wrap.data(initd, true);

        var $text = $('.js-transform-field__text', this);
        var $textWrap = $('.js-transform-field__text__wrap', this);
        var $input = $('.js-transform-field__input', this);

        $text.text($input.val());

        function editOn() {
          $wrap.addClass('edit-mode');
        }
        function editOnFocus() {
          editOn();
          var strLength = $input.val().length * 2;
          $input.focus();
          $input[0].setSelectionRange(strLength, strLength);
        }
        function editOff() {
          if ($input.val() !== '') {
            $text.text($input.val());
            $wrap.removeClass('edit-mode');
          }
        }

        if ($input.val() === '') {
          editOn();
        }

        $textWrap.click(editOnFocus);
        $input
          .on('blur', editOff)
          .on('keyup', function(e) {
            if (e.keyCode === 13) { // Enter
              editOff();
            }
          });
      }

    });

  };

});
