ns('primary-init', function() {

  $(function() {

    ns('number-field').init();
    ns('transform-field').init();
    ns('popover').init();
    ns('behind-overlay').init();
    ns('clipboard').init();
    ns('placeholder').init();
    ns('phone-field').init();

  });

});
