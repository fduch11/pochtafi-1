ns('proxy-person-modal', function(exports) {



  // Pure helpers

  function apply(x, f) {
    return f(x);
  }

  function isNotEmpty(xs) {
    return xs.length !== 0;
  }

  function removeByIndex(xs, index) {
    xs = xs.slice(0);
    xs.splice(index, 1);
    return xs;
  }




  // Kefir utils

  function takeUntil(source, control) {
    return Kefir.fromBinder(function(emitter) {
      control.onValue(emitter.end);
      source.onValue(emitter.emit);
      return function() {
        control.offValue(emitter.end);
        source.offValue(emitter.emit);
      }
    });
  }







  exports.show = function(initialItems) {
    var $modal = $('.js-ppf');
    if ($modal.length === 1) {


      // Constants

      var $nameField = $modal.find('.js-ppf__name-field');
      var $passportField = $modal.find('.js-ppf__passport-field');
      var $saveBtn = $modal.find('.js-ppf__save');

      var $menuWrap = $modal.find('.js-ppf__menu-wrap');
      var $menu = $modal.find('.js-ppf__menu');
      var $menuItemTemplate = $modal.find('.js-ppf__item-template').children().first();

      var $formTitle = $modal.find('.js-ppf__form-title');

      var removeConfirmMessage = $menu.data('remove-confirm');




      // Mutable

      var newItem = {
        name: '',
        passport: ''
      }



      // Observables

      var hideEv = $modal.asKefirStream('hide.bs.modal').take(1).delay(0);
      var saveIntent = $saveBtn.asKefirStream('click');
      var saveAction = takeUntil(saveIntent, hideEv);

      var modifyAtempts = Kefir.bus();
      var items = takeUntil(modifyAtempts, hideEv).scan(initialItems, apply);

      var showMenu = items.map(isNotEmpty);

      var startEditAtempts = $menu.asKefirStream('click', '.js-ppf__item__edit', function() {
        return $(this).parents('.js-ppf__item').index();
      })
      startEditAtempts = takeUntil(startEditAtempts, hideEv);

      var removeAtempts = $menu.asKefirStream('click', '.js-ppf__item__remove', function() {
        return $(this).parents('.js-ppf__item').index();
      })
      removeAtempts = takeUntil(removeAtempts, hideEv);

      var removes = removeAtempts.filter(function(index) {
        return confirm(removeConfirmMessage);
      });

      modifyAtempts.plug(removes.map(function(index) {
        return function(items) {
          return removeByIndex(items, index);
        }
      }));

      var currentItemIndex = Kefir.merge([
        startEditAtempts.map(function(index) {
          return function(cur) {return index}
        }),
        removes.map(function(index) {
          return function(cur) {
            if (cur === -1) {
              return cur;
            } else {
              if (cur === index) {
                return -1;
              } else if (cur > index) {
                return cur - 1;
              } else {
                return cur;
              }
            }
          }
        })
      ]).scan(-1, apply).skipDuplicates();

      var changeNameAtempt = $nameField.asKefirStream('change keyup', function() {
        return $(this).val();
      }).skipDuplicates();
      changeNameAtempt = takeUntil(changeNameAtempt, hideEv);

      var changePassportAtempt = $passportField.asKefirStream('change keyup', function() {
        return $(this).val();
      }).skipDuplicates();
      changePassportAtempt = takeUntil(changePassportAtempt, hideEv);

      modifyAtempts.plug(
        currentItemIndex.sampledBy(changeNameAtempt, function(index, newName) {
          if (index === -1) {
            return null
          } else {
            return function(items) {
              // fix me: why mutable
              items[index].name = newName;
              return items;
            }
          }
        }).filter(function(x){return x !== null})
      )

      modifyAtempts.plug(
        currentItemIndex.sampledBy(changePassportAtempt, function(index, newPassport) {
          if (index === -1) {
            return null
          } else {
            return function(items) {
              // fix me: why mutable
              items[index].passport = newPassport;
              return items;
            }
          }
        }).filter(function(x){return x !== null})
      )




      // Side effects helpers

      function renderItem(data) {
        return $menuItemTemplate
          .clone()
          .find('.js-ppf__item__name')
            .text(data.name)
            .end();
      }




      // Side effects

      showMenu.onValue(['toggle', $menuWrap]);

      items.onValue(function(items) {
        $menu.empty();
        _.each(items, function(item) {
          $menu.append(renderItem(item));
        });
      });

      items.sampledBy(currentItemIndex, function(items, index) {
        if (index === -1) {
          return newItem;
        } else {
          return items[index];
        }
      }).onValue(function(item) {
        $nameField.val(item.name);
        $passportField.val(item.passport);
      });

      currentItemIndex.sampledBy(changeNameAtempt).onValue(function(args) {
        var index = args[0];
        var newName = args[1];
        if (index === -1) {
          newItem.name = newName;
        }
      });

      currentItemIndex.sampledBy(changePassportAtempt).onValue(function(args) {
        var index = args[0];
        var newPassport = args[1];
        if (index === -1) {
          newItem.passport = newPassport;
        }
      });

      currentItemIndex.map(function(index) {
        if (index === -1) {
          return $formTitle.data('on-new');
        } else {
          return $formTitle.data('on-edit')
        }
      }).onValue(['text', $formTitle]);

      saveIntent.onValue(function() {  $modal.modal('hide')  });





      // Result

      $modal.modal('show');

      return  Kefir.sampledBy([items, currentItemIndex], [saveAction], function(items, currentItemIndex, __) {
        if (currentItemIndex === -1) {
          items = items.slice(0);
          items.push(newItem);
        }
        return _.filter(items, function(item) {
          return $.trim(item.name) !== '' || $.trim(item.passport) !== '';
        })
      });





    } else {
      throw new Error('.js-ppf element not found');
    }
  }

});
