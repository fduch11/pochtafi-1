ns('tabs', function() {

  $('.js-tabs').each(function() {

    var $btns, $contents, $wrap, curTab, isAnyOpen;


    // Pure helpers

    function notNull(x) {
      return x !== null && x !== void 0;
    }


    // Constants

    $wrap = $(this);
    $btns = $('[data-tab-btn]', this);
    $contents = $('[data-tab]', this);


    if ($btns.length > 0) {


      // Observables

      curTab = $btns
        .asKefirStream('click', function() {
          return $(this).data('tab-btn');
        })
        .toProperty( $btns.filter('.active').data('tab-btn') );

      isAnyOpen = curTab.map(function(x) {
        return notNull(x) ? true : false;
      });

      curTab = curTab.filter(notNull);



      // Side effects

      curTab.onValue(function(n) {
        $btns.removeClass('active').filter("[data-tab-btn='" + n + "']").addClass('active');
        $contents.removeClass('active').filter("[data-tab='" + n + "']").addClass('active');
        // Landing swap bg-image
        if ($wrap.hasClass("world-prices")){
          $wrap.removeClass(function (index, css) {
            return (css.match (/\bworld-prices-\S+/g) || []).join(' ');
          }).addClass("world-prices-" + n);
        }
      });

      isAnyOpen.onValue(['toggleClass', $wrap, 'is-tab-open']);


    }






  });

});


