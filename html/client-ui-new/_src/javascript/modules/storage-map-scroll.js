ns('storage-google-map', function(exports) {

 $(function(){
    $('.storage-google-map').hover(function(){
        stopScroll();},
                     function () {
    $('.storage-google-map iframe').css("pointer-events", "none");
    });
});

function stopScroll(){
$('.storage-google-map').click(function () {
    $('.storage-google-map iframe').css("pointer-events", "auto");
});
}
});