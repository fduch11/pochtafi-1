ns('parcel-info-proxy', function() {

  function wrapToDiv(text) {
    return $('<div></div>').text(text);
  }


  var modal = ns('proxy-person-modal');



  $('.js-parcel-info').each(function() {


    // Constants

    var $wrap = $(this);
    var $view = $('.js-parcel-info__proxy', this);
    var $setBtn = $('.js-parcel-info__set-proxy', this);
    var $setBtnText = $('.js-parcel-info__set-proxy__text', this);

    var initialData = PARCEL_INFO[$wrap.data('parcel-id')].proxyPersons || [];


    // Observables

    var setProxyIntent = $setBtn.asKefirStream('click');

    var _curProxyData = Kefir.pool();

    var curProxyData = _curProxyData
      .sampledBy(setProxyIntent, function(data, __) {return data})
      .flatMapFirst(modal.show)
      .toProperty(initialData);

    _curProxyData.plug(curProxyData);

    var hasProxy = curProxyData.map(function(data) {
      return data.length > 0;
    }).skipDuplicates();



    // Side effects

    curProxyData.onValue(function(data) {
      $view.empty();
      if (data.length === 0) {
        $view.append(wrapToDiv($view.data('on-empty')));
      } else {
        _.each(data, function(item) {
          $view.append(wrapToDiv(item.name));
        });
      }
    });

    hasProxy.map(function(has) {
      return $setBtnText.data(has ? 'on-full' : 'on-empty');
    }).onValue(['text', $setBtnText]);



  });

});
