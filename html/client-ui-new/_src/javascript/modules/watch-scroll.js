ns('watch-scroll', function() {



  var $window = $(window);
  var $document = $(document);
  var docWidth = $window.asKefirProperty('resize', function() {
    return $document.width();
  });

  var scrollLeft = (function() {
    var scrollLeft = $window.asKefirProperty('scroll', function() {
      return $window.scrollLeft();
    });
    var winWidth = $window.asKefirProperty('resize', function() {
      return $window.width();
    });
    return Kefir.combine([scrollLeft, winWidth, docWidth], function(scrollLeft, winWidth, docWidth) {
      return Math.min(docWidth - winWidth, Math.max(0, scrollLeft));
    }).skipDuplicates();
  }());




  $('.js-parcels-wathc-scroll').each(function() {
    var $el = $(this);
    var $sticky = $('.js-parcels-wathc-scroll__sticky', this);

    // 143 — the header height
    var watcher = scrollMonitor.create(this, {bottom: -143});


    function update() {

      // "true if any part of the element is above the viewport"
      $el.toggleClass('is-above-vp', watcher.isAboveViewport);

      // "true if any part of the element is visible, false if not"
      $el.toggleClass('is-in-vp', watcher.isInViewport);
      $el.toggleClass('isnt-in-vp', !watcher.isInViewport);

      classes.emit(watcher.isAboveViewport.toString() + watcher.isInViewport.toString());

    }



    // Решаем проблему с fixed элементом и боковым скроллом

    var classes = Kefir.emitter();

    var isStickyFixed = classes.skipDuplicates().map(function() {
      return $sticky.css('position') === 'fixed';
    });

    var stickyLeft = Kefir.combine([isStickyFixed, scrollLeft], function(isFixed, scroll) {
      return isFixed ? scroll : 0;
    }).skipDuplicates();

    var stickyWidth = Kefir.combine([isStickyFixed, docWidth], function(isFixed, docWidth) {
      return isFixed ? (docWidth + 'px') : 'auto';
    }).skipDuplicates();

    stickyLeft.onValue(function(left) {
      $sticky.css('left', (-left) + 'px');
    });

    stickyWidth.onValue(function(width) {
      $sticky.css('width', width);
    });



    update();
    watcher.on('stateChange', update);
  });

});
