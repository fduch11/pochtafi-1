ns('declaration', function(exports) {


  // Pure helpers

  function id(x) {return x}
  function notId(x) {return !x}
  function isNaN(x) {return x !== x}
  function fixed2(_num) {
    var num = parseFloat(_num);
    if (isNaN(num)) {
      return _num;
    } else {
      return parseFloat(num.toFixed(2));
    }
  }

  function isEmpty(item) {
    return item.name === "" && item.amount === "" && item.price === "";
  }

  function isComplete(item) {
    return item.name !== "" && item.amount !== "" && item.price !== "";
  }

  function isIncomplete(item) {
    return !isEmpty(item) && !isComplete(item);
  }

  function hasPriceInfo(item) {
    return item.amount !== "" && item.price !== "";
  }

  function normItem(item) {
    return {
      id: item.id,
      selection: item.selection,
      name: item.name,
      amount: parseInt(item.amount, 10),
      price: parseFloat(item.price)
    }
  }

  function extractData(el) {
    return {
      id: $('.js-declaration__form__item__id', el).val(),
      selection: $('.js-declaration__form__item__selection', el).val(),
      name: $('.js-declaration__form__item__name', el).val(),
      price: $('.js-declaration__form__item__price', el).val(),
      amount: $('.js-declaration__form__item__amount', el).val()
    }
  }




  $('.js-declaration').each(function() {


    // Constants

    var $wrap = $(this);
    var $toForm = $('.js-declaration__to-form', this);
    var $toInfo = $('.js-declaration__to-info', this);
    var $toInfoDis = $('.js-declaration__to-info-dis', this);

    var exposedAPI = {};
    $wrap.data('declaration-api', exposedAPI);

    var DEF_CUR = DECLARATION_INITIAL_DATA.defaultCurrency;
    var NOT_DEF_CUR = DECLARATION_INITIAL_DATA.notDefaultCurrency;

    var CURRENCY_SIGN = NOT_DEF_CUR ? NOT_DEF_CUR.sign : DEF_CUR.sign;
    var EXPECTED_TOTAL = NOT_DEF_CUR ? NOT_DEF_CUR.total : DEF_CUR.total;




    // Observables

    var itemsUpdaters = Kefir.pool();

    var declarationItems = itemsUpdaters
      .scan(DECLARATION_INITIAL_DATA.items, function(items, updater) {
        return updater(items);
      });

    var totalAmount = declarationItems.map(function(items) {
      return _.reduce(_.pluck(items, 'amount'), function(a, b) {return a + b});
    });

    var totalPrice = declarationItems.map(function(items) {
      return fixed2(_.reduce(items, function(sum, item) {return sum + item.price * item.amount}, 0));
    });

    exposedAPI.setEditMode = Kefir.emitter();

    var setEditModeAttempts = Kefir.merge(
      $toForm.asKefirStream('click').mapTo(true),
      $toInfo.asKefirStream('click').mapTo(false),
      exposedAPI.setEditMode
    );




    // Static view

    $('.js-declaration__info', this).each(function() {

      // Constants

      var $totalAmount = $('.js-declaration__info__total-amount', this);
      var $totalPrice = $('.js-declaration__info__total-price', this);
      var $currencySign = $('.js-declaration__info__currency-sign', this);
      var $itemTemplate = $('.js-declaration__info__item-template', this);
      var $itemsWrap = $('.js-declaration__info__items-wrap', this);

      $totalPrice.text(fixed2(EXPECTED_TOTAL));
      $currencySign.text(CURRENCY_SIGN);


      // Side effect helpers

      function renderItem(data) {
        return $itemTemplate
          .clone().removeClass('js-template')
          .find('.js-declaration__info__item__id')
            .text(data.id)
            .end()
          .find('.js-declaration__info__item__selection')
            .text(data.selection)
            .end()
          .find('.js-declaration__info__item__name')
            .text(data.name)
            .end()
          .find('.js-declaration__info__item__amount')
            .text(data.amount)
            .end()
          .find('.js-declaration__info__item__price')
            .text(fixed2(data.price))
            .end()
          .find('.js-declaration__info__item__total-price')
            .text(fixed2(data.price * data.amount))
            .end()
          .find('.js-declaration__info__item__currency')
            .text(CURRENCY_SIGN)
            .end();
      }


      // Side effects

      totalAmount.onValue(['text', $totalAmount]);
      totalPrice.onValue(['text', $totalPrice]);

      declarationItems.onValue(function(items) {
        $itemsWrap.empty();
        var items$ = _.map(items, renderItem);
        _.forEach(items$, function($item) {  $itemsWrap.append($item)  });
      });

    });






    // Form

    $('.js-declaration__form', this).each(function() {


      // Constants

      var $totalAmount = $('.js-declaration__form__total-amount', this);
      var $totalPrice = $('.js-declaration__form__total-price', this);
      var $currencySign = $('.js-declaration__form__currency-sign', this);
      var $defCurWrap = $('.js-declaration__form__def-cur-wrap', this);
      var $totalPriceDefCur = $('.js-declaration__form__total-price--def-cur', this);
      var $currencySignDefCur = $('.js-declaration__form__currency-sign--def-cur', this);
      var $itemTemplate = $('.js-declaration__form__item-template', this);
      var $itemsWrap = $('.js-declaration__form__items-wrap', this);

      $totalPrice.text(fixed2(EXPECTED_TOTAL));
      $currencySign.text(CURRENCY_SIGN);
      if (NOT_DEF_CUR) {
        $defCurWrap.show();
        $totalPriceDefCur.text(fixed2(DEF_CUR.total));
        $currencySignDefCur.text(DEF_CUR.sign);
      }


      // Observables

      var recalcTriggers = Kefir.pool();

      recalcTriggers.plug(
        $itemsWrap.asKefirStream('input keyup change', 'input')
      );

      var rows$ = recalcTriggers.map(function() {
        return $itemsWrap.children().get();
      });

      var tmpItems = rows$.map(function(_rows$) {
        return _.map(_rows$, extractData);
      });

      var hasEmptyRow = tmpItems.map(function(items) {
        if (items.lenght === 0) {
          return false;
        } else {
          return isEmpty(items[items.length - 1]);
        }
      });

      var hasIncompleteRows = tmpItems.map(function(items) {
        return _.filter(items, isIncomplete).length > 0;
      }).toProperty(false);

      var tmpItemsWithPrice = tmpItems.map(function(items) {
        return _.map(_.filter(items, hasPriceInfo), normItem);
      });

      itemsUpdaters.plug(
        tmpItemsWithPrice.map(function(items) {
          return function() {return items};
        })
      );

      var totalGreater = totalPrice.map(function(sum) {return sum > EXPECTED_TOTAL});
      var totalLess = totalPrice.map(function(sum) {return sum < EXPECTED_TOTAL});

      var showLessErr = Kefir.merge(
        setEditModeAttempts.filter(notId).filterBy(totalLess).mapTo(true),
        totalLess.filter(notId).mapTo(false)
      ).toProperty(false);

      var disableDoneBtn = Kefir.or([hasIncompleteRows, totalGreater, showLessErr]).skipDuplicates();

      var isInEditMode = Kefir.merge(
        setEditModeAttempts.filter(id),
        setEditModeAttempts.filter(notId)//.filterBy(disableDoneBtn.not())
      ).toProperty(false);



      // Side effect helpers

      function renderItem(data) {
        return $itemTemplate
          .clone().removeClass('js-template')
          .find('.js-declaration__form__item__id')
            .val(data.id)
            .end()
          .find('.js-declaration__form__item__selection')
            .val(data.selection)
            .end()
          .find('.js-declaration__form__item__name')
            .val(data.name)
            .end()
          .find('.js-declaration__form__item__amount')
            .val(data.amount)
            .end()
          .find('.js-declaration__form__item__price')
            .val(fixed2(data.price))
            .end()
          .find('.js-declaration__form__item__currency')
            .text(CURRENCY_SIGN)
            .end();
      }

      function appendEmptyRow() {
        $itemsWrap.append(updateClassesAndTotal(renderItem({})));
        ns('number-field').init();
        ns('transform-field').init();
      }

      function updateClassesAndTotal(el) {
        var item = extractData(el);
        $(el).removeClass('is-complete is-incomplete is-empty');
        if (isComplete(item)) {  $(el).addClass('is-complete')  }
        if (isIncomplete(item)) {  $(el).addClass('is-incomplete')  }
        if (isEmpty(item)) {  $(el).addClass('is-empty')  }
        if (hasPriceInfo(item)) {
          var _item = normItem(item);
          $('.js-declaration__form__item__total', el).text(fixed2(_item.amount * _item.price));
        } else {
          $('.js-declaration__form__item__total', el).text('0');
        }
        return el;
      }

      function removeRow(event) {
        var $btn = $(event.currentTarget);
        var $row = $btn.parents('.js-declaration__form__item-template');
        if (confirm($btn.data('confirm'))) {
          $row.remove();
        }
      }


      // Side effects

      //disableDoneBtn.not().onValue(['toggle', $toInfo]);
      //disableDoneBtn.onValue(['toggle', $toInfoDis]);

      recalcTriggers.plug(
        hasEmptyRow.filter(notId).tap(appendEmptyRow)
      );

      recalcTriggers.plug(
        $itemsWrap.asKefirStream('click', '.js-declaration__form__item__remove').tap(removeRow)
      );

      rows$.onValue(function(rows) {
        _.forEach(rows, updateClassesAndTotal);
      });

      //totalGreater.onValue(['toggle', $('.js-declaration__form__mismathc-more', this)]);
      //showLessErr.onValue(['toggle', $('.js-declaration__form__mismathc-less', this)]);

      //Kefir.or([totalGreater, showLessErr]).onValue(['toggleClass', $(this), 'has-mismathc-total-err']);

      isInEditMode.onValue(['toggleClass', $wrap, 'declaration--edit-mode']);

      declarationItems
        .sampledBy(isInEditMode.filter(id), id)
        .onValue(function(items) {
          $itemsWrap.empty();
          var items$ = _.map(items, renderItem);
          _.forEach(items$, updateClassesAndTotal);
          _.forEach(items$, function($item) {  $itemsWrap.append($item)  });
          ns('number-field').init();
          ns('transform-field').init();
          appendEmptyRow();
        });

      totalAmount.onValue(['text', $totalAmount]);
      totalPrice.onValue(['text', $totalPrice]);

    });



  });

});
