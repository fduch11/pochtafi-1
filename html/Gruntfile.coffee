module.exports = (grunt) ->

  grunt.initConfig(

    stylus:
      options:
        compress: false
      main:
        expand: true
        cwd: "stylesheets/_src"
        src: "*.styl"
        dest: "stylesheets"
        ext: ".css"
      client_ui_new:
        expand: true
        cwd: "client-ui-new/_src/css"
        src: "*.styl"
        dest: "client-ui-new/css"
        ext: ".css"
      kiosk_new:
        expand: true
        cwd: "kiosk-new/_src/css"
        src: "*.styl"
        dest: "kiosk-new/css"
        ext: ".css"

    autoprefixer:
      main:
        expand: true
        cwd: "stylesheets"
        src: "*.css"
        dest: "stylesheets"
        ext: ".css"
      client_ui_new:
        expand: true
        cwd: "client-ui-new/css"
        src: "*.css"
        dest: "client-ui-new/css"
        ext: ".css"
      kiosk_new:
        expand: true
        cwd: "kiosk-new/css"
        src: "*.css"
        dest: "kiosk-new/css"
        ext: ".css"

    concat:
      client_ui_new_js:
        options:
          # Simple js modules system https://gist.github.com/pozadi/8973106
          # (helps don't care about files order)
          banner: """
            window.ns=function(){function b(b,c){return c?a[b]=function(){
            var d={exports:{}};return c(d.exports,d),a[b]=function(){
            return d.exports},d.exports}:a[b]()}var a={};return b.initAll=
            function(){for(var b in a)a.hasOwnProperty(b)&&a[b]()},b}();
          """
          footer: ";ns.initAll();"
        files:
          'client-ui-new/javascript/bundle.js': [
            'client-ui-new/_src/javascript/vendor/jquery-1.11.1.min.js'
            'client-ui-new/_src/javascript/vendor/kefir.min.js'
            'client-ui-new/_src/javascript/vendor/*.js'
            'client-ui-new/_src/javascript/modules/*.js'
          ]
      kiosk_new_js:
        options:
          # Simple js modules system https://gist.github.com/pozadi/8973106
          # (helps don't care about files order)
          banner: """
            window.ns=function(){function b(b,c){return c?a[b]=function(){
            var d={exports:{}};return c(d.exports,d),a[b]=function(){
            return d.exports},d.exports}:a[b]()}var a={};return b.initAll=
            function(){for(var b in a)a.hasOwnProperty(b)&&a[b]()},b}();
          """
          footer: ";ns.initAll();"
        files:
          'kiosk-new/javascript/bundle.js': [
            'kiosk-new/_src/javascript/vendor/jquery-1.11.1.min.js'
            'kiosk-new/_src/javascript/vendor/kefir.min.js'
            'kiosk-new/_src/javascript/vendor/*.js'
            'kiosk-new/_src/javascript/modules/*.js'
          ]

    ejs:
      storage:
        options:
          trans: (str) -> str
          staticUrl: (part) -> "../#{part}"
          pageUrl: (name) -> "#{name}.html"
        cwd: "storage/_src/"
        src: "*.ejs"
        dest: "storage"
        expand: true
        ext: ".html"
      storage_php:
        options:
          trans: (str) -> "<?php echo $this->__('#{str}') ?>"
          staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
          pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
        cwd: "storage/_src/"
        src: "*.ejs"
        dest: "storage-php"
        expand: true
        ext: ".html"
      kiosk:
        cwd: "kiosk/_src/"
        src: "*.ejs"
        dest: "kiosk"
        expand: true
        ext: ".html"
      client_ui:
        cwd: "client-ui/_src/"
        src: "*.ejs"
        dest: "client-ui"
        expand: true
        ext: ".html"
      client_ui_new:
        options:
          trans: (str) -> str
          staticUrl: (part) -> part
          pageUrl: (name) -> "#{name}.html"
        cwd: "client-ui-new/_src/html/"
        src: "*.ejs"
        dest: "client-ui-new"
        expand: true
        ext: ".html"
      client_ui_new_php:
        options:
          trans: (str) -> "<?php echo $this->__('#{str}') ?>"
          staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
          pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
        cwd: "client-ui-new/_src/html/"
        src: "*.ejs"
        dest: "client-ui-new-php"
        expand: true
        ext: ".html"
      kiosk_new:
        options:
          trans: (str) -> str
          staticUrl: (part) -> part
          pageUrl: (name) -> "#{name}.html"
        cwd: "kiosk-new/_src/html/"
        src: "*.ejs"
        dest: "kiosk-new"
        expand: true
        ext: ".html"
      kiosk_new_php:
        options:
          trans: (str) -> "<?php echo $this->__('#{str}') ?>"
          staticUrl: (part) -> "<?php echo $this->getSkinUrl('#{part}') ?>"
          pageUrl: (name) -> "<?php echo $this->getUrl('#{name.replace(/-+/g, '/')}') ?>"
        cwd: "kiosk-new/_src/html/"
        src: "*.ejs"
        dest: "kiosk-new-php"
        expand: true
        ext: ".html"


    clean:
      css: {src: "stylesheets/*.css"}
      html_storage: {src: "storage/*.html"}
      html_kiosk: {src: "kiosk/*.html"}
      html_client_ui: {src: "client-ui/*.html"}
      client_ui_new: {src: ["client-ui-new/css", "client-ui-new/javascript", "client-ui-new/*.html", "client-ui-new-php"]}
      kiosk_new: {src: ["kiosk-new/css", "kiosk-new/javascript", "kiosk-new/*.html", "kiosk-new-php"]}

    watch:
      html_storage:
        files: ["storage/_src/**"]
        tasks: ["clean:html_storage", "ejs:storage"]
      html_kiosk:
        files: ["kiosk/_src/**"]
        tasks: ["clean:html_kiosk", "ejs:kiosk"]
      html_client_ui:
        files: ["client-ui/_src/**"]
        tasks: ["clean:html_client_ui", "ejs:client_ui"]
      css:
        files: ["stylesheets/_src/**"]
        tasks: ["clean:css", "stylus", "autoprefixer"]
      client_ui_new:
        files: ["client-ui-new/_src/**"]
        tasks: ["client_ui_new"]
      kiosk_new:
        files: ["kiosk-new/_src/**"]
        tasks: ["kiosk_new"]

  )

  require("load-grunt-tasks")(grunt)

  grunt.registerTask "default", [
    "clean",
    "ejs",
    "stylus", "autoprefixer",
    "concat:client_ui_new_js",
    "concat:kiosk_new_js"
  ]

  grunt.registerTask "client_ui_new", [
    "clean:client_ui_new", "ejs:client_ui_new", "ejs:client_ui_new_php"
    "stylus:client_ui_new", "autoprefixer:client_ui_new", "concat:client_ui_new_js"
  ]

  grunt.registerTask "kiosk_new", [
    "clean:kiosk_new", "ejs:kiosk_new", "ejs:kiosk_new_php"
    "stylus:kiosk_new", "autoprefixer:kiosk_new", "concat:kiosk_new_js"
  ]
