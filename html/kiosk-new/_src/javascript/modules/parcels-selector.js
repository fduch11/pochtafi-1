ns('parcels-selector', function() {


  // Pure utils

  function sum(a, b) {
    return a + b;
  }

  function sumDeposite(itemsData) {
    return _.reduce(_.pluck(itemsData, 'deposite'), sum, 0);
  }

  function sumService(itemsData) {
    return _.reduce(_.pluck(itemsData, 'service'), sum, 0);
  }

  function sumAll(itemsData) {
    return sumDeposite(itemsData) + sumService(itemsData);
  }

  function is0(n) {
    return n === 0;
  }

  function fixed2(_num) {
    var num = parseFloat(_num);
    if (isNaN(num)) {
      return _num;
    } else {
      return parseFloat(num.toFixed(2));
    }
  }

  function isEmpty(arr) {
    return arr.length === 0;
  }

  function getData(index, el) {
    return $(el).data();
  }



  $('.js-parcels-sel').each(function() {



    // Заблокированные чекбоксы не отправляются с формой

    $(this).on('submit', function() {
      $('input[type="checkbox"][disabled]').prop('disabled', false);
    });



    // Constants

    var $wrap = $(this);
    var $items = $('.js-parcels-sel__item', this);
    var $deposite = $('.js-parcels-sel__deposite', this);
    var $service = $('.js-parcels-sel__service', this);
    var $total = $('.js-parcels-sel__total', this);
    var $localCur = $('.js-parcels-sel__local-cur', this);

    var localCurExRate = parseFloat($localCur.data('ex-rate'));



    // Observables

    var itemsData = $items.asKefirProperty('change', function() {
      return $items.filter(':checked').map(getData).get();
    });

    var totalDeposite = itemsData.map(sumDeposite);
    var totalService = itemsData.map(sumService);
    var totalAll = itemsData.map(sumAll);
    var localCur = totalAll.map(function(n) {return n * localCurExRate});

    var isAllPaid = totalAll.map(is0);
    var isNoneSelected = itemsData.map(isEmpty);
    var noDeposit = totalDeposite.map(is0);
    var noService = totalService.map(is0);
    var noTotal = totalAll.map(is0);



    // Side effects

    totalDeposite.map(fixed2).onValue(['text', $deposite]);
    totalService.map(fixed2).onValue(['text', $service]);
    totalAll.map(fixed2).onValue(['text', $total]);
    localCur.map(fixed2).onValue(['text', $localCur]);

    noDeposit.onValue(['toggleClass', $wrap, 'no-deposit']);
    noService.onValue(['toggleClass', $wrap, 'no-service']);
    noTotal.onValue(['toggleClass', $wrap, 'no-total']);

    isAllPaid.onValue(['toggleClass', $wrap, 'is-all-paid']);
    isAllPaid.not().onValue(['toggleClass', $wrap, 'isnt-all-paid']);
    isNoneSelected.not().onValue(['toggleClass', $wrap, 'is-some-selected']);
    isNoneSelected.onValue(['toggleClass', $wrap, 'is-none-selected']);

  });

});
