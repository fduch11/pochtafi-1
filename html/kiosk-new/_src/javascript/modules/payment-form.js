ns('payment-form', function() {


  function isEmpty($el) {
    return $el.asKefirProperty('keyup change', function() {
      return $el.val() === '';
    })
  }

  $('.js-payment-form').each(function() {

    $('.js-payment-form__number', this).payment('formatCardNumber');
    $('.js-payment-form__code', this).payment('formatCardCVC');
    $('.js-payment-form__month', this).payment('restrictNumeric');
    $('.js-payment-form__year', this).payment('restrictNumeric');

    var isIncomplete = Kefir.or([
      isEmpty($('.js-payment-form__number', this)),
      isEmpty($('.js-payment-form__code', this)),
      isEmpty($('.js-payment-form__month', this)),
      isEmpty($('.js-payment-form__year', this))
    ]);

    isIncomplete.onValue(['prop', $('.js-payment-form__submit', this), 'disabled']);


    var $numInput = $('.js-payment-form__number', this);
    var cartNum = $numInput.asKefirProperty('input', function() {
      return $numInput.val();
    });

    cartNum
      .map($.payment.cardType)
      .map(function(type) {  return type === null ? '&nbsp;' : type  })
      .onValue(['html', $('.js-payment-form__cart-type', this)]);

  });


});
